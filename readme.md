# sick-bees

This repository collects information regarding the epidemiological models developed for the [VOODOO project][VOODOO]. It uses [Git LFS][git-lfs] which should be installed before cloning.

## Building and running

A literate programming approach is used where all artifacts are derived from a single set of LaTeX files.

To extract the Rust source code from the LaTeX files, run
```sh
cargo documents extract
```

To compile the LaTeX files into PDF documents, run
```sh
cargo documents compile
```

The Rust code requires a nightly toolchain. It is recommended to install this using [rustup][rustup]. 

### Documents

| file                                                                                                                                   | description                                                        |
|----------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| [references.bib](documents/references.bib)                                                                                             | BibTeX-based reference collection for all LaTeX documents          |
| [odd.tex][odd.tex] ([PDF][odd.pdf])                                                                                                    | Overview, design concepts and details                              |
| [diet_breadth_and_pathogen_prevalence.tex][diet_breadth_and_pathogen_prevalence.tex] ([PDF][diet_breadth_and_pathogen_prevalence.pdf]) | Paper on the relation between diet breadth and pathogen prevalence |
| [dach21.tex][dach21.tex] ([PDF][dach21.pdf])                                                                                           | Slides for a talk given at DACH-Tagung 2021                        |

### Programs

The main Cargo project is the model code. Additional Cargo projects provide support programs and libraries.

| folder          | description                                                     |
|-----------------|-----------------------------------------------------------------|
| `documents`     | Literate programming toolchain and main LaTeX files             |
| `experiments`   | Programs that execute the model and periodically dump its state |
| `measurements`  | Programs which read the dumps and extract CSV files             |
| `plots`         | Python scripts which load the CSV files and generate plots      |
| `visualisation` | Real-time 3D visualisation of the model state                   |
| `libraries`     | Support libraries for e.g. message passing and spatial indexing |

[VOODOO]: https://voodoo-project.eu/
[git-lfs]: https://git-lfs.github.com/
[rustup]: https://rustup.rs/
[odd.tex]: documents/odd.tex
[odd.pdf]: https://git.ufz.de/ecoepi/sick-bees/-/jobs/artifacts/main/raw/documents/odd.pdf?job=document
[diet_breadth_and_pathogen_prevalence.tex]: documents/diet_breadth_and_pathogen_prevalence/diet_breadth_and_pathogen_prevalence.tex
[diet_breadth_and_pathogen_prevalence.pdf]: https://git.ufz.de/ecoepi/sick-bees/-/jobs/artifacts/main/raw/documents/diet_breadth_and_pathogen_prevalence/diet_breadth_and_pathogen_prevalence.pdf?job=document
[dach21.tex]: documents/dach21/dach21.tex
[dach21.pdf]: https://git.ufz.de/ecoepi/sick-bees/-/jobs/artifacts/main/raw/documents/dach21/dach21.pdf?job=document
