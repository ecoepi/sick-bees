FROM centos:7

RUN yum update --assumeyes && \
    yum install --assumeyes gcc openmpi3-devel

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

RUN curl --output rustup-init --location https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init && \
    chmod +x rustup-init && \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain nightly-2022-03-07 && \
    rm rustup-init && \
    cargo install cargo-sweep
