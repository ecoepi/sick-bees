\subsection{Subdivision of landscape}
\label{grid}

\begin{figure}

\centering

\begin{tabular}{cc}

(a)

\begin{tikzpicture}[scale=5]

\draw (0,0) rectangle (1,1);

\fill[gray] (0.167,0.167) circle (0.167);
\fill[gray] (0.833,0.833) circle (0.167);

\draw[dashed] (0.333,0) -- (0.333,1);
\draw[dashed] (0.667,0) -- (0.667,1);

\draw[dashed] (0,0.333) -- (1,0.333);
\draw[dashed] (0,0.667) -- (1,0.667);

\end{tikzpicture}

&

(b)

\begin{tikzpicture}[scale=5]

\draw (0,0) rectangle (1,1);

\fill[gray] (0.167,0.167) circle (0.167);
\fill[gray] (0.833,0.833) circle (0.167);

\draw[dashed] (0.210,0) -- (0.210,1);
\draw[dashed] (0.789,0) -- (0.789,1);

\draw[dashed] (0,0.120) -- (0.210,0.120);
\draw[dashed] (0,0.213) -- (0.210,0.213);

\draw[dashed] (0.210,0.206) -- (0.789,0.206);
\draw[dashed] (0.210,0.793) -- (0.798,0.793);

\draw[dashed] (0.789,0.786) -- (1,0.786);
\draw[dashed] (0.789,0.879) -- (1,0.879);

\end{tikzpicture}

\end{tabular}

\caption{Equi-distant and weighted subdivisions of a landscape with two clusters of flower patches: In the equi-distant case (a), two processes are each assigned half of the weight while seven processes are assigned none. In the weighted case (b), all processes are assigned two ninths of the weight of a single cluster, i.e. one ninth of the whole weight.}

\end{figure}

\begin{hiddencode}
use std::cmp::Ordering;

use raster::Raster;

use crate::{
    params::Params,
    space::{mirror, Vector},
    units::Length,
};
\end{hiddencode}

The task of this module is to map the linear range of processes onto a two-dimensional grid. This grid subdivides the landscape in a manner that is conducive to balancing the computational load between the processes and is not equi-distant if a non-uniform landscape map is provided.

For each process, its rank and overall number processes as well as its column and row and the overall number of columns and rows are stored. The list \inlinecode{xy} contains the position of the subdivisions first along the \inlinecode{x} coordinate and then for each column along the \inlinecode{y} coordinate.

This information is used to efficiently map from a given position to the column and row within the grid and thereby rank of the process which should simulate any entity located at that position.

\begin{code}
pub struct Grid {
    pub rank: u8,
    pub size: u8,
    pub cols: u8,
    pub rows: u8,
    pub col: u8,
    pub row: u8,
    pub xy: Box<[Length]>,
}
\end{code}

The grid is initialised by first computing rows and columns based on the number of processes using the recursive functions \inlinecode{try\_cols\_rows} and starting at the smallest possible grid of one column and one row. This is then used to determine the column and row associated with the current process. Finally, a weighted or equi-distannt subdivision is constructed depending on whether a landscape map is provided.

\begin{code}
impl Grid {
    pub fn new(params: &Params, landscape: Option<&Raster>, rank: u8, size: u8) -> Self {
        let (cols, rows) = try_cols_rows(1, 1, size).unwrap();
        let (col, row) = rank_to_col_row(rank, rows);

        let mut xy = vec![Default::default(); (cols * (1 + rows)) as usize];

        if let Some(landscape) = landscape {
\end{code}

The construction of the weighted subdivision begins with a cummulative weight distribution of the landscape along the columns. The total weight is then split evenly among all columns. The upper end in histogram coordinates \inlinecode{col\_max} for each individual column is then computed by linearly extrapolating the this even split and finding the best matching histogram coordinate using a binary search in the cummulative weight distribution.

For each column, a cummulative weight distribution along the rows between the lower and upper end of the column is then computed. The total weight for this column is again split evenly among all rows and the upper end in histogram coordinates \inlinecode{row\_max} determined for each individual row.

This independent optimisation of the subdivisions along the columns and the rows of the histograms enables mapping a position to column-row pair using only two binary searches as with row subdivisions that are independent of the column subdivisions while stil ensuring a fully balanced weight between the grid cell.

\begin{code}
            let weights = landscape.columns();
            let weight_per_col = *weights.last().unwrap() / cols as f64;
            let length_per_col = params.length / weights.len() as f64;

            let mut col_min = 0;

            for col in 0..cols {
                let col_max = if col + 1 != cols {
                    let weight = weight_per_col * (col + 1) as f64;
                    weights.partition_point(|sum| *sum < weight)
                } else {
                    weights.len()
                };

                xy[col as usize] = length_per_col * col_max as f64;

                let weights = landscape.rows(col_min..col_max);
                let weight_per_row = *weights.last().unwrap() / rows as f64;
                let length_per_row = params.length / weights.len() as f64;

                for row in 0..rows {
                    let row_max = if row + 1 != rows {
                        let weight = weight_per_row * (row + 1) as f64;
                        weights.partition_point(|sum| *sum < weight)
                    } else {
                        weights.len()
                    };

                    xy[(cols + rows * col + row) as usize] = length_per_row * row_max as f64;
                }

                col_min = col_max;
            }
        } else {
            let length_per_col = params.length / cols as f64;
            let length_per_row = params.length / rows as f64;

            for col in 0..cols {
                xy[col as usize] = length_per_col * (col + 1) as f64;

                for row in 0..rows {
                    xy[(cols + rows * col + row) as usize] = length_per_row * (row + 1) as f64;
                }
            }
        }

        Self {
            rank,
            size,
            cols,
            rows,
            col,
            row,
            xy: xy.into(),
        }
    }
\end{code}

To determine whether an entity is visible to entities located at the current process, we compute the axis-aligned bounding box (AABB) of the associated grid cell. The given position is then checked against these bounds with additional padding according to a given visibility threshold.

Note that the position is assumed to be an element of the extended domain $\left[ -L, 2 L \right] \times \left[-L, 2 L \right]$, i.e. this visibility test accounts for the periodic boundary conditions by checking mirror locations as described in \autoref{mirror-locations} instead of the original positions of the entities.

\begin{code}
    pub fn is_visible(&self, vision: Length, position: &Vector) -> bool {
        let (centre, extent) = self.aabb(self.rank);

        let distance = centre - *position;

        distance.x.abs() <= extent.x + vision && distance.y.abs() <= extent.y + vision
    }
\end{code}

A process needs to communicate with only those other processes which simulate grid cells that are reachable from its grid cell in a single time step. These neighbouring processes can be determined by computing the AABB of both grid cells and checking for overlap which includes additional padding accounting for the given per-tick movement distance \inlinecode{speed}. The periodic boundary conditions are handled by computing mirror location as described in \autoref{mirror-locations} for the centre of one of the grid cells and checking for overlap with any of these transformed AABB as well.

\begin{code}
    pub fn is_reachable(&self, params: &Params, speed: Length, rank: u8) -> bool {
        let (centre0, extent0) = self.aabb(self.rank);
        let (centre1, extent1) = self.aabb(rank);
        let extent = extent0 + extent1;

        mirror(params, centre1).any(|centre1| {
            let distance = centre0 - centre1;

            distance.x.abs() <= extent.x + speed && distance.y.abs() <= extent.y + speed
        })
    }
\end{code}

Both visibility and reachability computations require the AABB of each grid cell. This is derived from the spatial subdivisions computed above and transformed into a centre-extent representation which works well with periodic boundary conditions as mirror locations can be computed for the centre to avoid splitting the AABB at the boundaries \autocite{periortree}.

\begin{code}
    fn aabb(&self, rank: u8) -> (Vector, Vector) {
        let (col, row) = rank_to_col_row(rank, self.rows);

        let x = &self.xy[..self.cols as usize];

        let x_min = if col == 0 {
            Default::default()
        } else {
            x[col as usize - 1]
        };

        let x_max = x[col as usize];

        let y = &self.xy[(self.cols + self.rows * col) as usize..][..self.rows as usize];

        let y_min = if row == 0 {
            Default::default()
        } else {
            y[row as usize - 1]
        };

        let y_max = y[row as usize];

        let bottom_left = Vector { x: x_min, y: y_min };
        let top_right = Vector { x: x_max, y: y_max };

        let centre = (top_right + bottom_left) / 2.;
        let extent = (top_right - bottom_left) / 2.;

        (centre, extent)
    }
\end{code}

Two binary searches are performed to map a given position to the rank of the process where an entity at that position should be located. First, the non-decreasing positions of the subdivisions of the \inlinecode{x} coordinate are searched to determine the column of that process. Second, the non-decreasing positions of the subdivisions of the \inlinecode{y} coordinate within that column are searched to determine the row of that row process. Finally, the column and row are combined into the rank.

\begin{code}
    pub fn location(&self, position: &Vector) -> u8 {
        let x = &self.xy[..self.cols as usize];
        let col = x.partition_point(|x| position.x >= *x) as u8;

        let y = &self.xy[(self.cols + self.rows * col) as usize..][..self.rows as usize];
        let row = y.partition_point(|y| position.y >= *y) as u8;

        col_row_to_rank(col, row, self.rows)
    }
}
\end{code}

The function \inlinecode{try\_cols\_rows} determines the largest possible number of columns and rows greater than the given values which multiply to yield the given size. It also produces the combinations that are as balanced as possible favoring rows over columns if equality is not attainable. For example, the square number $9$ is split into $3$ columns by $3$ rows. The composite number $12$ is split into $3$ columns by $4$ rows and the prime number $7$ is split into $1$ column and $7$ rows.

\begin{code}
fn try_cols_rows(cols: u8, rows: u8, size: u8) -> Option<(u8, u8)> {
    match (cols * rows).cmp(&size) {
        Ordering::Less => {
            let more_cols = || try_cols_rows(cols + 1, rows, size);
            let more_rows = || try_cols_rows(cols, rows + 1, size);

            if cols < rows {
                more_cols().or_else(more_rows)
            } else {
                more_rows().or_else(more_cols)
            }
        }
        Ordering::Equal => Some((cols, rows)),
        Ordering::Greater => None,
    }
}
\end{code}

Column and rows are mapped to the linear rank using column-major order.

\begin{code}
fn col_row_to_rank(col: u8, row: u8, rows: u8) -> u8 {
    rows * col + row
}

fn rank_to_col_row(rank: u8, rows: u8) -> (u8, u8) {
    (rank / rows, rank % rows)
}
\end{code}