\subsection{Units of measurement}

\begin{hiddencode}
use std::cmp::Ordering;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Rem, Sub, SubAssign};
\end{hiddencode}

Most state variables are stored as quantities with dimension and unit to improve compile-time checking of formulas. The following macro defines a named quantity which wraps a given type defined to represent a fixed base unit with other units defined by scaling the base unit. This wrapper initially only supports operations using values of the same quantity and dimensionless numbers as no assumption on the dimensions and their relationships are encoded in the generic type.

\begin{code}
macro_rules! quantity {
    (
        $outer:ident, $inner:ty,
        [$base:ident, $as_base:ident]
        $( , [$unit:ident, $as_unit:ident, $scale:expr] )*
        $( , $derives:ident )*
    ) => {
        #[derive(Clone, Copy, Default, PartialEq, PartialOrd $( , $derives )*)]
        #[repr(transparent)]
        pub struct $outer($inner);

        impl $outer {
            pub const fn $base(val: $inner) -> Self {
                Self(val)
            }

            pub const fn $as_base(&self) -> $inner {
                self.0
            }

            $(

            pub fn $unit(val: $inner) -> Self {
                Self(val * $scale)
            }

            pub fn $as_unit(&self) -> $inner {
                self.0 / $scale
            }

            )*

            #[must_use]
            pub fn min(self, other: Self) -> Self {
                match self.0.partial_cmp(&other.0) {
                    Some(Ordering::Greater) => other,
                    _ => self,
                }
            }

            #[must_use]
            pub fn max(self, other: Self) -> Self {
                match self.0.partial_cmp(&other.0) {
                    Some(Ordering::Less) => other,
                    _ => self,
                }
            }

            #[must_use]
            pub fn abs(self) -> Self {
                Self(self.0.abs())
            }

            pub const MAX: Self = Self(<$inner>::MAX);

            pub const MIN: Self = Self(<$inner>::MIN);
        }

        impl Neg for $outer {
            type Output = Self;

            fn neg(self) -> Self::Output {
                Self(-self.0)
            }
        }

        impl Add for $outer {
            type Output = Self;

            fn add(self, other: Self) -> Self::Output {
                Self(self.0 + other.0)
            }
        }

        impl AddAssign for $outer {
            fn add_assign(&mut self, other: Self) {
                self.0 += other.0;
            }
        }

        impl Sub for $outer {
            type Output = Self;

            fn sub(self, other: Self) -> Self::Output {
                Self(self.0 - other.0)
            }
        }

        impl SubAssign for $outer {
            fn sub_assign(&mut self, other: Self) {
                self.0 -= other.0;
            }
        }

        impl Mul<$inner> for $outer {
            type Output = Self;

            fn mul(self, other: $inner) -> Self::Output {
                Self(self.0 * other)
            }
        }

        impl MulAssign<$inner> for $outer {
            fn mul_assign(&mut self, other: $inner) {
                self.0 *= other;
            }
        }

        impl Div<$inner> for $outer {
            type Output = Self;

            fn div(self, other: $inner) -> Self::Output {
                Self(self.0 / other)
            }
        }

        impl DivAssign<$inner> for $outer {
            fn div_assign(&mut self, other: $inner) {
                self.0 /= other;
            }
        }

        impl Div for $outer {
            type Output = $inner;

            fn div(self, other: Self) -> Self::Output {
                self.0 / other.0
            }
        }

        impl Rem for $outer {
            type Output = Self;

            fn rem(self, other: Self) -> Self::Output {
                Self(self.0 % other.0)
            }
        }
    };
}
\end{code}

The following quantities are sufficient for all state variables used in the model with base units chosen so that typical magnitudes of these state variables are close to one and can therefore be represented with high accuracy using floating-point numbers.

\begin{code}
quantity!(
    Tick,
    i64,
    [milliseconds, as_milliseconds],
    [seconds, as_seconds, 1000],
    [minutes, as_minutes, 60 * 1000],
    [hours, as_hours, 60 * 60 * 1000],
    [days, as_days, 24 * 60 * 60 * 1000],
    [months, as_months, 30 * 24 * 60 * 60 * 1000],
    Eq,
    Ord
);

quantity!(
    Length,
    f64,
    [meters, as_meters],
    [kilometers, as_kilometers, 1000.]
);

quantity!(Velocity, f64, [meters_per_second, as_meters_per_second]);

quantity!(Scale, f64, [per_meter, as_per_meter]);

quantity!(Area, f64, [square_meters, as_square_meters]);

quantity!(
    Mass,
    f64,
    [micrograms, as_micrograms],
    [milligrams, as_milligrams, 1000.]
);

quantity!(MassRate, f64, [milligrams_per_hour, as_milligrams_per_hour]);

quantity!(Volume, f64, [cubic_millimeters, as_cubic_millimeters]);

quantity!(
    VolumeRate,
    f64,
    [cubic_millimeters_per_hour, as_cubic_millimeters_per_hour]
);

quantity!(
    Rate,
    f64,
    [per_second, as_per_second],
    [per_millisecond, as_per_millisecond, 1000.],
    [per_minute, as_per_minute, 1. / 60.],
    [per_hour, as_per_hour, 1. / (60. * 60.)],
    [per_day, as_per_day, 1. / (60. * 60. * 24.)]
);
\end{code}

The following trait implementations then define the relationships between the dimensions as required by the model computations.

\begin{code}
impl Length {
    pub fn atan2(self, other: Self) -> f64 {
        self.0.atan2(other.0)
    }
}

impl Area {
    pub fn sqrt(self) -> Length {
        Length(self.0.sqrt())
    }
}

impl Mul<Length> for Length {
    type Output = Area;

    fn mul(self, other: Length) -> Self::Output {
        Area(self.0 * other.0)
    }
}

impl Mul<Tick> for Velocity {
    type Output = Length;

    fn mul(self, other: Tick) -> Self::Output {
        Length(self.0 * other.0 as f64 / 1000.)
    }
}

impl Div<Tick> for Length {
    type Output = Velocity;

    fn div(self, other: Tick) -> Self::Output {
        Velocity(self.0 / other.0 as f64 * 1000.)
    }
}

impl Div<Length> for f64 {
    type Output = Scale;

    fn div(self, other: Length) -> Self::Output {
        Scale(self / other.0)
    }
}

impl Mul<Length> for Scale {
    type Output = f64;

    fn mul(self, other: Length) -> Self::Output {
        self.0 * other.0
    }
}

impl Div<Area> for Length {
    type Output = Scale;

    fn div(self, other: Area) -> Self::Output {
        Scale(self.0 / other.0)
    }
}

impl Mul<Tick> for MassRate {
    type Output = Mass;

    fn mul(self, other: Tick) -> Self::Output {
        Mass(self.0 * other.0 as f64 / (60. * 60.))
    }
}

impl Mul<Tick> for VolumeRate {
    type Output = Volume;

    fn mul(self, other: Tick) -> Self::Output {
        Volume(self.0 * other.0 as f64 / (60. * 60. * 1000.))
    }
}

impl Mul<Tick> for Rate {
    type Output = f64;

    fn mul(self, other: Tick) -> Self::Output {
        self.0 * other.0 as f64 / 1000.
    }
}
\end{code}