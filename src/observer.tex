\subsection{Observer}

The observer submodel represents a person walking along a transect to catch insects following the experiment protocol established for the VOODOO project. \autocite{experimental_protocol_2020} Its current purpose is to elucidate how the model dynamics shape the sample composition. Eventually, the aim is to perform direct comparisons between the simulated observations and empiricial results.

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[state,initial] (wait) {Wait};
\node[state] (catch-insect) [right of=wait,align=center] {Catch\\insect};
\node[state] (handle-specimen) [below of=catch-insect,align=center] {Handle\\specimen};
\node[state,accepting] (done) [right of=catch-insect] {Done};

\path
    (wait) edge node {} (catch-insect)
    (catch-insect) edge [bend right] node {} (handle-specimen)
    (catch-insect) edge node {} (done)
    (handle-specimen) edge [bend right] node {} (catch-insect);

\end{tikzpicture}

\caption{Activity diagram of observer}

\end{figure}

\begin{hiddencode}
use std::ops::ControlFlow;

use rand::Rng as _;

use kdtree::{KDTree, Query};

use crate::{
    bumblebee::Bumblebee,
    flower_patch::{FlowerPatch, Location as FlowerPatchLocation, Species as FlowerPatchSpecies},
    hoverfly::Hoverfly,
    insect::{Activity as InsectActivity, Species as InsectSpecies},
    params::Params,
    solitary_bee::SolitaryBee,
    space::{Vector, WithinDistanceOfLine, WithinDistanceOfPoint},
    units::Tick,
    Rng,
};
\end{hiddencode}

The observer keeps track of the amount of time spent looking for insects, its current position and its current activity.

\begin{code}
#[derive(Clone, Copy)]
pub struct Observer {
    pub duration: Tick,
    pub position: Vector,
    pub activity: Activity,
}
\end{code}

The overall process begins with waiting until the specified time at which the transect walk should start. This time usually dependens on the experiment performed and is chosen so that the model state has reached a sufficiently stable or even equilibrium state.

The observer then continuously scans its vicinity looking for insects to catch. This is interrupted by a certain duration spent handling each specimen before scanning the surroundings is resumed.

After the allocated amount of time has been spent looking for insects, the observer enters the done state and stays in that state for the rest of the simulation.

\begin{code}
#[derive(Clone, Copy)]
pub enum Activity {
    Wait,
    CatchInsect,
    HandleSpecimen(Tick),
    Done,
}
\end{code}

The observer supports multiple strategies on where its tries to catch insects. Either it continuously walks along the length of the transect or sits at a number of spots catching insects on the flower patches with its immediate vicinity.

\begin{code}
#[derive(Clone, Copy)]
pub enum Strategy {
    Walk,
    Sit(usize),
}
\end{code}

The observer creates records of insects it catches which inclue the time and place, the species of the flower patch and the insect as well as whether the flower patch was contaminated and whether the insect was infected. These are stored in the subworld state instead of the observer state to again simplify relocating the observer to different subworld.

\begin{code}
#[derive(Clone, Copy)]
pub struct Observation {
    pub tick: Tick,
    pub position: Vector,
    pub flower_patch_species: FlowerPatchSpecies,
    pub insect_species: InsectSpecies,
    pub contaminated: bool,
    pub infected: bool,
}
\end{code}

Initialisation differs between strategies: For the \inlinecode{Walk} strategy, a single observer is created and walks along the transect for the specified duration. For the \inlinecode{Sit} strategy, a separate observer is placed at each of the spots and the transect duration is divided evently among these. This is done so that observers do not need to be relocated between non-neighbouring subworlds.

Additionally, these spots are chosen so that at least a single flower patch is close-by to avoid wasting sampling effort where no insects can be expected to forage. The spots are also chosen sufficiently far apart so that observers will not deplete the insects in the vicinity of other observers.

\begin{code}
impl Observer {
    pub fn new(
        params: &Params,
        flower_patch_locations_close_to_transect: &KDTree<FlowerPatchLocation>,
        rng: &mut Rng,
    ) -> Vec<Self> {
        let mut observers = Vec::new();

        let duration = Default::default();
        let activity = Activity::Wait;

        match params.transect.strategy {
            Strategy::Walk => {
                let position = position(params, duration);

                observers.push(Self {
                    duration,
                    position,
                    activity,
                });
            }
            Strategy::Sit(number_of_spots) => {
                let spacing_2 = params.transect.spacing * params.transect.spacing;

                while observers.len() < number_of_spots {
                    let position = position(
                        params,
                        params.tick * rng.gen_range(0..=params.transect.duration / params.tick),
                    );

                    let mut flower_patch_close_by = false;

                    flower_patch_locations_close_to_transect.look_up(
                        WithinDistanceOfPoint::new(position, params.transect.vision),
                        |_location| {
                            flower_patch_close_by = true;

                            ControlFlow::Break(())
                        },
                    );

                    if !flower_patch_close_by {
                        continue;
                    }

                    let observer_close_by = observers
                        .iter()
                        .any(|observer| (observer.position - position).norm_2() <= spacing_2);

                    if observer_close_by {
                        continue;
                    }

                    observers.push(Self {
                        duration,
                        position,
                        activity,
                    });
                }
            }
        }

        observers
    }

    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        visited_flowers: &mut [Vec<Vec<u16>>; 3],
        tick: Tick,
        flower_patches: &[FlowerPatch],
        flowers: &[u64],
        hoverflies: &mut Vec<Hoverfly>,
        solitary_bees: &mut Vec<SolitaryBee>,
        bumblebees: &mut Vec<Bumblebee>,
        observations: &mut Vec<Observation>,
    ) {
        match self.activity {
            Activity::Wait => {
                if params.transect.wait_until <= tick {
                    self.activity = Activity::CatchInsect;
                }
            }
\end{code}

When looking for insects to catch, the observer enumerates all insects that collect nectar and pollen at some flower patch and which are within the maximum distance specificed in the transect parameters \autoref{transect-params}.

\begin{code}
            Activity::CatchInsect => {
                let insect = {
                    let max_distance_2 = params.transect.vision * params.transect.vision;

                    let hoverflies = hoverflies
                        .iter()
                        .enumerate()
                        .map(|(idx, hoverfly)| (idx, &hoverfly.insect));

                    let solitary_bees = solitary_bees
                        .iter()
                        .enumerate()
                        .map(|(idx, solitary_bee)| (idx, &solitary_bee.insect));

                    let bumblebees = bumblebees
                        .iter()
                        .enumerate()
                        .map(|(idx, bumblebee)| (idx, &bumblebee.insect));

                    hoverflies
                        .chain(solitary_bees)
                        .chain(bumblebees)
                        .filter(|(_, insect)| {
                            matches!(insect.activity, InsectActivity::CollectNectarAndPollen(..))
                        })
                        .map(|(idx, insect)| {
                            let distance_2 = (insect.position - self.position).norm_2();

                            (idx, insect, distance_2)
                        })
                        .filter(|(_, _, distance_2)| *distance_2 <= max_distance_2)
                        .min_by(|(_, _, lhs), (_, _, rhs)| lhs.partial_cmp(rhs).unwrap())
                };
\end{code}

If there are any such insects, the one that is closest to the observer is removed from the simulation. Considering human perception, this could be further refined to depend on the difficulty to spot the insect based on e.g. size and colour. After creating a record on the current specimen, the simulation of the time necessary to package and store it is started.

\begin{code}
                if let Some((idx, insect, _)) = insect {
                    let flower_patch = match insect.activity {
                        InsectActivity::CollectNectarAndPollen(index, _) => {
                            &flower_patches[index.get()]
                        }
                        _ => unreachable!(),
                    };

                    observations.push(Observation {
                        tick,
                        position: self.position,
                        flower_patch_species: flower_patch.species,
                        insect_species: insect.species,
                        contaminated: flower_patch.contaminated_flowers.count(flowers) != 0,
                        infected: insect.pathogen_status.is_infected(),
                    });

                    match insect.species {
                        InsectSpecies::Hoverfly(_) => {
                            hoverflies.swap_remove(idx);
                            visited_flowers[0].swap(idx, hoverflies.len());
                        }
                        InsectSpecies::SolitaryBee(_) => {
                            solitary_bees.swap_remove(idx);
                            visited_flowers[1].swap(idx, solitary_bees.len());
                        }
                        InsectSpecies::Bumblebee(_) => {
                            bumblebees.swap_remove(idx);
                            visited_flowers[2].swap(idx, bumblebees.len());
                        }
                    }

                    self.activity =
                        Activity::HandleSpecimen(tick + params.transect.handling_duration);
                } else {
\end{code}

Otherwise, the transect duration is incremented and the position is updated accordingly. When the transect duration reaches the value specified in the model parameters, the observer stops its activities for the rest of the simulation.

\begin{code}
                    self.duration += params.tick;

                    let duration = match params.transect.strategy {
                        Strategy::Walk => {
                            self.position = position(params, self.duration);

                            params.transect.duration
                        }
                        Strategy::Sit(number_of_spots) => {
                            params.transect.duration / number_of_spots as i64
                        }
                    };

                    if self.duration >= duration {
                        self.activity = Activity::Done;
                    }
                }
            }
            Activity::HandleSpecimen(finishes_at) => {
                if finishes_at <= tick {
                    self.activity = Activity::CatchInsect;
                }
            }
            Activity::Done => (),
        }
    }
}
\end{code}

\begin{figure}

\centering

\begin{tikzpicture}[scale=5]

\draw (0,0) rectangle (1,1);
\draw[dashed] (0.5,0.5) circle (0.5);

\draw[->,thick] (0.146,0.146) -- (0.854, 0.854);

\draw[dashed] (0.5,0.5) -- (1,0.5);
\draw[->] (0.75,0.5) arc (0:45:0.25);

\end{tikzpicture}

\caption{Illustration of the geometry of the simulated transects.}

\end{figure}

The geometry of the transects is currently a straight line through the centre of the landscape with \inlinecode{length} and \inlinecode{direction} as free parameters.

\begin{code}
fn position(params: &Params, duration: Tick) -> Vector {
    let radius = (params.transect.length / params.transect.duration) * duration
        - params.transect.length / 2.;

    let (sine, cosine) = params.transect.direction.sin_cos();

    Vector {
        x: params.length / 2. + radius * cosine,
        y: params.length / 2. + radius * sine,
    }
}
\end{code}

\begin{code}
pub fn close_to_transect(params: &Params) -> impl Fn(&Vector) -> bool {
    let from = position(params, Default::default());
    let to = position(params, params.transect.duration);

    let query = WithinDistanceOfLine::new(from, to, params.transect.vision);

    move |position| query.test(position)
}
\end{code}