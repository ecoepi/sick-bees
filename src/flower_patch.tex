\subsection{Flower patch}
\label{flower-patch-submodel}

Flower patches \marginpar{rationale} are the source of nectar and pollen for the insects which need nectar to sustain their flight and the protein-rich pollen to fuel their reproductive activities. Only the availability of nectar is modelled explicitly as flowers seem to actively regulate the amount of available nectar. Presumbly this mechanism evolved to force pollinators to visit multiple plants whereas pollen is produced in large quantities to ensure a high probability of pollination for every visitation. \autocite{collection_of_pollen_by_bees} \autocite{food_for_pollinators}

Considering the typical oral-fecal infection routes of bee pathogens, the individual flowers are the preferred unit of epidemiological contacts. But resource collection rates for seldomly studied species of wild pollinators are available only on the aggregate level of at least a single foraging bout. Additionally, the computational cost associated with tracking the relevant state variables for each flower as a separate entity has proven untenable. Therefore, the available floral resources are tracked on the aggregate flower patch level and this bulk flow of nutrients is used to drive the exploration of the landscape by the insects.

However, the flower patches are endowed with an additional spatially implicit fine-grained description of the emptiness and contamination of individual flowers. The visitation rates and handling times implicit in the gross collection rates are then used to derive visitation events and thereby epidemiological contact events at the level of individual flowers. Tracking contacts at this granularity allows the usage of state transition probabilities for infection and contamination which can be independently measured in controlled experiments. \autocite{bee_pathogen_transmission_dynamics}

While this small-scale description is spatially implicit, statistic correlations can still be encoded into the index order of the flowers within a patch. Examples for distinctions that can be realised without explicit spatial structure are visitation orders assuming perfect flower memory versus none at all or visitation orders that are fully independent between insects versus highly correlated.

\begin{hiddencode}
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::mem::MaybeUninit;

use rand::{
    distributions::{Distribution, OpenClosed01},
    Rng as _,
};

use bitmap::Bitmap;
use kdtree::Object;

use crate::{
    insect::Species as InsectSpecies,
    params::Params,
    space::Vector,
    units::{Area, Mass, Tick},
    Rng, TaggedIndex,
};
\end{hiddencode}

The parameters \marginpar{state variables} associated with each instance of the flower patch submodel include the plant species \inlinecode{species}, the amount of nectar produced per day \inlinecode{nectar\_production} and the number of individual flowers \inlinecode{flowers} which a given patch represents.

The relevant state variables are the mass of the nectar available at the flower patch \inlinecode{nectar}, a bitmap tracking which individual flowers are empty \inlinecode{empty_flowers} and a bitmap tracking which individual flowers are contaminated.

Having both a continuous representation of available resources by tracking nectar mass and a discrete representation by tracking empty flowers might seem redundant, but this redundancy is used to derive visits to individual flowers from the aggregate flow of nutrients on the patch level as described in \autoref{collect-nectar-and-pollen}. The epidemiological consequences of these contact events are then tracked on the level of individual flowers as well.

The auxiliary variable \inlinecode{contaminated\_by\_species} is used to track which insect species was responsible for last contamination of a flower in this patch and does not affect the pathogen dynamics but is used as a micro-observable as described in \autoref{micro-observables}.

\begin{code}
#[derive(Clone, Copy)]
pub struct FlowerPatch {
    pub species: Species,
    pub nectar: Mass,
    pub flowers: u16,
    pub nectar_production: Mass,
    pub empty_flowers: Bitmap,
    pub contaminated_flowers: Bitmap,
    pub contaminated_by_species: MaybeUninit<InsectSpecies>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Species(pub u8);
\end{code}

During model initialisation \marginpar{initialisation} as detailed in \autoref{initialisation}, all flower patches are assigned the same area. Local variations in the availability of floral resources are modelled via the number density of flower patches instead of the number of individual flowers per patch. This is realised as weighted random placement based on GIS data as described in \autoref{random-placement}.

Subsequently, this area is used to determine the number of flowers and nectar production of this particular species. Both parameters are based on randomly selecting a plant species according to the weights stored in the initialisation parameters described in \autoref{init-params} and include the biological variability of the underlying densities by using stochasticity during the initialisation.

\begin{code}
impl FlowerPatch {
    pub fn new(
        params: &Params,
        area: Area,
        refills_at: &mut Option<Tick>,
        flowers1: &mut Vec<u64>,
        rng: &mut Rng,
    ) -> Self {
        let species = Species(params.init.flower_patch_species.sample(rng) as u8);

        let flower_patch_params = &params.flower_patches[species.0 as usize];

        let flowers = flower_patch_params.flowers(area, rng);
        let nectar_production = flower_patch_params.nectar_production(rng);

        let mut empty_flowers = Bitmap::new(flowers1, flowers as usize);
        let contaminated_flowers = Bitmap::new(flowers1, flowers as usize);
\end{code}

To decrease the time until an equilibrium between nectar production and consumption is reached, the flower patches are initialised stochastically so that the fraction given by the parameter \inlinecode{empty\_flower\_patches} is empty with a uniformly distributed time-to-refill while the rest is given a uniformly distributed amount of nectar left.

For the non-depleted flower patches, the discrete representation of available nectar via the tracking of empty flowers is brought into correspondence with the continuous representation via the tracking of nectar mass by marking randomly selected flowers as empty until the nectar production associated with the full flowers falls below the continuous state variable. For the depleted flower patches, this happens by simply marking all flowers as empty.

\begin{code}
        let nectar = if params.init.empty_flower_patches.sample(rng) {
            *refills_at = Some(params.tick * rng.gen_range(1..=Tick::days(1) / params.tick));

            for flower in 0..flowers {
                empty_flowers.set(flowers1, flower as usize, true);
            }

            Default::default()
        } else {
            *refills_at = None;

            let nectar = nectar_production * OpenClosed01.sample(rng) * flowers as f64;

            let mut full_flowers = flowers;

            while full_flowers != 0 && nectar_production * full_flowers as f64 > nectar {
                let flower = rng.gen_range(0..flowers);

                if empty_flowers.test_and_set(flowers1, flower as usize, true) {
                    full_flowers -= 1;
                }
            }

            nectar
        };

        Self {
            species,
            nectar,
            flowers,
            nectar_production,
            empty_flowers,
            contaminated_flowers,
            contaminated_by_species: MaybeUninit::uninit(),
        }
    }
}
\end{code}

The nectar available at each flower patch is modified directly in the insect submodel described in \autoref{insect-submodel} for reasons of efficiency. The remaining state update \marginpar{state update} performed directly in the flower patch submodel comprises resetting the available nectar to the daily production when the refill interval of 24 hours has elapsed as indicated by the \inlinecode{refills\_at} state variable. Similarly, most of the pathogen transmission is implemented in the generic insect submodel whereas the flower patch submodel is limited to the decontamination state transitions shown in \autoref{state-transitions-bipartite-sis-model}.

These two processes are slow compared to the time step of the model and are therefore realised using a DES formalism instead of inspecting the entity state at each time step. All event kinds are timestamped and contain the index of the affected flower patch. The deconamination additionally encodes the index of the affected flower within the patch.

The first event \inlinecode{Event::Refill} is applied by setting the amount of available nectar to the daily production of all flowers within the patch and resetting all empty flowers to full. The second event \inlinecode{Event::Decontamination} is applied by resetting the contamination of the affected flower.

\begin{code}
#[derive(Clone, Copy)]
pub enum Event {
    Refill {
        happens_at: Tick,
        flower_patch: TaggedIndex,
    },
    Decontamination {
        happens_at: Tick,
        flower_patch: TaggedIndex,
        flower: u16,
    },
}

impl Event {
    pub fn process(
        events: &mut BinaryHeap<Event>,
        tick: Tick,
        flower_patches: &mut [FlowerPatch],
        flowers: &mut [u64],
    ) {
        loop {
            match events.peek() {
                Some(Self::Refill {
                    happens_at,
                    flower_patch,
                }) if *happens_at <= tick => {
                    let flower_patch = &mut flower_patches[flower_patch.get()];

                    flower_patch.nectar =
                        flower_patch.nectar_production * flower_patch.flowers as f64;

                    flower_patch.empty_flowers.reset(flowers);

                    events.pop();
                }
                Some(Self::Decontamination {
                    happens_at,
                    flower_patch,
                    flower,
                }) if *happens_at <= tick => {
                    flower_patches[flower_patch.get()].contaminated_flowers.set(
                        flowers,
                        *flower as usize,
                        false,
                    );

                    events.pop();
                }
                _ => break,
            }
        }
    }

    fn happens_at(&self) -> &Tick {
        match self {
            Self::Refill { happens_at, .. } => happens_at,
            Self::Decontamination { happens_at, .. } => happens_at,
        }
    }
}
\end{code}

Note that the \inlinecode{BinaryHeap} data structure is max heap but the simulation requires the event with the smallest timestamp. Hence the total order defined by the \inlinecode{Ord} trait is reversed by exchanging the left hand side and the right hand side of the comparison.

\begin{code}
impl PartialEq for Event {
    fn eq(&self, other: &Self) -> bool {
        self.happens_at() == other.happens_at()
    }
}

impl Eq for Event {}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Event {
    fn cmp(&self, other: &Self) -> Ordering {
        other.happens_at().cmp(self.happens_at())
    }
}
\end{code}

Flower patches as stationary entities which are indexed using K-D trees \marginpar{spatial indexing} to accelerate the spatial queries performed in the insect submodel described in \autoref{insect-submodel}. To enable indexing flower patches located in neighbouring subworlds, the \inlinecode{Location} type includes a tagged index which contains both the rank of the process which simulates the flower patches as well as the index of the entity state at that process.

Finally, the data structure is amended with the plant species as this is necessary to match the specific preferences when insects decide which of the currently visible flower patch they should visit. While this information is redundant when the flower patch and insect are simulated by the same process, it is unavailable if both entities are located in different subworlds.

\begin{code}
#[derive(Clone, Copy)]
pub struct Location {
    pub position: Vector,
    pub index: TaggedIndex,
    pub species: Species,
}

impl Object for Location {
    type Point = Vector;

    fn position(&self) -> &Self::Point {
        &self.position
    }
}
\end{code}