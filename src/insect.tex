\subsection{Insect}
\label{insect-submodel}

The generic insect submodel encapsulates \marginpar{rationale} the behaviours which are common to all three insect families. This includes foraging, i.e. searching or flower patches as well as collecting nectar and pollen. And it handles most of the pathogen transmission, i.e. infection, contamination and recovery. It is adapted for the different species of the of insect families using submodel-scope parameters like speed of flight, sensing distance for flowers, nectar consumption rate, pollen collection rate and the probability rates for the epidemiological state transition.

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[state,initial] (search-flower-patch) [align=center] {Search\\flower patch};
\node[state] (enter-flower-patch) [right of=search-flower-patch,align=center] {Enter\\flower patch};
\node[state] (collect-nectar-and-pollen) [right of=enter-flower-patch,align=center] {Collect nectar\\and pollen};
\node[state] (other) [below of=search-flower-patch] {Other};

\path
    (search-flower-patch) edge node {} (enter-flower-patch)
    (enter-flower-patch) edge node {} (collect-nectar-and-pollen)
    (collect-nectar-and-pollen) edge [out=135,in=45] node {} (search-flower-patch)
    (collect-nectar-and-pollen) edge node {} (other)
    (other) edge node {} (search-flower-patch);

\end{tikzpicture}

\caption{Activity diagram of insects}
\label{insect-activity-diagram}

\end{figure}

\begin{hiddencode}
use std::collections::BinaryHeap;
use std::ops::ControlFlow;

use rand::{distributions::Distribution, seq::SliceRandom, Rng as _};

use access::{dictionary, histogram};
use kdtree::KDTree;
use queue::Queue;

use crate::{
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation},
    params::{Insect as InsectParams, Params, FLOWER_PATCH_MEMORY},
    pathogen::{infection_contamination, recovery, RecoveryStatus, Status as PathogenStatus},
    reduce_lifetime,
    space::{reduce, Vector, WithinDistanceOfLine},
    units::{Tick, Volume},
    Contacts, FlowerContaminators, NextId, Rng, TaggedIndex, TemporalNetwork, Vectors,
};
\end{hiddencode}

The state variables \marginpar{state variables} associated with each instance of the generic insect submodel include
\begin{itemize}

\item the \inlinecode{position} and \inlinecode{movement} which is the velocity premultiplied by the time step,

\item the \inlinecode{activity} currently performed by the insect,

\item the amount of \inlinecode{pollen} collected which is evaluated by the family-specific submodels,

\item a fixed-capacity \inlinecode{memory} of visited or rejected flower patches,\footnote{The queue capacity is a compile-time constant as the time-ordered queue is stored inline due to out-of-line storage being incompatible with passing around the submodel state as a self-contained message.}

\item a simplified epidemiological status \inlinecode{pathogen_status}, indicating e.g. susceptible or infected insects.

\end{itemize}

The parameter \inlinecode{species} is also associated with each instance and is used to access parameter values like velocity or memory capacity that show specific differences. The type \inlinecode{Species} has a hierarchical structure indicating first the family and then enumerating the species within that family using a zero-based index.

\begin{code}
#[derive(Clone, Copy)]
pub struct Insect {
    pub id: u32,
    pub dies_at: Tick,
    pub position: Vector,
    pub movement: Vector,
    pub species: Species,
    pub activity: Activity,
    pub memory: Queue<TaggedIndex, FLOWER_PATCH_MEMORY>,
    pub pollen: Volume,
    pub visits: usize,
    pub pathogen_status: PathogenStatus,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Species {
    Hoverfly(u8),
    SolitaryBee(u8),
    Bumblebee(u8),
}

#[derive(Clone, Copy, PartialEq)]
pub enum Activity {
    SearchFlowerPatch(Tick),
    EnterFlowerPatch(TaggedIndex),
    CollectNectarAndPollen(TaggedIndex, Tick),
    Other,
}
\end{code}

All insects are initialised \marginpar{initialisation} searching for flower patches while at rest but immediately before their next turn. The position is determined in the top-level initialisation described in \autoref{initialisation} and can happen both stochastically for hoverflies and solitary bees as well as deterministically for bumblebees which always start at their associated colony.

The plant species and amount of pollen collected are determined by the family-specific submodel, which contains the generic insect submodel as a subsubmodel. These values are then passed into this initialisation routine. This is done for purely practical reasons as the necessary parameters are accessed there in any case.

Finally, the infection status is randomly initialised here using the strategy given by the \inlinecode{infection} parameter and described in detail in \autoref{pathogen-initialisation}.

\begin{code}
impl Insect {
    pub fn new(
        params: &Params,
        insect_params: &InsectParams,
        id: u32,
        position: Vector,
        species: Species,
        pollen: Volume,
        rng: &mut Rng,
    ) -> Self {
        let dies_at = insect_params.life_expectancy(params, rng);

        let pathogen_status = params
            .init
            .infection
            .status(species, params, insect_params, rng);

        Self {
            id,
            dies_at,
            position,
            movement: Default::default(),
            species,
            activity: Activity::SearchFlowerPatch(Default::default()),
            memory: Default::default(),
            pollen,
            visits: 0,
            pathogen_status,
        }
    }
\end{code}

The state update function \marginpar{state update} implements the state machine illustrated in \autoref{insect-activity-diagram}. Searching for flower patches is realised as a random walk and specific probabilities for visiting a flower patch. Collecting nectar and pollen is approximated as happening with constant rates and tracks statistics on insect-plant contacts and their durations. This activity also includes pathogen transmission via infection and contamination while foraging at a flower patch. This is further augmented by tracking statistics on the insect and plant species involved in spreading the disease. Finally, the integration of the generic subsubmodel with the family-specific submodel is managed via the \inlinecode{Other} activity which suspends the foraging activities leaving only pathogen recovery to be simulated.

\begin{code}
    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        insect_params: &InsectParams,
        flower_patch_locations: &KDTree<FlowerPatchLocation>,
        visible_flower_patch_locations: &mut Vec<&'static FlowerPatchLocation>,
        visited_flowers: &mut Vec<u16>,
        flower_contaminators: &mut FlowerContaminators,
        flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
        tick: Tick,
        flower_patches: &mut [FlowerPatch],
        flowers: &mut [u64],
        rng: &mut Rng,
        next_id: &mut NextId,
        contacts: &mut Contacts,
        vectors: &mut Vectors,
        temporal_network: &mut TemporalNetwork,
    ) {
        match &mut self.activity {
\end{code}

\phantomsection
\label{search-flower-patch}

The \emph{search for flower patches} is modelled as random walk with constant velocity\footnote{The \inlinecode{speed} parameter is premultiplied with the time step hence has the dimensions of a length to improve performance by avoiding to repeatedly multiply the same velocity with the constant time step.}, a constant time-between-turns and a turning angle with an arbitrary distribution that is assumed to be independent of the current movement direction.

The flower patches within the distance \inlinecode{vision} of the flight path covered in the current time step are considered visible. This geometry is represented as a tube with circular end caps around the straight line between old and new position and implemented by a single query against a K-D tree spatial index as described in \autoref{spatial-queries}.

These visible flower patches are then considered in random order as processing them in the order returned by the spatial query would introduce artificial correlations in the order in which the insects visit the flower patches. The resulting concentration of insects was found to have significant effects on pathogen prevalence values. These effects are artificial insofar they are purely based on the internal organisation of the K-D trees.

Each flower patch is then accepted or rejected with a probability chosen based on both insect and plant species and stored in \inlinecode{insect\_params.preferences}. All considered flower patches are immediately memorised as visited or rejected so that probabilities do not accumulate over multiple time steps. The maximum memory capacity \inlinecode{FLOWER\_PATCH\_MEMORY} is potentially reduced to the value of \inlinecode{insect\_params.memory} to enable the simulation of specific differences in memory capacity.

If a flower patch is accepted, the insect begins to collect nectar and pollen in the next time step after a possible relocation to the same subworld as the flower patch. If all flower patches are rejected, the search is continued in the next time step beginning at the new position.

\begin{code}
            Activity::SearchFlowerPatch(next_turn) => {
                if *next_turn <= tick {
                    let (sine, cosine) = insect_params.direction.sample(rng).sin_cos();

                    self.movement = Vector {
                        x: insect_params.speed * cosine,
                        y: insect_params.speed * sine,
                    };

                    *next_turn = tick + insect_params.between_turns;
                }

                let new_position = self.position + self.movement;

                reduce_lifetime(visible_flower_patch_locations, |visible_locations| {
                    flower_patch_locations.look_up(
                        WithinDistanceOfLine::new(
                            self.position,
                            new_position,
                            insect_params.vision,
                        ),
                        |location| {
                            if !self.memory.contains(&location.index) {
                                visible_locations.push(location);
                            }

                            ControlFlow::Continue(())
                        },
                    );

                    let mut chosen_location = None;

                    while !visible_locations.is_empty() {
                        let idx = rng.gen_range(0..visible_locations.len());
                        let location = visible_locations.swap_remove(idx);

                        if self.memory.len() == insect_params.memory {
                            self.memory.pop();
                        }

                        self.memory.push(location.index);

                        if insect_params.preferences[location.species.0 as usize].sample(rng) {
                            chosen_location = Some(location);
                            break;
                        }
                    }

                    if let Some(location) = chosen_location {
                        self.position = reduce(params, location.position);
                        self.activity = Activity::EnterFlowerPatch(location.index);
                    } else {
                        self.position = reduce(params, new_position);
                    }
                });
            }
\end{code}

\phantomsection
\label{collect-nectar-and-pollen}

The \emph{consumption of nectar and the collection of pollen} are simulated using constant rates premultiplied by the time step stored in \inlinecode{insect\_params.nectar\_consumption} and \inlinecode{pollen\_collection}. For reasons of computational efficiency, the insect submodel updates both its internal state as well as the state defined by the flower patch submodel. In particular, the insect submodel dispatches a \inlinecode{FlowerPatchEvent::Refill} event timestamped at one day into the future when the flower patch is depleted of nectar.

After each update of the continuous representation of floral resources, its discrete counterpart which tracks which individual flowers are empty is brought back into correspondence if the two points of view have diverged. This is achieved by marking additional flowers as empty until the nectar availability implied by the full flowers falls below the aggregate value. Each checked flower whether it is empty or not is considered as epidemiological contact. Thereby the redundant descriptions of the flow of nectar make it possible to use the flower visitation rates and handling times implicit to the bulk collection rates to drive the pathogen transmission submodel as described in \autoref{pathogen-submodel}.

The order in which the flowers are visited is defined by the indices stored in \inlinecode{visited_flowers}. When this trajectory is initialised in the transitional \inlinecode{EnterFlowerPatch} activity, assumptions on the small-scale spatial patterns in the insects foraging behaviour can be encoded even though the description of the fine-grained structure within a flower patch is spatially implicit. At the moment, it is assumed that every visitor independently draws a permutation of the flowers within a patch, i.e. there are no correlations between different insects foraging at the same patch but perfect memory is assumed as no flower is visited more than once.

As the scarcity of nectar is primarily used as a driver for the exploration of the landscape by the insects, it is only removed from the flower patches without considering its use as an energy source by the insects. Similarly, the primary role of pollen is as a resource which the insects invest into their reproduction \autocite{adult_feeding_by_holometabolous_insects} and it is hence only added to the amount collected by the insect. Its availability at the flower patch is assumed to not be a limiting factor.

\begin{code}
            Activity::EnterFlowerPatch(index) => {
                let flower_patch = &mut flower_patches[index.get()];

                visited_flowers.clear();
                visited_flowers.extend(0..flower_patch.flowers);
                visited_flowers.shuffle(rng);

                self.activity = Activity::CollectNectarAndPollen(*index, tick);
            }
            Activity::CollectNectarAndPollen(index, collecting_since) => {
                let flower_patch = &mut flower_patches[index.get()];

                if flower_patch.nectar > Default::default() {
                    flower_patch.nectar -= insect_params.nectar_consumption;

                    let mut full_flowers =
                        flower_patch.flowers - flower_patch.empty_flowers.count(flowers) as u16;

                    while full_flowers != 0
                        && flower_patch.nectar_production * full_flowers as f64
                            > flower_patch.nectar
                    {
                        let flower = visited_flowers.pop().unwrap();

                        if flower_patch
                            .empty_flowers
                            .test_and_set(flowers, flower as usize, true)
                        {
                            full_flowers -= 1;
                        }

                        infection_contamination(
                            self.id,
                            params,
                            insect_params,
                            self.species,
                            &mut self.pathogen_status,
                            *index,
                            flower,
                            flower_contaminators,
                            flower_patch_events,
                            tick,
                            flower_patch,
                            flowers,
                            rng,
                            vectors,
                            temporal_network,
                        );

                        self.visits += 1;
                    }

                    if flower_patch.nectar <= Default::default() {
                        flower_patch_events.push(FlowerPatchEvent::Refill {
                            happens_at: tick + Tick::days(1),
                            flower_patch: *index,
                        });

                        flower_patch.nectar = Default::default();
                    }

                    self.pollen += insect_params.pollen_collection;
                } else {
                    let collecting_for = tick - *collecting_since;

                    self.activity = Activity::SearchFlowerPatch(Default::default());
\end{code}

When the search for other flower patches is resumed due the depletion of the current flower patch, a micro-observable as described in \autoref{micro-observables} is updated to track which insect species visits which plant species how often and for how long. This is realised as a sparse map index by both species values storing a histogram of visits binned by their duration with a resolution of one minute. This distribution of contact durations is zero-inflated, i.e. most contacts only last a single tick as the flower patch is empty. Hence the histogram is stored only for contacts that last more than a single tick to allow using a resolution larger than a single tick.

Note that the collection of nectar and pollen can also be preempted by family-specific submodels at any time by directly switching the insect submodel into the \inlinecode{Other} activity when reproductive activities are commenced.

\begin{code}
                    let contacts = dictionary(contacts, (flower_patch.species, self.species));

                    if collecting_for == params.tick {
                        contacts.0 += 1;
                    } else {
                        histogram(&mut contacts.1, Tick::minutes(1), collecting_for);
                    }
                }
            }
            Activity::Other => (),
        }
\end{code}

Each individual insect is assigned a tick at which it dies as defined in \autoref{insect-params}. This is implemented by assigning a new identifier \inlinecode{id} and tick of death \inlinecode{dies_at}. Infected individuals will not die naturally but only when they recover according to the pathogen submodel as defined in \autoref{pathogen-submodel}. This is done so that the distributions of the life expectancy and the recovery time do not interact.

\begin{code}
        let just_died = match recovery(&mut self.pathogen_status, tick) {
            RecoveryStatus::WillRecover => false,
            RecoveryStatus::JustRecovered => true,
            RecoveryStatus::Unaffected => self.dies_at <= tick,
        };

        if just_died {
            let next_id = next_id.get();

            if params.trace_temporal_network {
                temporal_network.node_removals.push((tick, self.id));

                temporal_network
                    .node_additions
                    .push((tick, next_id, self.species));
            }

            self.id = next_id;
            self.dies_at = tick + insect_params.life_expectancy(params, rng);
        }
    }
}
\end{code}