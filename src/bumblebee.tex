\subsubsection{Bumblebee}
\label{bumblebee-submodel}

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[state] (return-to-colony) [align=center] {Return\\to colony};
\node[state] (work-in-colony) [right of=return-to-colony,align=center] {Work in\\colony};
\node[state,initial] (other) [below of=return-to-colony] {Other};

\path
    (return-to-colony) edge node {} (work-in-colony)
    (work-in-colony) edge node {} (other)
    (other) edge node {} (return-to-colony);

\end{tikzpicture}

\caption{Activity diagram of bumblebees}

\end{figure}

\begin{hiddencode}
use std::collections::BinaryHeap;

use rand::{distributions::Distribution, Rng as _};

use kdtree::KDTree;

use crate::{
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation},
    insect::{Activity as InsectActivity, Insect, Species as InsectSpecies},
    params::Params,
    pathogen::{colony_decontamination, colony_infection_contamination, Contaminated},
    space::{reduce, shortest_path, Vector},
    units::Tick,
    Contacts, FlowerContaminators, NextId, Rng, TaggedIndex, TemporalNetwork, Vectors,
};
\end{hiddencode}

\begin{code}
#[derive(Clone, Copy)]
pub struct Bumblebee {
    pub colony_location: ColonyLocation,
    pub insect: Insect,
    pub activity: Activity,
}

#[derive(Clone, Copy)]
pub enum Activity {
    ReturnToColony(Tick),
    WorkInColony(Tick),
    Other,
}
\end{code}

\begin{code}
impl Bumblebee {
    pub fn new(
        params: &Params,
        id: u32,
        colony_location: ColonyLocation,
        species: u8,
        rng: &mut Rng,
    ) -> Self {
        let bumblebee_params = &params.bumblebees[species as usize];

        let pollen = bumblebee_params.pollen_per_bout * rng.gen();

        let insect = Insect::new(
            params,
            &bumblebee_params.insect,
            id,
            colony_location.position,
            InsectSpecies::Bumblebee(species as u8),
            pollen,
            rng,
        );

        Self {
            colony_location,
            insect,
            activity: Activity::Other,
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        flower_patch_locations: &KDTree<FlowerPatchLocation>,
        visible_flower_patch_locations: &mut Vec<&'static FlowerPatchLocation>,
        visited_flowers: &mut Vec<u16>,
        flower_contaminators: &mut FlowerContaminators,
        flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
        tick: Tick,
        flower_patches: &mut [FlowerPatch],
        flowers: &mut [u64],
        colonies: &mut [Colony],
        rng: &mut Rng,
        next_id: &mut NextId,
        contacts: &mut Contacts,
        vectors: &mut Vectors,
        temporal_network: &mut TemporalNetwork,
    ) {
        let bumblebee_params = match self.insect.species {
            InsectSpecies::Bumblebee(species) => &params.bumblebees[species as usize],
            _ => unreachable!(),
        };

        let insect_params = &bumblebee_params.insect;

        match self.activity {
            Activity::ReturnToColony(returns_at) => {
                if returns_at <= tick {
                    self.insect.position = self.colony_location.position;
                    self.activity = Activity::WorkInColony(tick + bumblebee_params.work_duration);
                } else {
                    self.insect.position =
                        reduce(params, self.insect.position + self.insect.movement);
                }
            }
            Activity::WorkInColony(finishes_at) => {
                if finishes_at <= tick {
                    let colony = &mut colonies[self.colony_location.index.get()];

                    colony_infection_contamination(
                        params,
                        insect_params,
                        bumblebee_params,
                        &mut self.insect.pathogen_status,
                        tick,
                        colony,
                        rng,
                    );

                    self.insect.pollen = Default::default();
                    self.insect.activity = InsectActivity::SearchFlowerPatch(Default::default());
                    self.activity = Activity::Other;
                }
            }
            Activity::Other => {
                if self.insect.pollen >= bumblebee_params.pollen_per_bout {
                    let (direction, distance) =
                        shortest_path(params, self.insect.position, self.colony_location.position);

                    let (sine, cosine) = direction.sin_cos();

                    self.insect.movement = Vector {
                        x: insect_params.speed * cosine,
                        y: insect_params.speed * sine,
                    };

                    let returns_at =
                        tick + params.tick * (distance / insect_params.speed).ceil() as i64;

                    self.insect.activity = InsectActivity::Other;
                    self.activity = Activity::ReturnToColony(returns_at);
                }
            }
        }

        self.insect.update(
            params,
            insect_params,
            flower_patch_locations,
            visible_flower_patch_locations,
            visited_flowers,
            flower_contaminators,
            flower_patch_events,
            tick,
            flower_patches,
            flowers,
            rng,
            next_id,
            contacts,
            vectors,
            temporal_network,
        );
    }
}
\end{code}

\begin{code}
#[derive(Clone, Copy)]
pub struct Colony {
    pub location: ColonyLocation,
    pub species: u8,
    pub capacity: u16,
    pub contaminated: Contaminated,
}

#[derive(Clone, Copy)]
pub struct ColonyLocation {
    pub position: Vector,
    pub index: TaggedIndex,
}

impl Colony {
    pub fn new(params: &Params, location: ColonyLocation, rng: &mut Rng) -> Self {
        let species = params.init.bumblebee_species.sample(rng) as u8;
        let capacity = params.init.colony_capacity(rng);

        Self {
            location,
            species,
            capacity,
            contaminated: Contaminated::Free,
        }
    }

    pub fn update(&mut self, tick: Tick) {
        colony_decontamination(&mut self.contaminated, tick);
    }
}
\end{code}