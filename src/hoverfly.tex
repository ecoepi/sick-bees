\subsubsection{Hoverfly}
\label{hoverfly-submodel}

The hoverfly is one of the three insect families considered in the models. \marginpar{rationale} Compared to the other two it is arguably the simplest submodel as it neither includes central foraging nor shared colonies. Specifically, its reproductive resources called clutches are found in the same way as the flower patches so that there are no additional complications on top of the foraging movement patterns. Therefore, the submodel is useful as a baseline in simulation experiments with solitary bees and bumblebees considered as a refinement.

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[state] (search-clutch) [align=center] {Search\\clutch};
\node[state] (probe-clutch) [right of=search-clutch,align=center] {Probe\\clutch};
\node[state] (lay-eggs) [below of=probe-clutch,align=center] {Lay\\eggs};
\node[state,initial] (other) [below of=search-clutch] {Other};

\path
    (search-clutch) edge [bend right] node {} (probe-clutch)
    (probe-clutch) edge [bend right] node {} (search-clutch)
    (probe-clutch) edge node {} (lay-eggs)
    (lay-eggs) edge node {} (other)
    (other) edge node {} (search-clutch);

\end{tikzpicture}

\caption{Activity diagram of hoverflies}
\label{hoverfly-activity-diagram}

\end{figure}

\begin{hiddencode}
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::ops::ControlFlow;

use rand::{distributions::Distribution, seq::SliceRandom, Rng as _};

use kdtree::{KDTree, Object};
use queue::Queue;

use crate::{
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation},
    insect::{Activity as InsectActivity, Insect, Species as InsectSpecies},
    params::{Params, CLUTCH_MEMORY},
    reduce_lifetime,
    space::{reduce, Vector, WithinDistanceOfLine},
    units::Tick,
    Contacts, FlowerContaminators, NextId, Rng, TaggedIndex, TemporalNetwork, Vectors,
};
\end{hiddencode}

The state variables \marginpar{state variables} of the hoverfly submodel are those of the generic insect submodel detailed in \autoref{insect-submodel}, a current activity which describes the reproductive functions and a fixed-capacity memory of already probed clutches. This time-ordered queue stores the rank and index of clutches which were found to have no spare capacity left when probed last. 

\begin{code}
#[derive(Clone, Copy)]
pub struct Hoverfly {
    pub insect: Insect,
    pub activity: Activity,
    pub memory: Queue<TaggedIndex, CLUTCH_MEMORY>,
}

#[derive(Clone, Copy)]
pub enum Activity {
    SearchClutch(Tick),
    ProbeClutch(TaggedIndex),
    LayEggs(Tick),
    Other,
}
\end{code}

During initialisation \marginpar{initialisation}, a species within the hoverfly family is chosen randomly for each individual insect based on the weights defined in \autoref{init-params}. The current amount of pollen collected is sampled from a uniform distribution in the applicable interval between zero inclusive and the amount of pollen required for another clutch exclusive to facilitate a faster convergence of the model dynamics into an equilibrium state. These two values are then used to initialise the generic insect submodel described in \autoref{insect-submodel}.\footnote{The initial position of the simulated hoverfly is determined during overall initialisation as explained in \autoref{initialisation}.}

\begin{code}
impl Hoverfly {
    pub fn new(params: &Params, id: u32, position: Vector, rng: &mut Rng) -> Self {
        let species = params.init.hoverfly_species.sample(rng);

        let hoverfly_params = &params.hoverflies[species as usize];

        let pollen = hoverfly_params.pollen_per_clutch * rng.gen();

        let insect = Insect::new(
            params,
            &hoverfly_params.insect,
            id,
            position,
            InsectSpecies::Hoverfly(species as u8),
            pollen,
            rng,
        );

        Self {
            insect,
            activity: Activity::Other,
            memory: Default::default(),
        }
    }
\end{code}

The state update \marginpar{state update} directly manages the reproductive activities illustrated in \autoref{hoverfly-activity-diagram}, i.e. searching and probing clutch sites until one with spare capacity for another clutch of eggs is found and then laying them. It also organises the integration of the generic insect submodel which is mediated via the \inlinecode{Other} activity.

\begin{code}
    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        flower_patch_locations: &KDTree<FlowerPatchLocation>,
        visible_flower_patch_locations: &mut Vec<&'static FlowerPatchLocation>,
        visited_flowers: &mut Vec<u16>,
        flower_contaminators: &mut FlowerContaminators,
        flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
        clutch_locations: &KDTree<ClutchLocation>,
        visible_clutch_locations: &mut Vec<&'static ClutchLocation>,
        clutch_events: &mut BinaryHeap<ClutchEvent>,
        tick: Tick,
        flower_patches: &mut [FlowerPatch],
        flowers: &mut [u64],
        clutches: &mut [Clutch],
        rng: &mut Rng,
        next_id: &mut NextId,
        contacts: &mut Contacts,
        vectors: &mut Vectors,
        temporal_network: &mut TemporalNetwork,
    ) {
        let hoverfly_params = match self.insect.species {
            InsectSpecies::Hoverfly(species) => &params.hoverflies[species as usize],
            _ => unreachable!(),
        };

        let insect_params = &hoverfly_params.insect;

        match &mut self.activity {
\end{code}

The search for a possible clutch site is using a similar procedure as the search for a flower patch implemented in the generic insect submodel in \autoref{search-flower-patch}. It does reuse the parameter values for turning angles \inlinecode{insect\_params.direction} and movement speed \inlinecode{insect\_params.speed}, but employs a separate value for sensing distance \inlinecode{hoverfly\_params.vision}.

The only structural difference is that there is currently only one kind of potential clutch site and therefore a purely random choice is made among the visible clutch locations. If a new location was found, the position is updated and probing will happen in the next time step. Otherwise, the search is continued.

\begin{code}
            Activity::SearchClutch(next_turn) => {
                if *next_turn <= tick {
                    let (sine, cosine) = insect_params.direction.sample(rng).sin_cos();

                    self.insect.movement = Vector {
                        x: insect_params.speed * cosine,
                        y: insect_params.speed * sine,
                    };

                    *next_turn = tick + insect_params.between_turns;
                }

                let new_position = self.insect.position + self.insect.movement;

                reduce_lifetime(visible_clutch_locations, |visible_locations| {
                    clutch_locations.look_up(
                        WithinDistanceOfLine::new(
                            self.insect.position,
                            new_position,
                            hoverfly_params.vision,
                        ),
                        |location| {
                            if !self.memory.contains(&location.index) {
                                visible_locations.push(location);
                            }

                            ControlFlow::Continue(())
                        },
                    );

                    if let Some(location) = visible_locations.choose(rng) {
                        self.insect.position = reduce(params, location.position);
                        self.activity = Activity::ProbeClutch(location.index);
                    } else {
                        self.insect.position = reduce(params, new_position);
                    }
                });
            }
\end{code}

The separate step of probing a clutch site is necessary as a relocation could be required between choosing a location and being able to access the associated state by being located in the same subworld.

In this step, the capacity of the clutch site is checked and only if spare capacity is available and a new clutch of eggs is deposited. This mechanism is meant to capture the competition between the adult hoverflies for a finite amount of nutritional resources for their larvae. This involves two time scales: First the time necessary to lay the eggs as determined by \inlinecode{clutch\_laying\_duration}, and second the time required for the clutch of eggs to hatch controlled via \inlinecode{clutch\_hatching\_duration}.

After the hoverfly spent the necessary time at the clutch site to deposit its eggs, the search for flower patches driven by the generic insect submodel is resumed. If there was no spare capacity available at the clutch site, this is memorised and the search for another clutch location is continued.

\begin{code}
            Activity::ProbeClutch(index) => {
                let clutch = &mut clutches[index.get()];

                if clutch.eggs != clutch.capacity {
                    clutch.eggs += 1;

                    clutch_events.push(ClutchEvent::Hatch {
                        happens_at: tick + hoverfly_params.clutch_hatching_duration,
                        clutch: *index,
                    });

                    self.activity =
                        Activity::LayEggs(tick + hoverfly_params.clutch_laying_duration);
                } else {
                    self.memory.push(*index);

                    self.activity = Activity::SearchClutch(Default::default());
                }
            }
            Activity::LayEggs(finishes_at) => {
                if *finishes_at <= tick {
                    self.insect.pollen = Default::default();
                    self.insect.activity = InsectActivity::SearchFlowerPatch(Default::default());
                    self.activity = Activity::Other;
                }
            }
\end{code}

The integration with the generic insect submodel is handled via the \inlinecode{Other} activities of both submodels. If the simulated hoverfly engages in its reproductive functions, the foraging implemented by the insect submodel is suspended by changing its current activity to \inlinecode{Other}, while the hoverfly submodel enters the \inlinecode{SearchClutch} activity. Conversely, the hoverfly enters the \inlinecode{Other} activity and puts the insect submodel into the \inlinecode{SearchFlowerPatch} activity to resume foraging.

\begin{code}
            Activity::Other => {
                if self.insect.pollen >= hoverfly_params.pollen_per_clutch {
                    self.insect.activity = InsectActivity::Other;
                    self.activity = Activity::SearchClutch(Default::default());
                }
            }
        }

        self.insect.update(
            params,
            insect_params,
            flower_patch_locations,
            visible_flower_patch_locations,
            visited_flowers,
            flower_contaminators,
            flower_patch_events,
            tick,
            flower_patches,
            flowers,
            rng,
            next_id,
            contacts,
            vectors,
            temporal_network,
        );
    }
}
\end{code}

The clutch entity \marginpar{clutch entity} represents the availability of the resources required by hoverfly larvae at a certain location. Which kind of resource depends on the species of hoverfly as their larvae use a variety of different habitats and diets \autocite{TODO}, but there is currently no distinction between different resources implemented in the model.

The availability of these resources makes a location a potential site to deposit a clutch of eggs. The amount of available resources determines the capacity, i.e. how many clutches of eggs can mature at the site at any time. The utilisation is represented by the count of currently hatching eggs. \footnote{This is a bit of a misnomer as each increment actually represents a whole clutch of eggs.} This counter is decremented again after the hatching duration has elapsed so that eventually a dynamic equilibrium between newly deposited eggs and hatched eggs is reached.

\begin{code}
#[derive(Clone, Copy)]
pub struct Clutch {
    pub capacity: u16,
    pub eggs: u16,
}
\end{code}

The biological variation in the availability of resources at a clutch site is captured by randomly choosing its capacity during initialisation using the distribution described in \autoref{init-params}.

\begin{code}
impl Clutch {
    pub fn new(params: &Params, rng: &mut Rng) -> Self {
        let capacity = params.init.clutch_capacity(rng);

        Self { capacity, eggs: 0 }
    }
}
\end{code}

As the deposition of eggs is handled in the hoverfly submodel, the \emph{state update} of the clutch entities is limited to their hatching. This is a slow process that happens on the order of days, hence it is implemented using a DES formalism instead of checking for hatched eggs at each time step.

The corresponding event type \inlinecode{ClutchEvent::Hatch} contains a timestamp \inlinecode{happens_at} and the index of the affected clutch site. Processing these events means repeatedly checking the event with the smallest timestamp and applying all those events whose timestamp is less or equal to the current time. Applying an events means reducing the number of clutches of eggs which are currently hatching thereby replenishing the capacity of the clutch site. Processing stops if there are no more pending events or if the next pending event has a timestamp larger than the current time.

\begin{code}
#[derive(Clone, Copy)]
pub enum ClutchEvent {
    Hatch {
        happens_at: Tick,
        clutch: TaggedIndex,
    },
}

impl ClutchEvent {
    pub fn process(events: &mut BinaryHeap<ClutchEvent>, tick: Tick, clutches: &mut [Clutch]) {
        loop {
            match events.peek() {
                Some(Self::Hatch { happens_at, clutch }) if *happens_at <= tick => {
                    clutches[clutch.get()].eggs -= 1;

                    events.pop();
                }
                _ => break,
            }
        }
    }

    fn happens_at(&self) -> Tick {
        match self {
            Self::Hatch { happens_at, .. } => *happens_at,
        }
    }
}
\end{code}

Note that the \inlinecode{BinaryHeap} data structure is max heap but the simulation requires the event with the smallest timestamp. Hence the total order defined by the \inlinecode{Ord} trait is reversed by exchanging the left hand side and the right hand side of the comparison.

\begin{code}
impl PartialEq for ClutchEvent {
    fn eq(&self, other: &Self) -> bool {
        self.happens_at() == other.happens_at()
    }
}

impl Eq for ClutchEvent {}

impl PartialOrd for ClutchEvent {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for ClutchEvent {
    fn cmp(&self, other: &Self) -> Ordering {
        other.happens_at().cmp(&self.happens_at())
    }
}
\end{code}

Clutches are stationary entities and spatially indexed \marginpar{spatial indexing} in the \inlinecode{clutch\_locations} K-D tree used above. To enable indexing clutches located in neighbouring subworlds, the \inlinecode{ClutchLocation} type includes a tagged index which contains both the rank of the process which simulates the clutch as well as the index of the entity state at that process.

\begin{code}
#[derive(Clone, Copy)]
pub struct ClutchLocation {
    pub position: Vector,
    pub index: TaggedIndex,
}

impl Object for ClutchLocation {
    type Point = Vector;

    fn position(&self) -> &Self::Point {
        &self.position
    }
}
\end{code}