\subsection{Pathogen}
\label{pathogen-submodel}

Pathogen transmission \marginpar{rationale} is modelled using a bipartite susceptible-infected-susceptible (SIS) ansatz as illustrated in \autoref{state-transitions-bipartite-sis-model}. The two partitions refer to the two kinds of host organisms involved, i.e. the insects which are infected and the flower patches which are contaminated. Pathogen transmission between insects is mediated by flower patches, i.e. an insect is always indirectly infected by a contaminated flower patch instead of directly by another insect. All state transitions are stochastic using either given event probabilities or exponentially distributed time spans.

These probabilities and rates can be experimentally measured in controlled lab experiments \autocite{bee_pathogen_transmission_dynamics} in the model currently depend on the species of the insect or the species of flower patch, but not on their combination, e.g. specific degrees of susceptibility of the insects or pathogen stability on the flowers can be considered, but the probability of contamination can currently not depend on the pairing between insect and plant species.

The contamination of the flowers within a patch is tracked individually, so the that probability of coming into contact with a contaminated is determined by the overlapping visitation orders of multiple insects at a given flower patch. For example, simulation experiments showed that highly correlated orbits where all insects visit flowers in ascending order significantly increased transmission efficiency in scenarios with high competition.

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=5cm]
\node[matrix] (sus-free) {
    \draw (-0.25,0) circle (0.5);
    \draw (0,0.75) circle (0.25); \\
};

\node[matrix,right of=sus-free] (inf-free) {
    \draw (-0.25,0) circle (0.5);
    \fill (0,0.75) circle (0.25); \\
};

\node[matrix, below of=sus-free] (sus-cont) {
    \fill (-0.25,0) circle (0.5);
    \draw (0,0.75) circle (0.25); \\
};

\node[matrix,right of=sus-cont] (inf-cont) {
    \fill (-0.25,0) circle (0.5);
    \fill (0,0.75) circle (0.25); \\
};

\path
    (inf-free) edge [above] node {recovery} (sus-free)
    (inf-cont) edge [bend right] node[below=10pt] {recovery} (sus-cont)
    (sus-cont) edge [sloped] node[above=10pt] {decontamination} (sus-free)
    (inf-cont) edge [bend left,sloped] node[above=10pt] {decontamination} (inf-free)
    (inf-cont) edge [pos=0.6] node[align=left,fill=white] {decontamination\\and recovery} (sus-free)
    (inf-free) edge [very thick,bend left,sloped] node[above=10pt] {contamination} (inf-cont)
    (sus-cont) edge [very thick,bend right] node[above=10pt] {infection} (inf-cont);

\end{tikzpicture}

\caption{State transitions of the bipartite SIS model}
\label{state-transitions-bipartite-sis-model}

\end{figure}

\begin{hiddencode}
use std::collections::BinaryHeap;
use std::mem::MaybeUninit;

use rand_distr::{Bernoulli, Distribution};

use access::dictionary;

use crate::{
    bumblebee::Colony,
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch},
    insect::Species as InsectSpecies,
    params::{Bumblebee as BumblebeeParams, Insect as InsectParams, Params},
    units::Tick,
    FlowerContaminators, Rng, TaggedIndex, TemporalNetwork, Vectors,
};
\end{hiddencode}

\begin{code}
#[derive(Clone, Copy)]
pub enum Status {
    Susceptible,
    Infected(Tick),
    ChronicallyInfected,
    Resistant,
}

impl Status {
    pub fn is_infected(&self) -> bool {
        matches!(self, Self::Infected(_) | Self::ChronicallyInfected)
    }
}

#[derive(Clone, Copy)]
pub enum Contaminated {
    Free,
    Contaminated(Tick),
}
\end{code}

The state transitions for infection and contamination only apply while the insect forages at a flower patch as described in \autoref{collect-nectar-and-pollen}. The transition from susceptible to infected happens with probability \inlinecode{insect_params.infection}, but only if the currently visited flower is contaminated. When an infected insect visits a flower, it becomes contaminated with probability \inlinecode{insect_params.contamination}.

When an insect is infected or a flower patch is contaminated, the point in time when recovery or decontamination happens is sampled from the exponential distributions \inlinecode{insect_params.recovery} or \inlinecode{flower_patch_params.decontamination}. In the case of the decontamination of a flower, an event is dispatched which will later be applied to the relevant model state as described in \autoref{flower-patch-submodel}.

When an insect is infected, the micro-observable \inlinecode{vectors} is updated to record the chain of infection beginning with the species of the insect to last contaminate the flower patch \inlinecode{flower\_patch.contaminated\_by\_species}, then the species of the flower patch itself \inlinecode{flower\_patch.species} and finally the species of the insect that was just infected \inlinecode{species}. The auxiliary variable \inlinecode{flower\_patch.contaminated\_by\_species} is overwritten by \inlinecode{species} whenever another flower within a patch is contaminated. This is only an approximation as multiple species can contribute to the contamination at a single flower patch, but it should nevertheless enable comparing the relevance of the possible transmission pathways if the number of infection events is sufficiently high.

\begin{code}
#[allow(clippy::too_many_arguments)]
pub fn infection_contamination(
    id: u32,
    params: &Params,
    insect_params: &InsectParams,
    species: InsectSpecies,
    status: &mut Status,
    index: TaggedIndex,
    flower: u16,
    flower_contaminators: &mut FlowerContaminators,
    flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
    tick: Tick,
    flower_patch: &mut FlowerPatch,
    flowers: &mut [u64],
    rng: &mut Rng,
    vectors: &mut Vectors,
    temporal_network: &mut TemporalNetwork,
) {
    let flower_patch_params = &params.flower_patches[flower_patch.species.0 as usize];

\end{code}

The \inlinecode{flower_contaminators} hash table is used to track which individuals have contaminated a given flower within a patch. This is used to derive a temporal network for these epidemiologically relevant contacts by collecting the edges between contaminator and visitor. This tracking is tied to contamination events instead of infection events or just visits as the decontamination time provides the relevant time scale for the temporal network.

\begin{code}
    if params.trace_temporal_network {
        let flower_contaminators = flower_contaminators.entry((index, flower)).or_default();

        flower_contaminators.retain(|(contaminator, decontaminates_at)| {
            if *decontaminates_at > tick {
                temporal_network.edges.push((tick, *contaminator, id));

                true
            } else {
                false
            }
        });

        *dictionary(flower_contaminators, id) =
            tick + flower_patch_params.decontamination(params, rng);
    }

    let is_susceptible = matches!(status, Status::Susceptible);
    let is_infected = status.is_infected();

    let is_contaminated = flower_patch
        .contaminated_flowers
        .get(flowers, flower as usize);

    if is_susceptible && is_contaminated && insect_params.infection.sample(rng) {
        let recovers_at = tick + insect_params.recovery(params, rng);

        *status = Status::Infected(recovers_at);

        *dictionary(
            vectors,
            (
                flower_patch.species,
                unsafe { flower_patch.contaminated_by_species.assume_init() },
                species,
            ),
        ) += 1;
    }

    if is_infected && !is_contaminated && insect_params.contamination.sample(rng) {
        flower_patch_events.push(FlowerPatchEvent::Decontamination {
            happens_at: tick + flower_patch_params.decontamination(params, rng),
            flower_patch: index,
            flower,
        });

        flower_patch
            .contaminated_flowers
            .set(flowers, flower as usize, true);

        flower_patch.contaminated_by_species = MaybeUninit::new(species);
    }
}
\end{code}

As for the decontamination of flowers, the recovery of insects is spontaneous and happens after a stochastically predetermined time span. In contrast to the DES formalism applied to the flowers, this state transition is checked every time step by the insect submodel independently of whether the insect currently visits a flower patch or not.

\begin{code}
pub enum RecoveryStatus {
    Unaffected,
    WillRecover,
    JustRecovered,
}

pub fn recovery(status: &mut Status, tick: Tick) -> RecoveryStatus {
    match status {
        Status::Infected(recovers_at) => {
            if *recovers_at <= tick {
                *status = Status::Susceptible;

                RecoveryStatus::JustRecovered
            } else {
                RecoveryStatus::WillRecover
            }
        }
        _ => RecoveryStatus::Unaffected,
    }
}
\end{code}

\phantomsection
\label{pathogen-initialisation}

The initial epidemiological status of the insects is determined using pluggable strategies which encode different assumptions on how the pathogen is introduced into the community.

\begin{code}
pub trait Infection {
    fn status(
        &self,
        species: InsectSpecies,
        params: &Params,
        insect_params: &InsectParams,
        rng: &mut Rng,
    ) -> Status;
}
\end{code}

The first strategy is a uniform pathogen release which does not impose any constraints on the transmission dynamic captured by the model and yields a faster convergence into a situation of endemic equilibrium compared to e.g an individual release.

\begin{code}
pub struct InitialOverallPrevalence {
    prevalence: Bernoulli,
}

impl InitialOverallPrevalence {
    pub fn new(prevalence: f64) -> Self {
        Self {
            prevalence: Bernoulli::new(prevalence).unwrap(),
        }
    }
}

impl Infection for InitialOverallPrevalence {
    fn status(
        &self,
        _species: InsectSpecies,
        params: &Params,
        insect_params: &InsectParams,
        rng: &mut Rng,
    ) -> Status {
        if self.prevalence.sample(rng) {
            let recovers_at = insect_params.recovery(params, rng);

            Status::Infected(recovers_at)
        } else {
            Status::Susceptible
        }
    }
}
\end{code}

If one species is assumed to be a reservoir host of a pathogen and that its prevalence is determined by mechanisms outside of the model scope, e.g. honey bees spreading diseases which a transmitted mainly within their hives, a fixed specific prevalence can be used to constantly introduce the pathogen into the community without any feedback effects.

\begin{code}
pub struct FixedSpecificPrevalence {
    species: InsectSpecies,
    prevalence: Bernoulli,
}

impl FixedSpecificPrevalence {
    pub fn new(species: InsectSpecies, prevalence: f64) -> Self {
        Self {
            species,
            prevalence: Bernoulli::new(prevalence).unwrap(),
        }
    }
}

impl Infection for FixedSpecificPrevalence {
    fn status(
        &self,
        species: InsectSpecies,
        _params: &Params,
        _insect_params: &InsectParams,
        rng: &mut Rng,
    ) -> Status {
        if species == self.species {
            if self.prevalence.sample(rng) {
                Status::ChronicallyInfected
            } else {
                Status::Resistant
            }
        } else {
            Status::Susceptible
        }
    }
}
\end{code}

The following strategies describe intermediate approaches between the intrinsic and the externally fixed prevalence values by imposing upper or lower bounds on the prevalence values while still allowing for spill-back transmission into the focal species. However, these bounds will usually not be strict bounds even if only one species is considered due additional within-species circulation.

\begin{code}
pub struct MinimumSpecificPrevalence {
    species: InsectSpecies,
    prevalence: Bernoulli,
}

impl MinimumSpecificPrevalence {
    pub fn new(species: InsectSpecies, prevalence: f64) -> Self {
        Self {
            species,
            prevalence: Bernoulli::new(prevalence).unwrap(),
        }
    }
}

impl Infection for MinimumSpecificPrevalence {
    fn status(
        &self,
        species: InsectSpecies,
        _params: &Params,
        _insect_params: &InsectParams,
        rng: &mut Rng,
    ) -> Status {
        if species == self.species {
            if self.prevalence.sample(rng) {
                Status::ChronicallyInfected
            } else {
                Status::Susceptible
            }
        } else {
            Status::Susceptible
        }
    }
}

pub struct MaximumSpecificPrevalence {
    species: InsectSpecies,
    prevalence: Bernoulli,
}

impl MaximumSpecificPrevalence {
    pub fn new(species: InsectSpecies, prevalence: f64) -> Self {
        Self {
            species,
            prevalence: Bernoulli::new(prevalence).unwrap(),
        }
    }
}

impl Infection for MaximumSpecificPrevalence {
    fn status(
        &self,
        species: InsectSpecies,
        params: &Params,
        insect_params: &InsectParams,
        rng: &mut Rng,
    ) -> Status {
        if species == self.species {
            if self.prevalence.sample(rng) {
                let recovers_at = insect_params.recovery(params, rng);

                Status::Infected(recovers_at)
            } else {
                Status::Resistant
            }
        } else {
            Status::Susceptible
        }
    }
}
\end{code}

\subsubsection{Bumblebee colony}

\begin{code}
#[allow(clippy::too_many_arguments)]
pub fn colony_infection_contamination(
    params: &Params,
    insect_params: &InsectParams,
    bumblebee_params: &BumblebeeParams,
    status: &mut Status,
    tick: Tick,
    colony: &mut Colony,
    rng: &mut Rng,
) {
    let is_susceptible = matches!(status, Status::Susceptible);
    let is_infected = status.is_infected();

    let is_contaminated = matches!(colony.contaminated, Contaminated::Contaminated(_));

    if is_susceptible && is_contaminated && bumblebee_params.colony_infection.sample(rng) {
        let recovers_at = tick + insect_params.recovery(params, rng);

        *status = Status::Infected(recovers_at);
    }

    if is_infected && !is_contaminated && bumblebee_params.colony_contamination.sample(rng) {
        let decontaminates_at = tick + bumblebee_params.colony_decontamination(params, rng);

        colony.contaminated = Contaminated::Contaminated(decontaminates_at);
    }
}
\end{code}

\begin{code}
pub fn colony_decontamination(contaminated: &mut Contaminated, tick: Tick) {
    match contaminated {
        Contaminated::Contaminated(decontaminates_at) if *decontaminates_at <= tick => {
            *contaminated = Contaminated::Free;
        }
        _ => (),
    }
}
\end{code}