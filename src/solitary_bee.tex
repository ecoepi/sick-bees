\subsubsection{Solitary bee}
\label{solitary-bee-submodel}

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[state] (search-nest) [align=center] {Search\\nest};
\node[state] (probe-nest) [right of=search-nest,align=center] {Probe\\nest};
\node[state] (return-to-nest) [below of=search-nest,align=center] {Return\\to nest};
\node[state] (build-cell) [below of=probe-clutch,align=center] {Build\\cell};
\node[state,initial] (other) [below of=return-to-nest] {Other};

\path
    (search-nest) edge [bend right] node {} (probe-nest)
    (probe-nest) edge [bend right] node {} (search-nest)
    (probe-nest) edge node {} (build-cell)
    (return-to-nest) edge node {} (build-cell)
    (build-cell) edge node {} (other)
    (other) edge [bend left] node {} (search-nest)
    (other) edge node {} (return-to-nest);

\end{tikzpicture}

\caption{Activity diagram of solitary bees}

\end{figure}

\begin{hiddencode}
use std::collections::BinaryHeap;
use std::ops::ControlFlow;

use rand::{distributions::Distribution, seq::SliceRandom, Rng as _};

use kdtree::{KDTree, Object};
use queue::Queue;

use crate::{
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation},
    insect::{Activity as InsectActivity, Insect, Species as InsectSpecies},
    params::{Params, NEST_MEMORY},
    reduce_lifetime,
    space::{reduce, shortest_path, Vector, WithinDistanceOfLine},
    units::Tick,
    Contacts, FlowerContaminators, NextId, Rng, TaggedIndex, TemporalNetwork, Vectors,
};
\end{hiddencode}

\begin{code}
#[derive(Clone, Copy)]
pub struct SolitaryBee {
    pub insect: Insect,
    pub activity: Activity,
    pub memory: Queue<TaggedIndex, NEST_MEMORY>,
    pub nest_location: Option<NestLocation>,
}

#[derive(Clone, Copy)]
pub enum Activity {
    SearchNest(Tick),
    ProbeNest(TaggedIndex),
    ReturnToNest(Tick),
    BuildCell(Tick),
    Other,
}
\end{code}

\begin{code}
impl SolitaryBee {
    pub fn new(params: &Params, id: u32, position: Vector, rng: &mut Rng) -> Self {
        let species = params.init.solitary_bee_species.sample(rng);

        let solitary_bee_params = &params.solitary_bees[species as usize];

        let pollen = solitary_bee_params.pollen_per_brood_cell * rng.gen();

        let insect = Insect::new(
            params,
            &solitary_bee_params.insect,
            id,
            position,
            InsectSpecies::SolitaryBee(species as u8),
            pollen,
            rng,
        );

        Self {
            insect,
            activity: Activity::Other,
            memory: Default::default(),
            nest_location: None,
        }
    }

    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        flower_patch_locations: &KDTree<FlowerPatchLocation>,
        visible_flower_patch_locations: &mut Vec<&'static FlowerPatchLocation>,
        visited_flowers: &mut Vec<u16>,
        flower_contaminators: &mut FlowerContaminators,
        flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
        nest_locations: &KDTree<NestLocation>,
        visible_nest_locations: &mut Vec<&'static NestLocation>,
        tick: Tick,
        flower_patches: &mut [FlowerPatch],
        flowers: &mut [u64],
        nests: &mut [Nest],
        rng: &mut Rng,
        next_id: &mut NextId,
        contacts: &mut Contacts,
        vectors: &mut Vectors,
        temporal_network: &mut TemporalNetwork,
    ) {
        let solitary_bee_params = match self.insect.species {
            InsectSpecies::SolitaryBee(species) => &params.solitary_bees[species as usize],
            _ => unreachable!(),
        };

        let insect_params = &solitary_bee_params.insect;

        match self.activity {
            Activity::SearchNest(next_turn) => {
                if next_turn <= tick {
                    let (sine, cosine) = insect_params.direction.sample(rng).sin_cos();

                    self.insect.movement = Vector {
                        x: insect_params.speed * cosine,
                        y: insect_params.speed * sine,
                    };

                    self.activity = Activity::SearchNest(tick + insect_params.between_turns);
                }

                let new_position = self.insect.position + self.insect.movement;

                reduce_lifetime(visible_nest_locations, |visible_locations| {
                    nest_locations.look_up(
                        WithinDistanceOfLine::new(
                            self.insect.position,
                            new_position,
                            solitary_bee_params.vision,
                        ),
                        |location| {
                            if !self.memory.contains(&location.index) {
                                visible_locations.push(location);
                            }

                            ControlFlow::Continue(())
                        },
                    );

                    if let Some(location) = visible_locations.choose(rng) {
                        self.insect.position = reduce(params, location.position);
                        self.activity = Activity::ProbeNest(location.index);
                    } else {
                        self.insect.position = reduce(params, new_position);
                    }
                });
            }
            Activity::ProbeNest(index) => {
                let nest = &mut nests[index.get()];

                if !nest.occupied {
                    nest.occupied = true;

                    self.nest_location = Some(NestLocation {
                        position: self.insect.position,
                        index,
                    });
                    self.activity =
                        Activity::BuildCell(tick + solitary_bee_params.cell_building_duration);
                } else {
                    self.memory.push(index);
                    self.activity = Activity::SearchNest(Default::default());
                }
            }
            Activity::ReturnToNest(returns_at) => {
                if returns_at <= tick {
                    self.insect.position = self.nest_location.unwrap().position;
                    self.activity =
                        Activity::BuildCell(tick + solitary_bee_params.cell_building_duration);
                } else {
                    self.insect.position =
                        reduce(params, self.insect.position + self.insect.movement);
                }
            }
            Activity::BuildCell(finishes_at) => {
                if finishes_at <= tick {
                    let nest = &mut nests[self.nest_location.unwrap().index.get()];

                    nest.brood_cells += 1;

                    if nest.brood_cells == solitary_bee_params.brood_cells_per_nest {
                        self.nest_location = None;
                    }

                    self.insect.pollen = Default::default();
                    self.insect.activity = InsectActivity::SearchFlowerPatch(Default::default());
                    self.activity = Activity::Other;
                }
            }
            Activity::Other => {
                if self.insect.pollen >= solitary_bee_params.pollen_per_brood_cell {
                    self.insect.activity = InsectActivity::Other;

                    if let Some(nest_location) = self.nest_location {
                        let (direction, distance) =
                            shortest_path(params, self.insect.position, nest_location.position);

                        let (sine, cosine) = direction.sin_cos();

                        self.insect.movement = Vector {
                            x: insect_params.speed * cosine,
                            y: insect_params.speed * sine,
                        };

                        let returns_at =
                            tick + params.tick * (distance / insect_params.speed).ceil() as i64;

                        self.activity = Activity::ReturnToNest(returns_at);
                    } else {
                        self.activity = Activity::SearchNest(Default::default());
                    }
                }
            }
        }

        self.insect.update(
            params,
            insect_params,
            flower_patch_locations,
            visible_flower_patch_locations,
            visited_flowers,
            flower_contaminators,
            flower_patch_events,
            tick,
            flower_patches,
            flowers,
            rng,
            next_id,
            contacts,
            vectors,
            temporal_network,
        );
    }
}
\end{code}

\begin{code}
#[derive(Clone, Copy)]
pub struct Nest {
    pub occupied: bool,
    pub brood_cells: u8,
}

impl Nest {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            occupied: false,
            brood_cells: 0,
        }
    }
}
\end{code}

\begin{code}
#[derive(Clone, Copy)]
pub struct NestLocation {
    pub position: Vector,
    pub index: TaggedIndex,
}

impl Object for NestLocation {
    type Point = Vector;

    fn position(&self) -> &Self::Point {
        &self.position
    }
}
\end{code}