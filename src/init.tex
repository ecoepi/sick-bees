\section{Initialisation}
\label{initialisation}

\begin{hiddencode}
use std::collections::BinaryHeap;

use rand::distributions::{Distribution, Uniform};
use rand::{Rng as _, SeedableRng};

use kdtree::KDTree;
use process::MailBoxes;
use raster::Raster;

use crate::{
    bumblebee::{Bumblebee, Colony, ColonyLocation},
    flower_patch::{Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation},
    grid::Grid,
    hoverfly::{Clutch, ClutchLocation, Hoverfly},
    observer::{close_to_transect, Observer},
    params::Params,
    solitary_bee::{Nest, NestLocation, SolitaryBee},
    space::{mirror, Vector},
    units::Length,
    CryptoRng, Model, NextId, Rng, TaggedIndex, TemporalNetwork, Tick, World,
};
\end{hiddencode}

The model is initialised independently by each process to avoid having to communicate its results from a chosen process to the other processes. To ensure a consistent initial model state all processes start with the same random seed and perform the same operations on the PRNG state even though entities that are located at other processes are discarded.

The seeding is performed in a two step process using a cryptographically secure pseudo-random number generator (CSPRNG) based on the ChaCha stream cipher \autocite{chacha_variant_salsa20} as the parent generator to seed the PRNG used for initialisation and for parallel simulation of the subworlds. This ensures that artificial correlations between the PRNG instances due to their internal structure are highly unlikely due to the avalance criterion fulfilled by the CSPRNG. \autocite{parallel_random_numbers}

\begin{code}
impl Model {
    #[cold]
    pub fn new(params: Params, rank: u8, size: u8) -> Self {
        let mut parent_rng = CryptoRng::seed_from_u64(params.init.seed);
        let mut rng = Rng::from_rng(&mut parent_rng).unwrap();
\end{code}

\phantomsection
\label{random-placement}

The initial positions of all entities are chosen at random, either uniformly distributed in the simulated landscape or based on a map which gives a weight to each position as to determine the likelihood of an entity being positioned there via rejection sampling \autocite{monte_carlo_statistical_methods}. This can for example be used determine the distribution of floral resources using GIS data on land use to study the effects of spatial structures on foraging behaviour and thereby pathogen transmission.

Note that the weights given by the map are also used to optimise the subdivison of the simulated landscape described in \autoref{grid} with the goal of ensuring a balanced distribution of weight handled by the various processes improving computation efficiency.

\begin{code}
        let landscape = params
            .init
            .landscape
            .as_ref()
            .map(|path| unsafe { Raster::open(path) });

        let grid = Grid::new(&params, landscape.as_ref(), rank, size);

        let position = {
            let xy = Uniform::new(0., params.length.as_meters());

            move |rng: &mut Rng| {
                let x = Length::meters(xy.sample(rng));
                let y = Length::meters(xy.sample(rng));

                Vector { x, y }
            }
        };

        let position: Box<dyn Fn(&mut Rng) -> Vector> = if let Some(landscape) = landscape {
            let length = params.length;

            Box::new(move |rng| loop {
                let position = position(rng);

                if landscape.get(position.x / length, position.y / length) > rng.gen() {
                    return position;
                }
            })
        } else {
            Box::new(position)
        };
\end{code}

The \emph{flower patches} are stationary entities which are positioned randomly and assigned to a specific process \inlinecode{dest} based on their position.

The effective area of a single flower patch is computed based on the area of the simulated landscape and the number of simulated flower patches. This value is used to determine the number of flowers per patch and the nectar produced per patch and day based on empirical densities measured by the AgriLand project \autocite{agriland_flower_density} \autocite{agriland_nectar}.

The \inlinecode{flower\_patch\_cover} parameter involved here controls the fraction of the overall surface area that is considered vegetative cover and is thereby a proxy for the overall nectar productivity resp. scarcity of floral resources of the simulated landscape, i.e. it uniformly modifies the amount of resources available at each flower patch similar to the boost parameter used in the Bumble-BEEHAVE model \autocite{bumble_beehave}.

Note that the number of simulated flower patches does not influence the amount of available floral resources\footnote{Increasing the number of flower patches would decrease the effective area and hence productivity of a single flower patch thereby yielding the same overall productivity.} but rather determines the number density of flower patches and hence the difficulty of randomly discovering them. Also note that flower patches with zero flowers or no nectar productivity are not included in the simulation as they would neither support the collection of resources nor the possibility of pathogen transmission.

Like most stationary entities, flower patch are indexed using a K-D tree to accelerate spatial queries performed by the mobile entities to explore their surroundings. This process not only considers entites located at the current process but also those which are visible from the current subworld but located at neighbouring subworlds. This enables exploration to lead mobile entities from one subworld to another by seamlessly overlapping the spatial indexes managed by each process. The amount of overlap necessary is determined by the maximum vision over all insect families and species.

These spatial indexes do not contain the actual positions but mirror locations as described in \autoref{mirror-locations} which accounts for the periodic boundary conditions. This implies that a given entity can be associated with multiple entries even in a single index if its mirror locations are visible from the given subworld. It also means that any locations yielded by spatial queries need to be reduced before being related to actual positions.

As entities are identified by the rank of the process they are located at and the index within the list of entities of the same type managed by that processed, the number of entities allocated to the each subworld is tracked even for entities that are discarded by the current process using the \inlinecode{flower\_patch\_indices} vector.

\begin{code}
        let mut flower_patches = Vec::new();
        let mut flower_patch_locations = Vec::new();
        let mut flower_patch_indices = vec![0; size as usize];

        let mut flowers = Vec::new();
        let mut flowers_len = 0;

        let mut flower_patch_events = BinaryHeap::new();
        let mut refills_at = None;

        let close_to_transect = close_to_transect(&params);
        let mut flower_patch_locations_close_to_transect = Vec::new();

        let flower_patch_area = params.length * params.length * params.init.flower_patch_cover
            / params.init.flower_patches as f64;

        let max_vision = params
            .hoverflies
            .iter()
            .map(|hoverfly| &hoverfly.insect)
            .chain(
                params
                    .solitary_bees
                    .iter()
                    .map(|solitary_bee| &solitary_bee.insect),
            )
            .chain(params.bumblebees.iter().map(|bumblebee| &bumblebee.insect))
            .map(|insect| insect.vision + insect.speed)
            .max_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap())
            .unwrap();

        for _ in 0..params.init.flower_patches {
            let flower_patch = FlowerPatch::new(
                &params,
                flower_patch_area,
                &mut refills_at,
                &mut flowers,
                &mut rng,
            );

            if flower_patch.flowers == 0 || flower_patch.nectar_production == Default::default() {
                flowers.truncate(flowers_len);
                continue;
            }

            let position = position(&mut rng);
            let dest = grid.location(&position);
            let index = TaggedIndex::new(dest, flower_patch_indices[dest as usize]);

            for position in mirror(&params, position) {
                if grid.is_visible(max_vision, &position) {
                    flower_patch_locations.push(FlowerPatchLocation {
                        position,
                        index,
                        species: flower_patch.species,
                    });
                }
            }

            if close_to_transect(&position) {
                flower_patch_locations_close_to_transect.push(FlowerPatchLocation {
                    position,
                    index,
                    species: flower_patch.species,
                });
            }

            if dest == rank {
                if let Some(refills_at) = refills_at {
                    flower_patch_events.push(FlowerPatchEvent::Refill {
                        happens_at: refills_at,
                        flower_patch: index,
                    });
                }

                flower_patches.push(flower_patch);
                flowers_len = flowers.len();
            } else {
                flowers.truncate(flowers_len);
            }

            flower_patch_indices[dest as usize] += 1;
        }
\end{code}

Each individual insect is assigned a unique identifier so that its contacts can be traced through the subworlds.

\begin{code}
        let mut temporal_network = TemporalNetwork::default();
        let mut id = 0;
\end{code}

Hoverflies are mobile entities which perform their reproductive activities using the stationary clutch entities. Clutches are placed randomly, indexed spatially and discard if they do not support at least a single clutch of eggs in the same manner as the flower patches.

\begin{code}
        let mut hoverflies = Vec::new();

        for _ in 0..params.init.hoverflies {
            let hoverfly = Hoverfly::new(&params, id, position(&mut rng), &mut rng);

            let dest = grid.location(&hoverfly.insect.position);

            if dest == rank {
                hoverflies.push(hoverfly);

                if params.trace_temporal_network {
                    temporal_network.node_additions.push((
                        Tick::default(),
                        id,
                        hoverfly.insect.species,
                    ));
                }
            }

            id += 1;
        }

        let mut clutches = Vec::new();
        let mut clutch_locations = Vec::new();
        let mut clutch_indices = vec![0; size as usize];

        let max_vision = params
            .hoverflies
            .iter()
            .map(|hoverfly| hoverfly.vision + hoverfly.insect.speed)
            .max_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap())
            .unwrap_or_default();

        for _ in 0..params.init.clutches {
            let clutch = Clutch::new(&params, &mut rng);

            if clutch.capacity == 0 {
                continue;
            }

            let position = position(&mut rng);
            let dest = grid.location(&position);

            for position in mirror(&params, position) {
                if grid.is_visible(max_vision, &position) {
                    clutch_locations.push(ClutchLocation {
                        position,
                        index: TaggedIndex::new(dest, clutch_indices[dest as usize]),
                    });
                }
            }

            if dest == rank {
                clutches.push(clutch);
            }

            clutch_indices[dest as usize] += 1;
        }
\end{code}

Solitary bees are mobile entities which perform their reproductive activities using the stationary nest entities. Nests are placed randomly and indexed spatially in the same manner as the flower patches.

\begin{code}
        let mut solitary_bees = Vec::new();

        for _ in 0..params.init.solitary_bees {
            let solitary_bee = SolitaryBee::new(&params, id, position(&mut rng), &mut rng);

            let dest = grid.location(&solitary_bee.insect.position);

            if dest == rank {
                solitary_bees.push(solitary_bee);

                if params.trace_temporal_network {
                    temporal_network.node_additions.push((
                        Tick::default(),
                        id,
                        solitary_bee.insect.species,
                    ));
                }
            }

            id += 1;
        }

        let mut nests = Vec::new();
        let mut nest_locations = Vec::new();
        let mut nest_indices = vec![0; size as usize];

        let max_vision = params
            .solitary_bees
            .iter()
            .map(|solitary_bee| solitary_bee.vision + solitary_bee.insect.speed)
            .max_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap())
            .unwrap_or_default();

        for _ in 0..params.init.nests {
            let nest = Nest::new();

            let position = position(&mut rng);
            let dest = grid.location(&position);

            for position in mirror(&params, position) {
                if grid.is_visible(max_vision, &position) {
                    nest_locations.push(NestLocation {
                        position,
                        index: TaggedIndex::new(dest, nest_indices[dest as usize]),
                    });
                }
            }

            if dest == rank {
                nests.push(nest);
            }

            nest_indices[dest as usize] += 1;
        }
\end{code}

Bumblebees are mobile entities which are always associated to single colony. Hence the initialisation is a two step procedure where first a colony is initialised, positioned randomly and discarded if it is unable to support at least one bumblebee. Then, the associated number of bumblebees is initialised at the same position as and being of the species as the current colony.

\begin{code}
        let mut colonies = Vec::new();
        let mut bumblebees = Vec::new();

        for _ in 0..params.init.colonies {
            let location = ColonyLocation {
                position: position(&mut rng),
                index: TaggedIndex::new(rank, colonies.len() as u32),
            };

            let colony = Colony::new(&params, location, &mut rng);

            if colony.capacity == 0 {
                continue;
            }

            let dest = grid.location(&colony.location.position);

            if dest == rank {
                colonies.push(colony);
            }

            for _ in 0..colony.capacity {
                let bumblebee =
                    Bumblebee::new(&params, id, colony.location, colony.species, &mut rng);

                if dest == rank {
                    bumblebees.push(bumblebee);

                    if params.trace_temporal_network {
                        temporal_network.node_additions.push((
                            Tick::default(),
                            id,
                            bumblebee.insect.species,
                        ));
                    }
                }

                id += 1;
            }
        }
\end{code}

The observers are mobile entities which are initially positioned along a transect defined by the parameters given in \autoref{transect-params}. Their initial positions are chosen based on the presence of flower patches close to the transect where foraging insect can be caught. To this end, the temporary spatial index \inlinecode{flower_patch_locations_close_to_transect} covering all of the subworlds is constructed as each process places all observers but keeps only those located within its subworld.

\begin{code}
        let flower_patch_locations_close_to_transect =
            KDTree::new(flower_patch_locations_close_to_transect.into());

        let mut observers =
            Observer::new(&params, &flower_patch_locations_close_to_transect, &mut rng);

        observers.retain(|observer| {
            let dest = grid.location(&observer.position);

            dest == rank
        });
\end{code}

Each subworld is provided with a PRNG seeded from a cryptographically-secure RNG to avoid artificial correlations between the streams of pseudo-random numbers.

\begin{code}
        for _ in 0..rank {
            rng = Rng::from_rng(&mut parent_rng).unwrap();
        }
\end{code}

Finally, an associative set of lists of messages used to relocate mobile entities between subworlds as explained in \autoref{process-overview} is allocated for each neighbouring process as determined by the spatial subdivision described in \autoref{grid}.

\begin{code}
        let visited_flowers = [
            vec![Vec::new(); hoverflies.len()],
            vec![Vec::new(); solitary_bees.len()],
            vec![Vec::new(); bumblebees.len()],
        ];

        let max_speed = params
            .hoverflies
            .iter()
            .map(|hoverfly| &hoverfly.insect)
            .chain(
                params
                    .solitary_bees
                    .iter()
                    .map(|solitary_bee| &solitary_bee.insect),
            )
            .chain(params.bumblebees.iter().map(|bumblebee| &bumblebee.insect))
            .map(|insect| insect.speed)
            .max_by(|lhs, rhs| lhs.partial_cmp(rhs).unwrap())
            .unwrap();

        let relocations = MailBoxes::new(
            (0..size).filter(|dest| *dest != rank && grid.is_reachable(&params, max_speed, *dest)),
        );

        Self {
            params,
            world: World {
                tick: Default::default(),
                flower_patches: flower_patches.into(),
                flowers: flowers.into(),
                hoverflies,
                clutches: clutches.into(),
                solitary_bees,
                nests: nests.into(),
                bumblebees,
                colonies: colonies.into(),
                observers,
                rng,
                next_id: NextId::new(id, rank, size),
                observations: Default::default(),
                contacts: Default::default(),
                vectors: Default::default(),
                temporal_network,
            },
            grid,
            flower_patch_locations: KDTree::new(flower_patch_locations.into()),
            visible_flower_patch_locations: Default::default(),
            visited_flowers,
            flower_contaminators: Default::default(),
            flower_patch_events,
            clutch_locations: KDTree::new(clutch_locations.into()),
            visible_clutch_locations: Default::default(),
            clutch_events: Default::default(),
            nest_locations: KDTree::new(nest_locations.into()),
            visible_nest_locations: Default::default(),
            relocations,
        }
    }
}
\end{code}