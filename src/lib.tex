\section{Entities, state variables, and scales}
\label{state-variables}

The model considers a landscape of $1 ~\mathrm{km}^2$ with continuous spatial coordinates and discrete time steps of $1 ~\mathrm{s}$ for durations of several months, i.e. on the order of $10^7$ time steps per run. The simulation is usually populated with around $10^5$ flower patches and $10^4$ insects of three different families, i.e. hoverflies, solitary bees and bumblebees.

While the overall scheduling uses discrete time steps, some submodels use a discrete event simulation (DES) approach \autocite{theory_of_modeling_and_simulation} to reduce the computational costs implied by a discrete time step simulation (DTSS) of slow changes like the refilling of flower patches and the decontamination of flowers. These processes operate on time scales of hours instead of seconds and are therefore amenable to a quantised approach, i.e. in each time step, all events with a timestamp that is less or equals to the current time are applied to the relevant model state.

Periodic boundary conditions are used as reflecting boundary conditions were found to measurably concentrate insects in the centre of the landscape. This effect was significant enough to turn a given scenario from guaranteed die out of the pathogen into endemic equilibrium.

\begin{hiddencode}
pub mod bumblebee;
pub mod flower_patch;
pub mod grid;
pub mod hoverfly;
pub mod init;
pub mod insect;
pub mod observer;
pub mod params;
pub mod pathogen;
pub mod solitary_bee;
pub mod space;
pub mod units;

use std::collections::BinaryHeap;
use std::mem::transmute;

use hashbrown::HashMap;
pub use rand_chacha::ChaCha8Rng as CryptoRng;
pub use rand_pcg::Pcg64 as Rng;

use kdtree::KDTree;
use process::{MailBoxes, Message, Process};

use crate::{
    bumblebee::{Bumblebee, Colony},
    flower_patch::{
        Event as FlowerPatchEvent, FlowerPatch, Location as FlowerPatchLocation,
        Species as FlowerPatchSpecies,
    },
    grid::Grid,
    hoverfly::{Clutch, ClutchEvent, ClutchLocation, Hoverfly},
    insect::Species as InsectSpecies,
    observer::{Observation, Observer},
    params::Params,
    solitary_bee::{Nest, NestLocation, SolitaryBee},
    units::Tick,
};
\end{hiddencode}

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=3cm]

\node[draw,ellipse] (hoverflies) {Hoverflies};
\node[draw] (clutches) [right of=hoverflies] {Clutches};

\node[draw,ellipse] (solitary-bees) [right of=clutches] {Solitary bees};
\node[draw] (nests) [right of=solitary-bees] {Nests};

\node[draw,ellipse] (bumblebees) [below of=solitary-bees] {Bumblebees};
\node[draw] (colonies) [right of=bumblebees] {Colonies};

\node[draw] (flower-patches) [below of=hoverflies] {Flower patches};

\node[draw,ellipse] (observer) [left of=hoverflies] {Observer};

\path
    (hoverflies) edge node [below,sloped] {$n:m$} (flower-patches)
    (hoverflies) edge node [below] {$n:m$} (clutches)
    (solitary-bees) edge node [above,sloped] {$n:m$} (flower-patches)
    (solitary-bees) edge node [below] {$1:n$} (nests)
    (bumblebees) edge node [below] {$n:m$} (flower-patches)
    (bumblebees) edge node [below] {$n:1$} (colonies)
    (observer) edge [bend left] node [below] {$n:m$} (hoverflies)
    (observer) edge [bend left] node [below] {$n:m$} (solitary-bees)
    (observer) edge node [above,sloped] {$n:m$} (bumblebees);

\end{tikzpicture}

\caption{Entities and their interactions including cardinalities}

\end{figure}

\begin{figure}

\centering

\begin{tikzpicture}[scale=3]

\draw (0,0) rectangle (1,1);
\draw (1,0) rectangle (2,1);
\draw (0,1) rectangle (1,2);
\draw (1,1) rectangle (2,2);

\node at (0.5,0.5) {rank 0};
\node at (1.5,0.5) {rank 1};
\node at (0.5,1.5) {rank 2};
\node at (1.5,1.5) {rank 3};

\draw[dashed] (-0.1,-0.1) rectangle (1.1,1.1);
\draw[dashed] (0.9,-0.1) rectangle (2.1,1.1);
\draw[dashed] (-0.1,0.9) rectangle (1.1,2.1);
\draw[dashed] (0.9,0.9) rectangle (2.1,2.1);

\end{tikzpicture}

\caption{Subdivision of landscape for distributed simulation by communicating processes}

\end{figure}

The model is implemented as a distributed simulation using communicating processes. This organisation improves computational efficiency as all interactions between entities are indirect, i.e. insect-insect interactions are mediated via flower patches. As the flower patches are stationary and are assigned to processes during initialisation as described in \autoref{initialisation}, this removes any synchronisation overhead on the simulation of insect-flower-patch interactions.

Each process keeps track of the state variables of its subworld \inlinecode{world}, the free parameters of the various submodels \inlinecode{params} and a data structure describing the spatial subdivision of the simulated landscape \inlinecode{grid}. A process also manages the heaps \inlinecode{flower_patch_events} and \inlinecode{clutch_events} for the timestamped events driving the submodels which use a DES-based formalism. This data structure allows the fast retrieval of the event with the smallest timestamp without the overhead imposed by a fully sorted arrangement.

It also contains several auxiliary data structures which are used during the simulation but are not strictly speaking part of the state of the model, i.e. the \inlinecode{relocations} which contain mobile entities received from and sent to other processes and several K-D trees \inlinecode{flower_patch_locations}, \inlinecode{clutch_locations} and \inlinecode{nest_locations} used to speed up spatial queries for stationary entities within or near to this subworld. It also manages a temporary buffer for each K-D tree which is used to collect and shuffle query results to avoid artificial correlations due to the organisation of entity locations in the search trees.

Furthermore, the \inlinecode{visited_flowers} buffers are used to store the trajectories each insect takes along the flowers within a patch which are stored independently of the state of the insects as they are never relocated to another subworld as a relocation implies not being within a flower patch.

Finally, the \inlinecode{flower_contaminators} hash table is used to determine which individual is responsible for contaminating a flower wihtin a patch and is used to trace the temporal structure of potentially epidemiologically relevant contacts.

\begin{code}
pub struct Model {
    pub params: Params,
    pub world: World,
    pub grid: Grid,
    pub flower_patch_locations: KDTree<FlowerPatchLocation>,
    visible_flower_patch_locations: Vec<&'static FlowerPatchLocation>,
    visited_flowers: [Vec<Vec<u16>>; 3],
    flower_contaminators: FlowerContaminators,
    flower_patch_events: BinaryHeap<FlowerPatchEvent>,
    pub clutch_locations: KDTree<ClutchLocation>,
    visible_clutch_locations: Vec<&'static ClutchLocation>,
    clutch_events: BinaryHeap<ClutchEvent>,
    pub nest_locations: KDTree<NestLocation>,
    visible_nest_locations: Vec<&'static NestLocation>,
    relocations: MailBoxes<Relocation>,
}

pub type FlowerContaminators = HashMap<(TaggedIndex, u16), Vec<(u32, Tick)>>;
\end{code}

The subworld state itself consists of the current time step \inlinecode{tick}, the state associated with the various entities resp. their driving submodels, a PCG pseudo-random number generator (PRNG) \inlinecode{rng} \autocite{pcg_family_algorithms_random_number_generation} and the measurements of several micro-observables.

\phantomsection
\label{micro-observables}

In this context, \emph{micro-observables} are realised by instrumenting the submodels themselves to record measurements about their operations, e.g. every time the pathogen processes in the insect submodel simulate an infection, the species of the infected insect and flower patch at which the infection happened as well as the species of the insect which contaminated the flower patch are stored to implement the \inlinecode{vectors} micro-observable.

In contrast to this, \emph{macro-observables} are realised by periodically dumping the complete world state to persistent storage and then using additional programs to restore the world state and compute measurements based on it, e.g. the prevalence macro-observable is implemented by loading the world state and counting all entities whose state variables indicate infection.

\begin{code}
pub struct World {
    pub tick: Tick,
    pub flower_patches: Box<[FlowerPatch]>,
    pub flowers: Box<[u64]>,
    pub hoverflies: Vec<Hoverfly>,
    pub clutches: Box<[Clutch]>,
    pub solitary_bees: Vec<SolitaryBee>,
    pub nests: Box<[Nest]>,
    pub bumblebees: Vec<Bumblebee>,
    pub colonies: Box<[Colony]>,
    pub observers: Vec<Observer>,
    pub rng: Rng,
    pub next_id: NextId,
    pub observations: Vec<Observation>,
    pub contacts: Contacts,
    pub vectors: Vectors,
    pub temporal_network: TemporalNetwork,
}

pub type Contacts = Vec<((FlowerPatchSpecies, InsectSpecies), (usize, Vec<usize>))>;

pub type Vectors = Vec<((FlowerPatchSpecies, InsectSpecies, InsectSpecies), usize)>;

#[derive(Default)]
pub struct TemporalNetwork {
    pub edges: Vec<(Tick, u32, u32)>,
    pub node_additions: Vec<(Tick, u32, InsectSpecies)>,
    pub node_removals: Vec<(Tick, u32)>,
}

impl TemporalNetwork {
    pub fn clear(&mut self) {
        self.edges.clear();
        self.node_additions.clear();
        self.node_removals.clear();
    }
}
\end{code}

The simulation is parallelized by compartmentalizing different parts of the world that only need to communicate when an insect crosses from one part to another. The number of subworlds is given by \inlinecode{size}, each is enumerated by \inlinecode{rank}. To generate mutually exclusive identifiers, each subworld first increments their identifier by their rank, and afterwards the next identifier is always incremented by size, thus for each subworld we have $\mathtt{id} \mod \mathtt{size} = \mathtt{rank}$.

\begin{code}
#[derive(Clone, Copy)]
pub struct NextId {
    id: u32,
    step: u32,
}

impl NextId {
    pub fn new(id: u32, rank: u8, size: u8) -> Self {
        Self {
            id: id + u32::from(rank),
            step: u32::from(size),
        }
    }

    pub fn get(&mut self) -> u32 {
        let next_id = self.id;
        self.id += self.step;
        next_id
    }
}
\end{code}

\section{Process overview and scheduling}
\label{process-overview}

\begin{figure}

\centering

\begin{tikzpicture}[->,node distance=10cm]

\node[draw,ellipse] (flower-patch) {Flower patch};
\node[draw,ellipse] (empty-flower-patch) [right of=hoverflies] {Empty flower patch};

\path
    (flower-patch) edge node[above] {Collect nectar and pollen} (empty-flower-patch)
    (empty-flower-patch) edge[bend left] node[below] {Produce nectar} (flower-patch)
    (empty-flower-patch) edge[bend right] node[above] {Search flower patch} (flower-patch);

\end{tikzpicture}

\caption{Nectar is produced by the flower patches and collected by the insects.}

\end{figure}

The model dynamics are driven primarily by the scarcity of nectar produced by the flower patches and collected by the insects: Flower patches are stationary and either have nectar available or are empty whereas insects are mobile and search for flower patches that have nectar available and collect nectar and pollen until the flower patches become empty. This in turn drives the insects to search for other flower patches which still provide nectar whereas the empty flower patches will refill after one day.

Thus by varying the rates of consumption and production as well as the abundances and preferences of the insect and plant species, different scenarios concerning the contact rates and indirect interactions can be investigated, e.g. a highly abundant insect species that favours a low productivity plant species will quickly empty their preferred flower patches and therefore experience high contact rates. Similarly, by placing flower patches non-uniformly based on e.g. land use surveys available as GIS data, the effects of various spatial structures like hedgerows on contact rates and thereby pathogen transmission can be analysed in simulated experiments.

The simulation is advanced in discrete steps in which the subworld state is updated by the various submodels which also places relocations for mobile entities which should be transferred to another process in the outbox of this process. After exchanging those relocations with the neighbouring processes, the mobile entities which have been relocated to this process are taken from the inbox and integrated into the subworld state so that it is consistent when written to disk and before commencing the next iteration.

\begin{code}
impl Model {
    pub fn run(&mut self, process: &mut dyn Process<Relocation>) {
        self.world.update(
            &self.params,
            &self.grid,
            &self.flower_patch_locations,
            &mut self.visible_flower_patch_locations,
            &mut self.visited_flowers,
            &mut self.flower_contaminators,
            &mut self.flower_patch_events,
            &self.clutch_locations,
            &mut self.visible_clutch_locations,
            &mut self.clutch_events,
            &self.nest_locations,
            &mut self.visible_nest_locations,
            &mut self.relocations,
        );

        process.exchange(&mut self.relocations);

        for relocation in self.relocations.drain() {
            match relocation {
                Relocation::Hoverfly(hoverfly) => {
                    self.world.hoverflies.push(hoverfly);
                    if self.visited_flowers[0].len() < self.world.hoverflies.len() {
                        self.visited_flowers[0].push(Vec::new());
                    }
                }
                Relocation::SolitaryBee(solitary_bee) => {
                    self.world.solitary_bees.push(solitary_bee);
                    if self.visited_flowers[1].len() < self.world.solitary_bees.len() {
                        self.visited_flowers[1].push(Vec::new());
                    }
                }
                Relocation::Bumblebee(bumblebee) => {
                    self.world.bumblebees.push(bumblebee);
                    if self.visited_flowers[2].len() < self.world.bumblebees.len() {
                        self.visited_flowers[2].push(Vec::new());
                    }
                }
                Relocation::Observer(observer) => {
                    self.world.observers.push(observer);
                }
            }
        }
    }
}
\end{code}

For each time step, the submodels are invoked in order, first on the stationary entities attached to this subworld and then for the mobile entities currently located in this subworld. Some submodels use a DES-based formulation and process events instead of entities. Other submodels like the insect submodel are invoked only indirectly via the directly invoked submodels for hoverflies, solitary bees and bumblebees. All world state is passed to the submodels explicitly, i.e. a submodel can only access the state variables of the relevant entity and those state variables which are explicitly made available to it by passing them to the corresponding \inlinecode{update} function.

\begin{figure}

\begin{center}
\begin{tikzpicture}

\tikzstyle{every node}=[font=\footnotesize]

\draw[->] (1,1) -- (8,1) node[midway,above] {call depth};
\draw[->] (-1,-1) -- (-1,-18) node[midway,below,sloped] {invocation order};

\draw (0,0) rectangle node {flower patch} +(3,-2);
    \draw (3,-0.1) rectangle node {refill nectar} +(3,-0.9);
    \draw (3,-1) rectangle node {decontaminate} +(3,-0.9);

\draw (0,-2) rectangle node {hoverfly clutch} +(3,-1.5);
    \draw (3,-2.1) rectangle node {hatch eggs} +(3,-1.3);

\draw (0,-3.5) rectangle node {bumblebee colony} +(3,-1.5);
    \draw (3,-3.6) rectangle node {decontaminate} +(3,-1.3);

\draw (0,-5) rectangle node {hoverfly} +(3,-4);
    \draw (3,-5.1) rectangle node {reproduce} +(3,-1.4);
    \draw (3,-6.5) rectangle node {insect} +(3,-2.4);
        \draw (6,-6.6) rectangle node {forage} +(3,-1.2);
        \draw (6,-7.8) rectangle node[align=center] {infect \&\\contaminate} +(3,-1);

\draw (0,-9) rectangle node {solitary bee} +(3,-4);
    \draw (3,-9.1) rectangle node {reproduce} +(3,-1.4);
    \draw (3,-10.5) rectangle node {insect} +(3,-2.4);
        \draw (6,-10.6) rectangle node {forage} +(3,-1.2);
        \draw (6,-11.8) rectangle node[align=center] {infect \&\\contaminate} +(3,-1);

\draw (0,-13) rectangle node {bumblebee} +(3,-4);
    \draw (3,-13.1) rectangle node {reproduce} +(3,-1.4);
    \draw (3,-14.5) rectangle node {insect} +(3,-2.4);
        \draw (6,-14.6) rectangle node {forage} +(3,-1.2);
        \draw (6,-15.8) rectangle node[align=center] {infect \&\\contaminate} +(3,-1);

\draw (0,-17) rectangle node {observer} +(3,-2);
    \draw (3,-17.1) rectangle node {catch insects} +(3,-1.8);

\end{tikzpicture}
\end{center}

\caption{Schedule of submodel invocations within a single time step. The vertical direction represents the order in which the per-entity submodels are invoked. The horizontal direction shows the caller-callee relation. This illustrates how the generic insect submodel is invoked at three different call sites by each of the three insect family submodels.}

\end{figure}

The invocation order of the submodels on the involved entities is arbitrary but reproducible, i.e. it depends only on the initial random seed which determines how mobile entities are relocated between the subworlds.\footnote{Strictly speaking, it depends on the initial random seed and the number of subworlds.} This makes it possible to have unrestricted dependencies between the invocations of different submodels and for different entities, e.g. one pseudo-random number generator per subworld instead of per entity can be used and changes to flower patches induced by insects are applied in invocation order.

\begin{code}
impl World {
    #[allow(clippy::too_many_arguments)]
    pub fn update(
        &mut self,
        params: &Params,
        grid: &Grid,
        flower_patch_locations: &KDTree<FlowerPatchLocation>,
        visible_flower_patch_locations: &mut Vec<&'static FlowerPatchLocation>,
        visited_flowers: &mut [Vec<Vec<u16>>; 3],
        flower_contaminators: &mut FlowerContaminators,
        flower_patch_events: &mut BinaryHeap<FlowerPatchEvent>,
        clutch_locations: &KDTree<ClutchLocation>,
        visible_clutch_locations: &mut Vec<&'static ClutchLocation>,
        clutch_events: &mut BinaryHeap<ClutchEvent>,
        nest_locations: &KDTree<NestLocation>,
        visible_nest_locations: &mut Vec<&'static NestLocation>,
        relocations: &mut MailBoxes<Relocation>,
    ) {
        FlowerPatchEvent::process(
            flower_patch_events,
            self.tick,
            &mut self.flower_patches,
            &mut self.flowers,
        );

        ClutchEvent::process(clutch_events, self.tick, &mut self.clutches);

        for colony in &mut *self.colonies {
            colony.update(self.tick);
        }
\end{code}

The invocation order between the three insect families is fixed, i.e. first the hoverflies, then the solitary bees and finally the bumblebees. Experiments with both randomising the invocation between the families as well as randomising the processing order of the individual insects within a family showed no measurable effects and therefore the simplest possible implementation was chosen. This relies on the time step being significantly smaller than the time scales of any competitive interactions\footnote{For example, foraging visits to a flower patch last on the order of minutes to hours using the default nectar production and consumption rates.} and on the randomised initialisation of the individual insects.

\begin{code}
        let mut idx = 0;
        while idx < self.hoverflies.len() {
            let hoverfly = &mut self.hoverflies[idx];
            let position = hoverfly.insect.position;

            hoverfly.update(
                params,
                flower_patch_locations,
                visible_flower_patch_locations,
                &mut visited_flowers[0][idx],
                flower_contaminators,
                flower_patch_events,
                clutch_locations,
                visible_clutch_locations,
                clutch_events,
                self.tick,
                &mut self.flower_patches,
                &mut self.flowers,
                &mut self.clutches,
                &mut self.rng,
                &mut self.next_id,
                &mut self.contacts,
                &mut self.vectors,
                &mut self.temporal_network,
            );
\end{code}

For all mobile entities, their current position is checked after each time step to determine if they should still be located in the current subworld or whether they need to be relocated to another subworld.

To efficiently implement relocations, the order in which the insects are processed is not preserved. Specifically, if no relocation is required, the index of the currently updated insect \inlinecode{idx} is incremented at the end of the loop and the next insect is processed during the next iteration. But if a relocation is required, the insect that is currently updated is removed and replaced by the last entry in the list of insects of that family. Hence, the index is not incremented to update the insect that was just swapped in during the next iteration. Finally, to preserve the association with the visited flowers lists, the same swap of position is applied there.

This can be illustrated in a tabular form where capital letters A, B, C, etc. denote the state of a particular insect, small letters a, b, c, etc. denote the visited flowers associated with the insects A, B, C, etc. Assuming that B is currently processed and that a relocated is required, the situation could be \\
\begin{tabular}{|l|ccccc:cc|}
\hline
insects & A & B & C & D & E & & \\
visited flowers & a & b & c & d & e & k & p \\
\hline
\end{tabular} \\
and is then changed to \\
\begin{tabular}{|l|ccccccc|}
\hline
insects & A & E & C & D & & & \\
visited flowers & a & b & c & d & e & k & p \\
\hline
\end{tabular} \\
by removing B and putting E in its place and finally becomes \\
\begin{tabular}{|l|cccc:ccc|}
\hline
insects & A & E & C & D & & & \\
visited flowers & a & e & c & d & b & k & p \\
\hline
\end{tabular} \\
by swapping b and e.

\begin{code}
            if position != hoverfly.insect.position {
                let dest = grid.location(&hoverfly.insect.position);
                if dest != grid.rank {
                    let hoverfly = self.hoverflies.swap_remove(idx);
                    visited_flowers[0].swap(idx, self.hoverflies.len());

                    relocations.push(dest, Relocation::Hoverfly(hoverfly));
                    continue;
                }
            }

            idx += 1;
        }
\end{code}

Note that the entries of the \inlinecode{visited\_flowers} arrays are never discarded to avoid the overhead of repeatedly freeing and allocating memory. This is possible as while these lists of flowers need to be kept available as long as an insect forages at a flower patch, they can be discarded as soon as the flower patch is left and are regenerated when another flower patch is entered. Since the need for a relocation requires a change in position, it also implies that the insect in question is not currently foraging at a flower patch. After a relocation, an insect can therefore be associated with a previously discarded list of flowers to reuse the underlying allocation instead of including the list of flowers in the transferred entity state.

\begin{code}
        let mut idx = 0;
        while idx < self.solitary_bees.len() {
            let solitary_bee = &mut self.solitary_bees[idx];
            let position = solitary_bee.insect.position;

            solitary_bee.update(
                params,
                flower_patch_locations,
                visible_flower_patch_locations,
                &mut visited_flowers[1][idx],
                flower_contaminators,
                flower_patch_events,
                nest_locations,
                visible_nest_locations,
                self.tick,
                &mut self.flower_patches,
                &mut self.flowers,
                &mut self.nests,
                &mut self.rng,
                &mut self.next_id,
                &mut self.contacts,
                &mut self.vectors,
                &mut self.temporal_network,
            );

            if position != solitary_bee.insect.position {
                let dest = grid.location(&solitary_bee.insect.position);
                if dest != grid.rank {
                    let solitary_bee = self.solitary_bees.swap_remove(idx);
                    visited_flowers[1].swap(idx, self.solitary_bees.len());

                    relocations.push(dest, Relocation::SolitaryBee(solitary_bee));
                    continue;
                }
            }

            idx += 1;
        }

        let mut idx = 0;
        while idx < self.bumblebees.len() {
            let bumblebee = &mut self.bumblebees[idx];
            let position = bumblebee.insect.position;

            bumblebee.update(
                params,
                flower_patch_locations,
                visible_flower_patch_locations,
                &mut visited_flowers[2][idx],
                flower_contaminators,
                flower_patch_events,
                self.tick,
                &mut self.flower_patches,
                &mut self.flowers,
                &mut self.colonies,
                &mut self.rng,
                &mut self.next_id,
                &mut self.contacts,
                &mut self.vectors,
                &mut self.temporal_network,
            );

            if position != bumblebee.insect.position {
                let dest = grid.location(&bumblebee.insect.position);
                if dest != grid.rank {
                    let bumblebee = self.bumblebees.swap_remove(idx);
                    visited_flowers[2].swap(idx, self.bumblebees.len());

                    relocations.push(dest, Relocation::Bumblebee(bumblebee));
                    continue;
                }
            }

            idx += 1;
        }

        let mut idx = 0;
        while idx < self.observers.len() {
            let observer = &mut self.observers[idx];
            let position = observer.position;

            observer.update(
                params,
                visited_flowers,
                self.tick,
                &self.flower_patches,
                &self.flowers,
                &mut self.hoverflies,
                &mut self.solitary_bees,
                &mut self.bumblebees,
                &mut self.observations,
            );

            if position != observer.position {
                let dest = grid.location(&observer.position);
                if dest != grid.rank {
                    let observer = self.observers.swap_remove(idx);

                    relocations.push(dest, Relocation::Observer(observer));
                    continue;
                }
            }

            idx += 1;
        }
\end{code}

The duration of each time step is a free parameter, i.e. the model can be run at various time resolutions essentially without affecting the dynamics of the simulated processes as all rates are rescaled and all events are tracked w.r.t. simulated time instead of simulation steps. This enables the same code to be used for simulated experiments with a large time step of $1 ~\mathrm{s}$ and to drive a real-time visualisation running at 25 frames per second and hence with a small time step of $40 ~\mathrm{ms}$. However, the intrisic time scales of the simulated processes imply an upper limit which is usually $3 ~\mathrm{s}$ due to the time-between-turns parameter of the insect submodel.

\begin{code}
        self.tick += params.tick;
    }
}
\end{code}

Relocations of mobile entities from one subworld to another are facilitated via message passing using either shared memory or the standardised message passing interface \autocite{message_passing_interface}. The current implementation relies on the \inlinecode{Relocation} data structure being contiguous and free of indirections so that it can be transferred by plain memory copies which is asserted by implementing the \inlinecode{Message} trait.

\begin{code}
#[derive(Clone, Copy)]
pub enum Relocation {
    Hoverfly(Hoverfly),
    SolitaryBee(SolitaryBee),
    Bumblebee(Bumblebee),
    Observer(Observer),
}

unsafe impl Message for Relocation {}
\end{code}

For a mobile entity to properly identify a stationary entity, e.g. for memorisation of depleted flower patches, both the process rank as well as the index of the entity need to be stored. To avoid the loss of memory density due to alignment and padding, these two values are packed into a single four byte integer using a single byte for the rank and three bytes for the index, i.e. at most $2^8 = 256$ processes handling fewer than $2^{24} = 16\,777\,216$ entities of a given type are assumed.

\begin{code}
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct TaggedIndex(u32);

impl TaggedIndex {
    const TAG: u32 = 0xFF_00_00_00;

    pub fn new(rank: u8, index: u32) -> Self {
        assert!(index & Self::TAG == 0);

        Self((rank as u32) << 24 | index)
    }

    pub fn get(&self) -> usize {
        (self.0 & !Self::TAG) as usize
    }
}
\end{code}

Several submodels need growable buffers of short-lived references. To reduce the overhead of dynamically allocating those buffers, they are declared as storing references with a \inlinecode{'static} lifetime, stored in the model state and reused. To do so safely, a function that temporarily reduces these lifetimes is defined. For this to be safe, the buffer is cleared after the code that creates these short-lived references has executed.

\begin{code}
fn reduce_lifetime<'a, T, F>(buf: &'a mut Vec<&'static T>, f: F)
where
    F: FnOnce(&mut Vec<&'a T>),
{
    struct ClearOnDrop<'a, 'b, T>(&'a mut Vec<&'b T>);

    impl<'a, 'b, T> Drop for ClearOnDrop<'a, 'b, T> {
        fn drop(&mut self) {
            self.0.clear();
        }
    }

    let buf = ClearOnDrop(unsafe { transmute(buf) });

    f(buf.0);
}
\end{code}