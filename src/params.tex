\section{Parameters}
\label{params}

\begin{hiddencode}
use std::f64::consts::{FRAC_PI_4, TAU};
use std::path::PathBuf;
use std::sync::Arc;

use rand::distributions::{Distribution, Uniform};
use rand_distr::{Bernoulli, Exp, Normal, Pareto, WeightedIndex};

use crate::{
    observer::Strategy as ObserverStrategy,
    pathogen::{Infection, InitialOverallPrevalence},
    units::{Area, Length, Mass, MassRate, Rate, Tick, Velocity, Volume, VolumeRate},
    Rng,
};
\end{hiddencode}

The model parameters are grouped by the different kinds of entities present in the model, similarly to the submodels. 

The time resolution is given by the discrete time step \inlinecode{tick} whereas the spatial extent is defined by the side length \inlinecode{length} of the square landscape.

\begin{code}
#[derive(Clone)]
pub struct Params {
    pub tick: Tick,
    pub length: Length,
    pub flower_patches: Box<[FlowerPatch]>,
    pub hoverflies: Box<[Hoverfly]>,
    pub solitary_bees: Box<[SolitaryBee]>,
    pub bumblebees: Box<[Bumblebee]>,
    pub transect: Box<Transect>,
    pub init: Box<Init>,
    pub trace_temporal_network: bool,
}
\end{code}

The parameters of the flower patch submodel essentially describe the different plant species present in the simulated landscape. This includes their flower density \inlinecode{flower_density} to determine the number of individual flowers within a patch, the nectar production per day and flower \inlinecode{nectar_production} and the exponentially distributed decontamination time of a single flower \inlinecode{decontamination}.

\begin{code}
#[derive(Clone)]
pub struct FlowerPatch {
    pub flower_density: Normal<f64>,
    pub nectar_production: Normal<f64>,
    pub decontamination: Exp<f64>,
}

impl FlowerPatch {
    pub fn flowers(&self, area: Area, rng: &mut Rng) -> u16 {
        let flowers = self.flower_density.sample(rng).max(0.) * area.as_square_meters();

        assert!(flowers <= u16::MAX as f64);

        flowers as u16
    }

    pub fn nectar_production(&self, rng: &mut Rng) -> Mass {
        Mass::micrograms(self.nectar_production.sample(rng).max(0.))
    }

    pub fn decontamination(&self, params: &Params, rng: &mut Rng) -> Tick {
        params.tick * self.decontamination.sample(rng) as i64
    }
}
\end{code}

\phantomsection
\label{insect-params}

The parameters of the insect submodel and its three specialisations describe the different insect species and families.

Mainly this is related to their foraging efforts. The exploration of the landscape is described the distance at which flower patch can be sensed \inlinecode{vision} and the capacity of the memory for depleted and ignored flower patches \inlinecode{memory} with a default value \inlinecode{FLOWER_PATCH_MEMORY} choosen large enough to avoid insects getting stuck within some part of the landscape. Their specific preferences for the various plant species are defined by an acceptance probabilities for each plant species given in \inlinecode{preferences}.

The random walk used to represent insect flight is controlled by the pre-multiplied \inlinecode{speed}, the number of time steps between turns given by \inlinecode{between_turns} and the distribution of turning angles \inlinecode{direction} which is currently assumed to be uniform, i.e. the random walk is assumed to be completely uncorrelated.

The foraging activity within a flower patch is controlled by the two rates \inlinecode{nectar_consumption} and \inlinecode{pollen_collection}. The former one determines the rate at which the nectar available at flower patches is depleted and thereby the rates at which individual flowers within a patch are visited. The latter one determines the rate at which the insect accumulate the amount of pollen required to engage in their reproductive activities.

The \inlinecode{life_expectancy} distribution determines how often insect identifiers are reassigned which affects resulting contact traces.

The parameters related to the pathogen submodel are the probability of infection when foraging at a contaminated flower \inlinecode{infection}, the probability of contamination of a flower when an infected insect forages there \inlinecode{contamination} and finally the exponentially distributed recovery time of an infected insect \inlinecode{recovery}.

\begin{code}
#[derive(Clone)]
pub struct Insect {
    pub life_expectancy: Exp<f64>,
    pub vision: Length,
    pub memory: u16,
    pub speed: Length,
    pub direction: Uniform<f64>,
    pub between_turns: Tick,
    pub preferences: Box<[Bernoulli]>,
    pub nectar_consumption: Mass,
    pub pollen_collection: Volume,
    pub infection: Bernoulli,
    pub contamination: Bernoulli,
    pub recovery: Exp<f64>,
}

impl Insect {
    pub fn life_expectancy(&self, params: &Params, rng: &mut Rng) -> Tick {
        params.tick * self.life_expectancy.sample(rng) as i64
    }

    pub fn recovery(&self, params: &Params, rng: &mut Rng) -> Tick {
        params.tick * self.recovery.sample(rng) as i64
    }
}

pub const FLOWER_PATCH_MEMORY: usize = 128;
\end{code}

The parameters used to represent a hoverfly species are related to their reproductive activities. The distance at which they are assumed to be able to sense viable clutch sites is given by \inlinecode{vision}. The currently fixed memory capacity for remembering fully occupied clutch sites is given by \inlinecode{CLUTCH_MEMORY}. The remaining parameters describe the resource needs and scheduling of laying and hatching a single clutch of eggs.

\begin{code}
#[derive(Clone)]
pub struct Hoverfly {
    pub insect: Insect,
    pub vision: Length,
    pub pollen_per_clutch: Volume,
    pub clutch_laying_duration: Tick,
    pub clutch_hatching_duration: Tick,
}

pub const CLUTCH_MEMORY: usize = 4;
\end{code}

The parameters used to represent a solitary bee species are related to their reproductive activities. The distance at which they are assumed to be able to sense viable nest sites is given by \inlinecode{vision}. The currently fixed memory capacity for remembering occupied nest sites is given by \inlinecode{NEST_MEMORY}. The remaining parameters describe the resource needs and scheduling of building a single brood cell as well as the capacity of a single nest.

\begin{code}
#[derive(Clone)]
pub struct SolitaryBee {
    pub insect: Insect,
    pub vision: Length,
    pub pollen_per_brood_cell: Volume,
    pub brood_cells_per_nest: u8,
    pub cell_building_duration: Tick,
}

pub const NEST_MEMORY: usize = 8;
\end{code}

The parameters used to represent a bumblebee species define their foraging bouts w.r.t. pollen load and duration of the work they perform within the colony afterwards. Further parameters define the adaption of the general SIS model for within-colony transmission where a colony is considered as a single contact site epidemiologically.

\begin{code}
#[derive(Clone)]
pub struct Bumblebee {
    pub insect: Insect,
    pub pollen_per_bout: Volume,
    pub work_duration: Tick,
    pub colony_infection: Bernoulli,
    pub colony_contamination: Bernoulli,
    pub colony_decontamination: Exp<f64>,
}

impl Bumblebee {
    pub fn colony_decontamination(&self, params: &Params, rng: &mut Rng) -> Tick {
        params.tick * self.colony_decontamination.sample(rng) as i64
    }
}
\end{code}

\phantomsection
\label{default-param-values}

The following code provides default values which define a baseline scenario that can then be modified for the various simulation experiments.

The default time step is one second. All rate parameters will be pre-multiplied by the time step to improve the efficiency of the time-discrete simulation.

\begin{code}
impl Default for Params {
    fn default() -> Self {
        Params::new(Tick::seconds(1))
    }
}

impl Params {
    pub fn new(tick: Tick) -> Self {
\end{code}

The default pathogen parameters are based on the analytical SIS model published in \autocite{landscape_simplification_shapes_pathogen_prevalence}.

\begin{code}
        let infection = Bernoulli::new(0.02).unwrap();
        let contamination = Bernoulli::new(0.02).unwrap();

        let decontamination = Exp::new(Rate::per_hour(1. / 3.) * tick).unwrap();
        let recovery = Exp::new(Rate::per_day(1. / 3.) * tick).unwrap();
\end{code}

The default set of plant species was chosen as the five most common plant species from a 2013 study of solitary bees on nine Higher Level Stewardship farms in England. \autocite{bee_and_flower_abundance} Their flower density and nectar production per flower and day were sourced from the AgriLand data sets. \autocite{agriland_flower_density} \autocite{agriland_nectar}

\begin{code}
        let lotus_corniculatus = FlowerPatch {
            flower_density: Normal::new(331.59, f64::sqrt(84639.06)).unwrap(),
            nectar_production: Normal::new(61.82, f64::sqrt(3613.92)).unwrap(),
            decontamination,
        };

        let centaurea_nigra = FlowerPatch {
            flower_density: Normal::new(4390.49, f64::sqrt(9036797.71)).unwrap(),
            nectar_production: Normal::new(198.99, f64::sqrt(23157.83)).unwrap(),
            decontamination,
        };

        let trifolium_pratense = FlowerPatch {
            flower_density: Normal::new(4082.21, f64::sqrt(1169007.29)).unwrap(),
            nectar_production: Normal::new(116.86, f64::sqrt(5041.12)).unwrap(),
            decontamination,
        };

        let trifolium_repens = FlowerPatch {
            flower_density: Normal::new(4902.41, f64::sqrt(7005426.36)).unwrap(),
            nectar_production: Normal::new(48.97, f64::sqrt(1317.19)).unwrap(),
            decontamination,
        };

        let melilotus_officinalis = FlowerPatch {
            flower_density: Normal::new(8575.52, f64::sqrt(9232579.09)).unwrap(),
            nectar_production: Normal::new(20.17, f64::sqrt(172.73)).unwrap(),
            decontamination,
        };

        let flower_patches = vec![
            lotus_corniculatus,
            centaurea_nigra,
            trifolium_pratense,
            trifolium_repens,
            melilotus_officinalis,
        ]
        .into();
\end{code}

The period of adult foraging of solitary bees only lasts for a couple of weeks, for some it is only between 10 to 14 days (see page 37 from \autocite{the_solitary_bees}).

The consumption and collection rates used numbers given for bumblebees in \autocite{commercially_imported_bumble_bees} for nectar consumption and \autocite{imidacloprid_reduce_bumble_bee_pollen_foraging} for pollen collection. Due to lack of more detailed data, these values were then divided by two for solitary bees and by two again for hoverflies based on the relations of their body masses.

The amount of pollen required for a clutch of hoverfly eggs is based on the frequency of up to five eggs per day described in \autocite{biologie_der_schwebfliegen_deutschlands}. The amount of pollen a solitary bee invests into a single brood cell is sourced from \autocite{quantitative_pollen_requirements_of_solitary_bees} while the duration of building a single brood cell is taken from \autocite{components_of_nest_provisioning_behavior}.

The parameters of the random walk used to model insect flight are based on \autocite{bumble_beehave}, i.e. a speed above ground of 3 meters per second and a completely random turn every 3 seconds.

\begin{code}
        let hoverfly = Hoverfly {
            insect: Insect {
                life_expectancy: Exp::new(Rate::per_day(1. / 30.) * tick).unwrap(),
                vision: Length::meters(2.),
                memory: 128,
                speed: Velocity::meters_per_second(3.) * tick,
                direction: Uniform::new(0., TAU),
                between_turns: Tick::seconds(3),
                preferences: (0..5).map(|_| Bernoulli::new(1.).unwrap()).collect(),
                nectar_consumption: MassRate::milligrams_per_hour(43.875) * tick,
                pollen_collection: VolumeRate::cubic_millimeters_per_hour(4.5) * tick,
                infection,
                contamination,
                recovery,
            },
            vision: Length::meters(0.5),
            pollen_per_clutch: Volume::cubic_millimeters(25.),
            clutch_laying_duration: Tick::minutes(15),
            clutch_hatching_duration: Tick::days(10),
        };

        let solitary_bee = SolitaryBee {
            insect: Insect {
                life_expectancy: Exp::new(Rate::per_day(1. / 30.) * tick).unwrap(),
                vision: Length::meters(2.),
                memory: 128,
                speed: Velocity::meters_per_second(3.) * tick,
                direction: Uniform::new(0., TAU),
                between_turns: Tick::seconds(3),
                preferences: (0..5).map(|_| Bernoulli::new(1.).unwrap()).collect(),
                nectar_consumption: MassRate::milligrams_per_hour(87.75) * tick,
                pollen_collection: VolumeRate::cubic_millimeters_per_hour(9.) * tick,
                infection,
                contamination,
                recovery,
            },
            vision: Length::meters(0.5),
            pollen_per_brood_cell: Volume::cubic_millimeters(50.),
            brood_cells_per_nest: 20,
            cell_building_duration: Tick::minutes(30),
        };

        let bumblebee = Bumblebee {
            insect: Insect {
                life_expectancy: Exp::new(Rate::per_day(1. / 30.) * tick).unwrap(),
                vision: Length::meters(2.),
                memory: 128,
                speed: Velocity::meters_per_second(3.) * tick,
                direction: Uniform::new(0., TAU),
                between_turns: Tick::seconds(3),
                preferences: (0..5).map(|_| Bernoulli::new(1.).unwrap()).collect(),
                nectar_consumption: MassRate::milligrams_per_hour(175.5) * tick,
                pollen_collection: VolumeRate::cubic_millimeters_per_hour(18.) * tick,
                infection,
                contamination,
                recovery,
            },
            pollen_per_bout: Volume::cubic_millimeters(100.),
            work_duration: Tick::minutes(60),
            colony_infection: infection,
            colony_contamination: contamination,
            colony_decontamination: decontamination,
        };

        Self {
            tick,
            length: Length::kilometers(1.),
            flower_patches,
            hoverflies: vec![hoverfly].into(),
            solitary_bees: vec![solitary_bee].into(),
            bumblebees: vec![bumblebee].into(),
            transect: Default::default(),
            init: Default::default(),
            trace_temporal_network: false,
        }
    }
}
\end{code}

\phantomsection
\label{transect-params}

The transect parameters describe the geometry and scheduling of the simulated field sampling as given by the experimental protocol for the VOODOO project \autocite{experimental_protocol_2020}.

\begin{code}
#[derive(Clone)]
pub struct Transect {
    pub length: Length,
    pub direction: f64,
    pub vision: Length,
    pub spacing: Length,
    pub wait_until: Tick,
    pub duration: Tick,
    pub handling_duration: Tick,
    pub strategy: ObserverStrategy,
}

impl Default for Transect {
    fn default() -> Self {
        Self {
            length: Length::kilometers(1.),
            vision: Length::meters(1.),
            spacing: Length::meters(25.),
            direction: FRAC_PI_4,
            wait_until: Tick::days(15),
            duration: Tick::minutes(120),
            handling_duration: Tick::minutes(1),
            strategy: ObserverStrategy::Walk,
        }
    }
}
\end{code}

\phantomsection
\label{init-params}

As the model does not include population dynamics of the insects or the plants, the most important role of the initialisation parameters is to determine the species composition of the insect and plant community.

The number of flower patches given by \inlinecode{flower_patches} determines the resolution at which the distribution of floral resources is considered, e.g. a uniform placement of $100\,000$ flower patches in a simulated landscape of $1 ~\mathrm{km}^2$ is roughly equivalent to using a regular grid with a cell size of $3 ~\mathrm{m}$.

The abundance of floral resources is controlled by the \inlinecode{flower_patch_cover} parameters which determines the fraction of the landscape area which is assumed to be covered by flowering plants and is then used to compute the number of individual flowers based on empirical values of specific flower densities.

The parameter \inlinecode{empty_flower_patches} is used to determine the initial state of the flower patches and does not affect the model dynamics beyond reducing the time until they reach a steady state.

The placement of floral resources is uniformly random by default but can be determined based on GIS data by optionally giving the path to a pre-processed raster via \inlinecode{landscape}.

The number of insects given by \inlinecode{hoverflies}, \inlinecode{solitary_bees} directly determines their density in the simulated landscape. Similarly, the density of reproductive resources like clutching and nesting sites is controlled by specifying their count.

For bumblebees, the effect is slightly more indirect as the number of colonies and the distribution of the size as measured by number of workers is specified via \inlinecode{colonies} and \inlinecode{colony_capacity}.

The composition of species within each family is given by the weights \inlinecode{hoverfly_species}, \inlinecode{solitary_bee_species} and \inlinecode{bumblebee_species}.

The initial distribution of the pathogen is controlled by by strategy for initialising the epidemiological status of each insect as described in \autoref{pathogen-initialisation} and given by \inlinecode{infection}.

The random seed given by \inlinecode{seed} is used to initialise the state of all PRNG instances used by the model and can therefore be used to sample the stochastic variability built into the various submodels.

\begin{code}
#[derive(Clone)]
pub struct Init {
    pub seed: u64,
    pub landscape: Option<PathBuf>,
    pub flower_patches: u32,
    pub flower_patch_species: WeightedIndex<usize>,
    pub flower_patch_cover: f64,
    pub empty_flower_patches: Bernoulli,
    pub hoverflies: u32,
    pub hoverfly_species: WeightedIndex<usize>,
    pub clutches: u32,
    pub clutch_capacity: Pareto<f32>,
    pub solitary_bees: u32,
    pub solitary_bee_species: WeightedIndex<usize>,
    pub nests: u32,
    pub bumblebee_species: WeightedIndex<usize>,
    pub colonies: u32,
    pub colony_capacity: Normal<f32>,
    pub infection: Arc<dyn Infection + Send + Sync>,
}

impl Default for Init {
    fn default() -> Self {
        Self {
            seed: 0,
            landscape: None,
            flower_patches: 100_000,
            flower_patch_species: WeightedIndex::new(&[1350199, 906112, 884065, 458559, 333960])
                .unwrap(),
            flower_patch_cover: 0.05,
            empty_flower_patches: Bernoulli::new(0.9).unwrap(),
            hoverflies: 3_000,
            hoverfly_species: WeightedIndex::new(&[1]).unwrap(),
            clutches: 3_000,
            clutch_capacity: Pareto::new(50., 5.).unwrap(),
            solitary_bees: 3_000,
            solitary_bee_species: WeightedIndex::new(&[1]).unwrap(),
            nests: 12_000,
            bumblebee_species: WeightedIndex::new(&[1]).unwrap(),
            colonies: 15,
            colony_capacity: Normal::new(200., 10.).unwrap(),
            infection: Arc::new(InitialOverallPrevalence::new(0.01)),
        }
    }
}

impl Init {
    pub fn clutch_capacity(&self, rng: &mut Rng) -> u16 {
        self.clutch_capacity.sample(rng).ceil() as u16
    }

    pub fn colony_capacity(&self, rng: &mut Rng) -> u16 {
        self.colony_capacity.sample(rng).ceil() as u16
    }
}
\end{code}