\subsection{Coordinate system}

\begin{hiddencode}
use std::ops::{Add, Div, Mul, Sub};

use kdtree::{Point, Query};

use crate::{
    params::Params,
    units::{Area, Length, Scale},
};
\end{hiddencode}

Spatial relations are represented using a two-dimensional vector space endowed with the Euclidean scalar product and norm. To improve compile-time checking of formulas, vector components and scalars are quantities with dimension and unit defaulting to length and meters. The implemented dot product, vector addition and scalar multiplication uphold dimensional relations, e.g. the scalar product of two length vectors is an area scalar.

\begin{code}
#[derive(Clone, Copy, Default, PartialEq, Eq)]
pub struct Vector<T = Length> {
    pub x: T,
    pub y: T,
}

impl<T> Vector<T> {
    pub fn dot<S>(self, other: Vector<S>) -> <T::Output as Add>::Output
    where
        T: Mul<S>,
        T::Output: Add,
    {
        self.x * other.x + self.y * other.y
    }

    pub fn norm_2(self) -> <T::Output as Add>::Output
    where
        T: Copy + Mul,
        T::Output: Add,
    {
        self.dot(self)
    }
}

impl<T> Add for Vector<T>
where
    T: Add<Output = T>,
{
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T> Sub for Vector<T>
where
    T: Sub<Output = T>,
{
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<S, T> Mul<S> for Vector<T>
where
    T: Mul<S>,
    S: Copy,
{
    type Output = Vector<<T as Mul<S>>::Output>;

    fn mul(self, other: S) -> Self::Output {
        Vector {
            x: self.x * other,
            y: self.y * other,
        }
    }
}

impl<S, T> Div<S> for Vector<T>
where
    T: Div<S>,
    S: Copy,
{
    type Output = Vector<<T as Div<S>>::Output>;

    fn div(self, other: S) -> Self::Output {
        Vector {
            x: self.x / other,
            y: self.y / other,
        }
    }
}
\end{code}

The coordinate system is Cartesian and periodic in both dimensions to avoid boundary effects. The simulated landscape corresponds to the domain
\begin{displaymath}
\left[ 0, L \right] \times \left[ 0, L \right] \qquad \left\lbrace 0 \right\rbrace \times \left[ 0, L \right] \equiv \left\lbrace L \right\rbrace \times \left[ 0, L \right] \quad \left[ 0, L \right] \times \left\lbrace 0 \right\rbrace \equiv \left[ 0, L \right] \times \left\lbrace L \right\rbrace
\end{displaymath}
where $L$ is the length of the rectangular landscape.

\subsubsection{Mirror locations}
\label{mirror-locations}

\begin{figure}

\centering

\begin{tikzpicture}[scale=3]

\tikzstyle{point} = [circle,fill,inner sep=1pt]
\tikzstyle{mirror-point} = [circle,fill=lightgray,inner sep=1pt]

\draw[fill=lightgray] (0,0) rectangle (1,1);

\draw (-1, 0) rectangle (0, -1);
\draw (0, 0) rectangle (1, -1);
\draw (1, 0) rectangle (2, -1);
\draw (1, 1) rectangle (2, 0);
\draw (1, 2) rectangle (2, 1);
\draw (0, 2) rectangle (1, 1);
\draw (-1, 2) rectangle (0, 1);
\draw (-1, 1) rectangle (0, 0);

\draw[dashed] (-0.25,-0.25) rectangle (1.25,1.25);

\node[point] at (0.85,0.85) {};

\node[mirror-point] at (1.85,1.85) {};
\node[mirror-point] at (1.85,0.85) {};
\node[mirror-point] at (1.85,-0.15) {};
\node[mirror-point] at (0.85,1.85) {};
\node[point] at (0.85,-0.15) {};
\node[mirror-point] at (-0.15,1.85) {};
\node[point] at (-0.15,0.85) {};
\node[point] at (-0.15,-0.15) {};

\end{tikzpicture}

\caption{Mirror locations of a given point}
\label{mirror-locations-of-a-given-point}

\end{figure}

To determine the proximity of two locations under periodic boundary conditions, one needs to consider the direct way and the indirect ways which cross a boundary and reenter the domain on the opposite side. For a given pair of points $A$ and $B$, an equivalent approach is to consider only the direct connection between the position $A$ and the position $B$ together with its so-called \emph{mirror locations} $B', B'', \dots$ in the extended domain $\left[ -L, 2 L \right] \times \left[-L, 2 L \right]$ as illustrated in \autoref{mirror-locations-of-a-given-point}.

The function \inlinecode{mirror} computes these locations by shifting a given position by the length of the landscape in each direction and for each component.

\begin{code}
pub fn mirror(params: &Params, position: Vector) -> impl ExactSizeIterator<Item = Vector> + '_ {
    [
        (1., 1.),
        (1., 0.),
        (1., -1.),
        (0., 1.),
        (0., 0.),
        (0., -1.),
        (-1., 1.),
        (-1., 0.),
        (-1., -1.),
    ]
    .iter()
    .map(move |(dx, dy)| Vector {
        x: position.x + params.length * *dx,
        y: position.y + params.length * *dy,
    })
}
\end{code}

The function \inlinecode{reduce} implements the inverse operation which collapses all mirror locations from the extended domain back to the original domain by shifting along the necessary directions and components.

\begin{code}
pub fn reduce(params: &Params, position: Vector) -> Vector {
    let mut x = position.x;

    if x >= params.length {
        x -= params.length;
    }

    if x < Default::default() {
        x += params.length;
    }

    let mut y = position.y;

    if y >= params.length {
        y -= params.length;
    }

    if y < Default::default() {
        y += params.length;
    }

    Vector { x, y }
}
\end{code}

The shortest path between two positions \inlinecode{from} and \inlinecode{to} is still a straight line, but the mirror locations need to be considered as well to account for the periodic boundary conditions.

\begin{code}
pub fn shortest_path(params: &Params, from: Vector, to: Vector) -> (f64, Length) {
    let (to, distance_2) = mirror(params, to)
        .map(|to| {
            let to = to - from;

            (to, to.norm_2())
        })
        .min_by(|(_, lhs), (_, rhs)| lhs.partial_cmp(rhs).unwrap())
        .unwrap();

    (to.y.atan2(to.x), distance_2.sqrt())
}
\end{code}

\subsubsection{Spatial queries}
\label{spatial-queries}

\inlinecode{WithinDistanceOfPoint} implements the query for all objects within a given distance of a given point, i.e. it queries a circular neighbourhood by precomputing the AABB the square of the distance.

\begin{code}
pub struct WithinDistanceOfPoint {
    aabb: (Vector, Vector),
    point: Vector,
    distance_2: Area,
}

impl WithinDistanceOfPoint {
    pub fn new(point: Vector, distance: Length) -> Self {
        let aabb = (
            Vector {
                x: point.x - distance,
                y: point.y - distance,
            },
            Vector {
                x: point.x + distance,
                y: point.y + distance,
            },
        );

        let distance_2 = distance * distance;

        Self {
            aabb,
            point,
            distance_2,
        }
    }
}

impl Query<Vector> for WithinDistanceOfPoint {
    fn aabb(&self) -> &(Vector, Vector) {
        &self.aabb
    }

    fn test(&self, position: &Vector) -> bool {
        let distance = *position - self.point;

        distance.norm_2() <= self.distance_2
    }
}
\end{code}

\begin{figure}

\centering

\begin{tikzpicture}[scale=3]

\fill[fill=lightgray]
    (-0.35,0.35) arc (135:315:0.5) -- (1.35,0.65) arc (-45:135:0.5);

\draw[dashed] (-0.5,-0.5) rectangle (1.5,1.5);

\filldraw (0,0) circle[radius=1pt];
\node[below left=5pt of {(0,0)}] {$A$};

\filldraw (1,1) circle[radius=1pt];
\node[above right=5pt of {(1,1)}] {$B$};

\draw (0,0) to (1,1);

\filldraw (0.5,0.5) circle[radius=0.5pt];
\node[right=2.5pt of {(0.5,0.5)}] {$C$};

\filldraw (1,0) circle[radius=0.5pt];
\node[right=2.5pt of {(1,0)}] {$P$};

\draw[dashed] (0.5,0.5) to (1,0);

\end{tikzpicture}

\caption{K-D tree query considering all locations within a given distance to the line segment joining two points}

\end{figure}

When mobile entities move through the landscape to search for stationary entities, a K-D tree spatial index is queried each tick for all points $P$ within a given distance to the line segment joining position before the tick $A$ and the position after the tick $B$.

The point $C$ closest to $P$ on the line segment joining $A$ and $B$ is given by
\begin{displaymath}
t := \max\left\lbrace 0,~ \min\left\lbrace 1,~ \frac{\left< P - A, B - A \right>}{\left< B - A, B - A \right>} \right\rbrace \right\rbrace \qquad C = A + t \left( B - A \right)
\end{displaymath}

\inlinecode{WithinDistanceOfLine} implements this query by computing the axis-aligned bounding box \inlinecode{aabb} based on \inlinecode{from} ($A$) and \inlinecode{to} ($B$). It also precomputes the expression
\begin{displaymath}
\frac{1}{\left< B - A, B - A \right>} \left( B - A \right)
\end{displaymath}
stored in \inlinecode{to\_norm\_2} as this is evaluated repeatedly for each candidate point \inlinecode{position} ($P$) to compute \inlinecode{distance} ($C - P$) and thereby determine the distance of the candidate point to the line segment.

\begin{code}
pub struct WithinDistanceOfLine {
    aabb: (Vector, Vector),
    from: Vector,
    to: Vector,
    to_norm_2: Vector<Scale>,
    distance_2: Area,
}

impl WithinDistanceOfLine {
    pub fn new(from: Vector, to: Vector, distance: Length) -> Self {
        let aabb = (
            Vector {
                x: from.x.min(to.x) - distance,
                y: from.y.min(to.y) - distance,
            },
            Vector {
                x: from.x.max(to.x) + distance,
                y: from.y.max(to.y) + distance,
            },
        );

        let to = to - from;

        let to_norm_2 = to / to.norm_2();

        let distance_2 = distance * distance;

        Self {
            aabb,
            from,
            to,
            to_norm_2,
            distance_2,
        }
    }
}

impl Query<Vector> for WithinDistanceOfLine {
    fn aabb(&self) -> &(Vector, Vector) {
        &self.aabb
    }

    fn test(&self, position: &Vector) -> bool {
        let position = *position - self.from;

        let distance = self.to * 0_f64.max(1_f64.min(self.to_norm_2.dot(position))) - position;

        distance.norm_2() <= self.distance_2
    }
}
\end{code}

As the K-D tree implementation is generic w.r.t. the representation of the indexed points, \inlinecode{Vector} needs to be adapted to specify its dimension and to enable access to its coordinates.

\begin{code}
impl Point for Vector {
    const DIM: usize = 2;

    fn coord(&self, axis: usize) -> f64 {
        match axis {
            0 => self.x.as_meters(),
            1 => self.y.as_meters(),
            _ => unreachable!(),
        }
    }
}
\end{code}