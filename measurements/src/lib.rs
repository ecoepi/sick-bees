use std::fs::{read_dir, File};
use std::path::{Path, PathBuf};

use memmap2::{MmapMut, MmapOptions};

use dump::restore;
use model::{
    bumblebee::{Bumblebee, Colony},
    flower_patch::FlowerPatch,
    hoverfly::{Clutch, Hoverfly},
    insect::Insect,
    solitary_bee::{Nest, SolitaryBee},
    units::Tick,
    World,
};

pub fn list<P>(dir: P, prefix: &str) -> Vec<PathBuf>
where
    P: AsRef<Path>,
{
    let dir = read_dir(dir).unwrap();
    let mut files = Vec::new();

    for entry in dir {
        let entry = entry.unwrap();

        if let Some(file_name) = entry.file_name().to_str() {
            if file_name.starts_with(prefix) && file_name.ends_with(".bin") {
                files.push(entry.path());
            }
        }
    }

    files.sort_unstable();

    files
}

pub fn collect<P, F>(dir: P, mut f: F)
where
    P: AsRef<Path>,
    F: FnMut(Tick, &[&World]),
{
    let files = list(dir, "world_");

    let mut snapshot = "";
    let mut bufs = Vec::<MmapMut>::new();

    for file in &files {
        let next_snapshot = file
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .split('_')
            .nth(1)
            .unwrap();

        if snapshot != next_snapshot {
            if !bufs.is_empty() {
                let worlds = bufs
                    .iter_mut()
                    .map(|buf| unsafe { restore::<World>(buf) })
                    .collect::<Vec<_>>();

                f(worlds[0].tick, &worlds);

                bufs.clear();
            }

            snapshot = next_snapshot;
        }

        let buf = unsafe {
            MmapOptions::new()
                .map_copy(&File::open(file).unwrap())
                .unwrap()
        };

        bufs.push(buf);
    }

    if !bufs.is_empty() {
        let worlds = bufs
            .iter_mut()
            .map(|buf| unsafe { restore::<World>(buf) })
            .collect::<Vec<_>>();

        f(worlds[0].tick, &worlds);
    }
}

pub fn flower_patches<'a>(
    worlds: &'a [&'a World],
) -> impl Iterator<Item = (&'a FlowerPatch, &'a [u64])> + 'a {
    worlds.iter().flat_map(|world| {
        world
            .flower_patches
            .iter()
            .map(move |flower_patch| (flower_patch, &*world.flowers))
    })
}

pub fn insects<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Insect> + 'a {
    hoverflies(worlds)
        .map(|hoverfly| &hoverfly.insect)
        .chain(solitary_bees(worlds).map(|solitary_bee| &solitary_bee.insect))
        .chain(bumblebees(worlds).map(|bumblebee| &bumblebee.insect))
}

pub fn hoverflies<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Hoverfly> + 'a {
    worlds.iter().flat_map(|world| world.hoverflies.iter())
}

pub fn clutches<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Clutch> + 'a {
    worlds.iter().flat_map(|world| world.clutches.iter())
}

pub fn solitary_bees<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a SolitaryBee> + 'a {
    worlds.iter().flat_map(|world| world.solitary_bees.iter())
}

pub fn nests<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Nest> + 'a {
    worlds.iter().flat_map(|world| world.nests.iter())
}

pub fn bumblebees<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Bumblebee> + 'a {
    worlds.iter().flat_map(|world| world.bumblebees.iter())
}

pub fn colonies<'a>(worlds: &'a [&'a World]) -> impl Iterator<Item = &'a Colony> + 'a {
    worlds.iter().flat_map(|world| world.colonies.iter())
}
