use std::env::args_os;

use hashbrown::{HashMap, HashSet};

use measurements::{collect, flower_patches, insects};

fn main() {
    let mut insect_species = Vec::new();
    let mut flower_patch_species = Vec::new();

    let mut contacts = Default::default();
    let mut observations = Default::default();

    collect(args_os().nth(1).unwrap(), |_tick, worlds| {
        if insect_species.is_empty() {
            insect_species = insects(worlds)
                .map(|insect| insect.species)
                .collect::<HashSet<_>>()
                .into_iter()
                .collect::<Vec<_>>();

            insect_species.sort_unstable();
        }

        if flower_patch_species.is_empty() {
            flower_patch_species = flower_patches(worlds)
                .map(|(flower_patch, _)| flower_patch.species)
                .collect::<HashSet<_>>()
                .into_iter()
                .collect::<Vec<_>>();

            flower_patch_species.sort_unstable();
        }

        contacts = worlds.iter().map(|world| &world.contacts).fold(
            HashMap::<_, usize>::new(),
            |mut acc, val| {
                for (key, val) in val {
                    let sum = val.0 + val.1.iter().sum::<usize>();

                    *acc.entry(*key).or_default() += sum;
                }

                acc
            },
        );

        observations = worlds.iter().map(|world| &world.observations).fold(
            HashMap::<_, usize>::new(),
            |mut acc, val| {
                for val in val {
                    *acc.entry((val.flower_patch_species, val.insect_species))
                        .or_default() += 1;
                }

                acc
            },
        );
    });

    let print_web = |var, web: &HashMap<_, usize>| {
        println!("{} <- rbind(", var);

        for (flower_patch_idx, flower_patch_species) in flower_patch_species.iter().enumerate() {
            let comma = if flower_patch_idx == 0 { "" } else { ",\n" };
            print!("{}\tc(", comma);

            for (insect_idx, insect_species) in insect_species.iter().enumerate() {
                let cnt = web
                    .get(&(*flower_patch_species, *insect_species))
                    .unwrap_or(&0);

                let comma = if insect_idx == 0 { "" } else { ", " };
                print!("{}{}", comma, cnt);
            }

            print!(")");
        }

        println!(")");
        println!();
    };

    print_web("contacts", &contacts);

    if observations.values().sum::<usize>() != 0 {
        print_web("observations", &observations);
    }
}
