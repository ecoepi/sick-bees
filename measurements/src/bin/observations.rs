use std::env::args_os;

use hashbrown::{HashMap, HashSet};

use measurements::{collect, insects};
use model::{insect::Species as InsectSpecies, observer::Observation};

fn main() {
    let mut insect_species = Vec::new();

    collect(args_os().nth(1).unwrap(), |tick, worlds| {
        if insect_species.is_empty() {
            insect_species = insects(worlds)
                .map(|insect| insect.species)
                .collect::<HashSet<_>>()
                .into_iter()
                .collect::<Vec<_>>();

            insect_species.sort_unstable();
        }

        let observations: Vec<Observation> = worlds.iter().map(|world| &world.observations).fold(
            Default::default(),
            |mut acc, val| {
                acc.extend_from_slice(val);

                acc
            },
        );

        let mut insects = HashMap::<InsectSpecies, usize>::new();
        let mut contaminated = HashMap::<InsectSpecies, usize>::new();
        let mut infected = HashMap::<InsectSpecies, usize>::new();

        for observation in &observations {
            *insects.entry(observation.insect_species).or_default() += 1;

            if observation.contaminated {
                *contaminated.entry(observation.insect_species).or_default() += 1;
            }

            if observation.infected {
                *infected.entry(observation.insect_species).or_default() += 1;
            }
        }

        print!("{}, {}", tick.as_hours(), observations.len());

        for species in &insect_species {
            let insects = insects.get(species).unwrap_or(&0);
            let infected = infected.get(species).unwrap_or(&0);
            let contaminated = contaminated.get(species).unwrap_or(&0);

            print!(", {}, {}, {}", insects, infected, contaminated);
        }

        println!();
    });
}
