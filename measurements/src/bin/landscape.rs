use std::env::args_os;
use std::fs::File;
use std::mem::replace;

use memmap2::MmapOptions;

use dump::restore;
use measurements::{bumblebees, collect, colonies, hoverflies, list, solitary_bees};
use model::{
    flower_patch::Location as FlowerPatchLocation, hoverfly::ClutchLocation, params::Params,
    solitary_bee::NestLocation,
};

fn main() {
    let params: Params = Default::default();

    let dir = args_os().nth(1).unwrap();

    for file in list(&dir, "flower_patch_locations_") {
        let mut buf = unsafe {
            MmapOptions::new()
                .map_copy(&File::open(file).unwrap())
                .unwrap()
        };

        let locations = unsafe { restore::<&[FlowerPatchLocation]>(&mut buf) };

        for location in *locations {
            let x = location.position.x / params.length;
            let y = location.position.y / params.length;

            println!("{} {}", x, y);
        }
    }

    println!();

    for file in list(&dir, "clutch_locations_") {
        let mut buf = unsafe {
            MmapOptions::new()
                .map_copy(&File::open(file).unwrap())
                .unwrap()
        };

        let locations = unsafe { restore::<&[ClutchLocation]>(&mut buf) };

        for location in *locations {
            let x = location.position.x / params.length;
            let y = location.position.y / params.length;

            println!("{} {}", x, y);
        }
    }

    println!();

    for file in list(&dir, "nest_locations_") {
        let mut buf = unsafe {
            MmapOptions::new()
                .map_copy(&File::open(file).unwrap())
                .unwrap()
        };

        let locations = unsafe { restore::<&[NestLocation]>(&mut buf) };

        for location in *locations {
            let x = location.position.x / params.length;
            let y = location.position.y / params.length;

            println!("{} {}", x, y);
        }
    }

    println!();

    let mut first_tick = true;

    collect(&dir, |_tick, worlds| {
        if replace(&mut first_tick, false) {
            for colony in colonies(worlds) {
                let x = colony.location.position.x / params.length;
                let y = colony.location.position.y / params.length;

                println!("{} {}", x, y);
            }

            println!();
        }

        for hoverfly in hoverflies(worlds) {
            let x = hoverfly.insect.position.x / params.length;
            let y = hoverfly.insect.position.y / params.length;

            println!("{} {}", x, y);
        }

        println!();

        for solitary_bee in solitary_bees(worlds) {
            let x = solitary_bee.insect.position.x / params.length;
            let y = solitary_bee.insect.position.y / params.length;

            println!("{} {}", x, y);
        }

        println!();

        for bumblebee in bumblebees(worlds) {
            let x = bumblebee.insect.position.x / params.length;
            let y = bumblebee.insect.position.y / params.length;

            println!("{} {}", x, y);
        }

        println!();
    });
}
