use std::env::args_os;

use hashbrown::HashMap;

use measurements::{collect, flower_patches, insects};
use model::{flower_patch::Species as FlowerPatchSpecies, insect::Species as InsectSpecies};

fn main() {
    collect(args_os().nth(1).unwrap(), |tick, worlds| {
        let mut infected = HashMap::<InsectSpecies, (usize, usize)>::new();

        for insect in insects(worlds) {
            let (insects, infected) = infected.entry(insect.species).or_default();

            *insects += 1;

            if insect.pathogen_status.is_infected() {
                *infected += 1;
            }
        }

        let mut infected = infected.into_iter().collect::<Vec<_>>();

        infected.sort_unstable_by_key(|(species, _)| *species);

        let all_insects = infected
            .iter()
            .map(|(_, (insects, _))| *insects)
            .sum::<usize>();

        let all_infected = infected
            .iter()
            .map(|(_, (_, infected))| *infected)
            .sum::<usize>();

        let mut contaminated = HashMap::<FlowerPatchSpecies, (usize, usize)>::new();

        for (flower_patch, flowers) in flower_patches(worlds) {
            let (all_flowers, all_contaminated_flowers) =
                contaminated.entry(flower_patch.species).or_default();

            *all_flowers += flower_patch.flowers as usize;
            *all_contaminated_flowers += flower_patch.contaminated_flowers.count(flowers) as usize;
        }

        let mut contaminated = contaminated.into_iter().collect::<Vec<_>>();

        contaminated.sort_unstable_by_key(|(species, _)| *species);

        let all_flowers = contaminated
            .iter()
            .map(|(_, (flowers, _))| *flowers)
            .sum::<usize>();

        let all_contaminated_flowers = contaminated
            .iter()
            .map(|(_, (_, contaminated_flowers))| *contaminated_flowers)
            .sum::<usize>();

        print!(
            "{}, {:.3}",
            tick.as_days(),
            all_infected as f64 / all_insects as f64
        );

        for (_, (insects, infected)) in infected {
            print!(", {:.3}", infected as f64 / insects as f64);
        }

        print!(
            ", {:.4}",
            all_contaminated_flowers as f64 / all_flowers as f64
        );

        for (_, (flowers, contaminated_flowers)) in contaminated {
            print!(", {:.4}", contaminated_flowers as f64 / flowers as f64);
        }

        println!();
    });
}
