use std::env::args_os;

use measurements::{clutches, collect, flower_patches, insects, nests};
use model::{
    insect::{Activity as InsectActivity, Species as InsectSpecies},
    params::Params,
    units::{Mass, Volume},
};

fn main() {
    let params: Params = Default::default();

    collect(args_os().nth(1).unwrap(), |tick, worlds| {
        let mut searching_insects = 0;
        let mut collecting_insects = 0;
        let mut all_insects = 0;

        let mut pollen: Volume = Default::default();
        let mut max_pollen: Volume = Default::default();

        for insect in insects(worlds) {
            match insect.activity {
                InsectActivity::SearchFlowerPatch(..) => {
                    searching_insects += 1;
                }
                InsectActivity::CollectNectarAndPollen(..) => {
                    collecting_insects += 1;
                }
                _ => (),
            }

            all_insects += 1;

            pollen += insect.pollen;

            max_pollen += match insect.species {
                InsectSpecies::Hoverfly(species) => {
                    params.hoverflies[species as usize].pollen_per_clutch
                }
                InsectSpecies::SolitaryBee(species) => {
                    params.solitary_bees[species as usize].pollen_per_brood_cell
                }
                InsectSpecies::Bumblebee(species) => {
                    params.bumblebees[species as usize].pollen_per_bout
                }
            };
        }

        let searching = searching_insects as f64 / all_insects as f64;
        let collecting = collecting_insects as f64 / all_insects as f64;

        let pollen = pollen / max_pollen;

        let mut empty_flower_patches = 0;
        let mut all_flower_patches = 0;

        let mut all_nectar: Mass = Default::default();
        let mut max_nectar: Mass = Default::default();

        for (flower_patch, _) in flower_patches(worlds) {
            if flower_patch.nectar == Default::default() {
                empty_flower_patches += 1;
            }

            all_flower_patches += 1;

            all_nectar += flower_patch.nectar;
            max_nectar += flower_patch.nectar_production * flower_patch.flowers as f64;
        }

        let empty = empty_flower_patches as f64 / all_flower_patches as f64;

        let nectar = all_nectar / max_nectar;

        let mut all_eggs = 0;
        let mut max_eggs = 0;

        for clutch in clutches(worlds) {
            all_eggs += clutch.eggs as usize;
            max_eggs += clutch.capacity as usize;
        }

        let eggs = all_eggs as f64 / max_eggs as f64;

        let mut occupied_nests = 0;
        let mut all_nests = 0;

        for nest in nests(worlds) {
            if nest.occupied {
                occupied_nests += 1;
            }

            all_nests += 1;
        }

        let nests = occupied_nests as f64 / all_nests as f64;

        println!(
            "{}, {:.3}, {:.3}, {:.3}, {:.3}, {:.3}, {:.3}, {:.3}",
            tick.as_hours(),
            searching,
            collecting,
            empty,
            pollen,
            nectar,
            eggs,
            nests,
        );
    });
}
