use std::env::args_os;
use std::io::{stdout, BufWriter, Write};

use measurements::collect;
use model::insect::Species as InsectSpecies;

fn main() {
    let stdout = stdout();
    let mut stdout = BufWriter::with_capacity(1024 * 1024, stdout.lock());

    // The two highest bit of the tick determine the record type:
    // 00 => edge = tick|source|destination
    // 10 => node removed = tick|id
    // 11 => node added = tick|id|species
    const MASK: u32 = 1 << 31 | 1 << 30;

    collect(args_os().nth(1).unwrap(), |_tick, worlds| {
        for temporal_network in worlds.iter().map(|world| &world.temporal_network) {
            for (tick, source, destination) in &temporal_network.edges {
                let tick = tick.as_seconds() as u32;
                assert!(MASK & tick == 0);

                stdout.write_all(&tick.to_ne_bytes()).unwrap();
                stdout.write_all(&source.to_ne_bytes()).unwrap();
                stdout.write_all(&destination.to_ne_bytes()).unwrap();
            }

            for (tick, old_node) in &temporal_network.node_removals {
                let mut tick = tick.as_seconds() as u32;
                assert!(MASK & tick == 0);

                tick |= 1 << 31;

                stdout.write_all(&tick.to_ne_bytes()).unwrap();
                stdout.write_all(&old_node.to_ne_bytes()).unwrap();
                stdout.write_all(&[0; 4]).unwrap();
            }

            for (tick, new_node, species) in &temporal_network.node_additions {
                let mut tick = tick.as_seconds() as u32;
                assert!(MASK & tick == 0);

                tick |= 1 << 31;
                tick |= 1 << 30;

                let species = match species {
                    InsectSpecies::Hoverfly(species) => *species as u32,
                    InsectSpecies::SolitaryBee(species) => 256 + *species as u32,
                    InsectSpecies::Bumblebee(species) => 512 + *species as u32,
                };

                stdout.write_all(&tick.to_ne_bytes()).unwrap();
                stdout.write_all(&new_node.to_ne_bytes()).unwrap();
                stdout.write_all(&species.to_ne_bytes()).unwrap();
            }
        }
    });
}
