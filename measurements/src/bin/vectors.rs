use std::env::args_os;

use access::dictionary;
use measurements::collect;
use model::Vectors;

fn main() {
    let mut vectors: Vectors = Default::default();

    collect(args_os().nth(1).unwrap(), |_tick, worlds| {
        vectors =
            worlds
                .iter()
                .map(|world| &world.vectors)
                .fold(Default::default(), |mut acc, val| {
                    for (key, val) in val {
                        *dictionary(&mut acc, *key) += val;
                    }

                    acc
                });
    });

    let all_infections = vectors.iter().map(|(_, val)| val).sum::<usize>();

    let mut flower_patch_species = vectors
        .iter()
        .map(|((species, _, _), _)| *species)
        .collect::<Vec<_>>();

    flower_patch_species.sort_unstable();
    flower_patch_species.dedup();

    let mut insect_species = vectors
        .iter()
        .flat_map(|((_, contaminated_by_species, infected_species), _)| {
            [*contaminated_by_species, *infected_species]
        })
        .collect::<Vec<_>>();

    insect_species.sort_unstable();
    insect_species.dedup();

    println!("infections = {}", all_infections);
    println!();

    for flower_patch_species in &flower_patch_species {
        println!("vectors =");

        for contaminated_by_species in &insect_species {
            print!("\t");

            for infected_species in &insect_species {
                let infections = dictionary(
                    &mut vectors,
                    (
                        *flower_patch_species,
                        *contaminated_by_species,
                        *infected_species,
                    ),
                );

                print!("{:.3} ", *infections as f64 / all_infections as f64);
            }

            println!();
        }

        println!();
    }
}
