use std::env::args_os;

use access::dictionary;
use measurements::collect;
use model::Contacts;

fn main() {
    let mut contacts: Contacts = Default::default();

    collect(args_os().nth(1).unwrap(), |_tick, worlds| {
        contacts =
            worlds
                .iter()
                .map(|world| &world.contacts)
                .fold(Default::default(), |mut acc, val| {
                    for (key, val) in val {
                        let acc = dictionary(&mut acc, *key);

                        acc.0 += val.0;

                        for (acc, val) in acc.1.iter_mut().zip(&val.1) {
                            *acc += *val;
                        }

                        if acc.1.len() < val.1.len() {
                            acc.1.extend_from_slice(&val.1[acc.1.len()..]);
                        }
                    }

                    acc
                });
    });

    let mut flower_patch_species = contacts
        .iter()
        .map(|((species, _), _)| *species)
        .collect::<Vec<_>>();

    flower_patch_species.sort_unstable();
    flower_patch_species.dedup();

    let mut insect_species = contacts
        .iter()
        .map(|((_, species), _)| *species)
        .collect::<Vec<_>>();

    insect_species.sort_unstable();
    insect_species.dedup();

    println!("contacts =");

    for flower_patch_species in &flower_patch_species {
        print!("\t");

        for insect_species in &insect_species {
            let contacts = dictionary(&mut contacts, (*flower_patch_species, *insect_species));

            let sum = contacts.0 + contacts.1.iter().sum::<usize>();

            print!("{} ", sum);
        }

        println!();
    }

    println!();

    for flower_patch_species in &flower_patch_species {
        println!("contact_durations =");

        for insect_species in &insect_species {
            let contacts = dictionary(&mut contacts, (*flower_patch_species, *insect_species));

            print!("\t{}", contacts.0);

            for cnt in &contacts.1 {
                print!(" {}", cnt);
            }

            println!();
        }

        println!();
    }
}
