use std::env::args_os;

use measurements::{collect, insects};

fn main() {
    collect(args_os().nth(1).unwrap(), |tick, worlds| {
        let mut min_visitation_rate = f64::INFINITY;
        let mut max_visitation_rate = f64::NEG_INFINITY;
        let mut sum_visitation_rate = 0.0;
        let mut cnt_insects = 0;

        for insect in insects(worlds) {
            let visitation_rate = insect.visits as f64 / tick.as_seconds() as f64;

            if min_visitation_rate > visitation_rate {
                min_visitation_rate = visitation_rate;
            }

            if max_visitation_rate < visitation_rate {
                max_visitation_rate = visitation_rate;
            }

            sum_visitation_rate += visitation_rate;

            cnt_insects += 1;
        }

        let mean_visitation_rate = sum_visitation_rate / cnt_insects as f64;

        println!(
            "{}, {:.3}, {:.3}, {:.3}",
            tick.as_days(),
            mean_visitation_rate,
            min_visitation_rate,
            max_visitation_rate,
        );
    });
}
