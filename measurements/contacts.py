import numpy as np


def read_contacts(path):
    rows = []

    with open(path, "r") as log_file:
        for line in log_file:
            if line.strip().startswith("contacts ="):
                break
        else:
            return None

        for line in log_file:
            line = line.strip()

            if len(line) == 0:
                break

            cols = np.fromstring(line, sep=" ")
            rows.append(cols)

    data = np.array(rows)
    data /= np.sum(data)

    return data


def read_contact_durations(path):
    data = []

    with open(path, "r") as log_file:
        for line in log_file:
            if line.strip().startswith("contact_durations ="):
                break
        else:
            return None

        rows = []

        for line in log_file:
            line = line.strip()

            if line.startswith("contact_durations =") or len(line) == 0:
                if len(rows) != 0:
                    data.append(rows)
                    rows = []
            else:
                cols = np.fromstring(line, sep=" ", dtype=np.int64)
                rows.append(cols)

    return data


if __name__ == "__main__":
    print(read_contact_durations("/dev/stdin"))
