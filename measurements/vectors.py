import numpy as np


def read_infections(path):
    with open(path, "r") as log_file:
        for line in log_file:
            line = line.strip()

            if line.startswith("infections ="):
                return int(line.split("=")[1])
        else:
            return None


def read_vectors(path):
    data = []

    with open(path, "r") as log_file:
        for line in log_file:
            if line.strip().startswith("vectors ="):
                break
        else:
            return None

        rows = []

        for line in log_file:
            line = line.strip()

            if line.startswith("vectors =") or len(line) == 0:
                if len(rows) != 0:
                    data.append(rows)
                    rows = []
            else:
                cols = np.fromstring(line, sep=" ")
                rows.append(cols)

    return np.array(data)


if __name__ == "__main__":
    print(read_vectors("/dev/stdin"))
