import sys

from PIL import Image, ImageDraw


WIDTH, HEIGHT = 1000, 1000

DURATION = 1000

PALETTE = [
    [0x00, 0x00, 0x00],  # 0 = black = bumblebees and colonies
    [0xFF, 0x00, 0x00],  # 1 = red = solitary bees
    [0x00, 0xFF, 0x00],  # 2 = green = flower patches
    [0x00, 0x00, 0xFF],  # 3 = blue = nests
    [0xFF, 0xFF, 0x00],  # 4 = yellow = hoverflies
    [0xFF, 0x00, 0xFF],  # 5 = magenta = clutches
    [0xBF, 0xB2, 0x80],  # 6 = brown = landscape
]


def draw_section(ctx, size, color):
    half_size = size / 2

    for line in sys.stdin:
        if len(line.rstrip()) == 0:
            return True

        [x, y] = line.split()
        x, y = float(x), float(y)

        x0 = WIDTH * (x - half_size)
        y0 = HEIGHT * (y - half_size)
        x1 = WIDTH * (x + half_size)
        y1 = HEIGHT * (y + half_size)

        ctx.rectangle([x0, y0, x1, y1], fill=color)
    else:
        return False


bg = Image.new("P", (WIDTH, HEIGHT), color=6)
bg.putpalette(sum(PALETTE, []))

ctx = ImageDraw.Draw(bg)

draw_section(ctx, 0.002, 2)
draw_section(ctx, 0.001, 5)
draw_section(ctx, 0.001, 3)
draw_section(ctx, 0.005, 0)

frames = []

while True:
    fg = bg.copy()

    ctx = ImageDraw.Draw(fg)

    if not draw_section(ctx, 0.003, 4):
        break

    if not draw_section(ctx, 0.003, 1):
        break

    if not draw_section(ctx, 0.003, 0):
        break

    frames.append(fg)

frames[0].save(
    sys.stdout.buffer,
    format="gif",
    save_all=True,
    append_images=frames[1:],
    duration=DURATION,
    loop=0,
)
