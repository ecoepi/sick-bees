import sys

import pandas as pd
import matplotlib.pyplot as plt


def plot_prevalence(csv_file, axes, label):
    data = pd.read_csv(csv_file, header=None)

    if label is not None:
        artists = axes.plot(data[0], 100 * data[1], label=label)
        globals()["last_color"] = artists[0].get_color()
    else:
        color = globals().get("last_color", None)
        axes.plot(data[0], 100 * data[1], color=color)

    mean = data[-30:].mean()

    print(
        f"{100 * mean[1]:.1f} {100 * mean[2]:.1f} {100 * mean[3]:.1f} "
        f"{100 * mean[4]:.2f} {100 * mean[5]:.2f} {100 * mean[6]:.2f}",
        file=sys.stderr,
    )


def plot_filtered_prevalence(csv_files, axes, label):
    merged_data = None

    for csv_file in csv_files:
        data = pd.read_csv(csv_file, header=None)

        if merged_data is None:
            merged_data = data[[0, 1]]
        else:
            merged_data[len(merged_data.columns)] = data[1]

    filtered_data = pd.DataFrame(merged_data.loc[:, 0])
    filtered_data[1] = merged_data.loc[:, 1:].quantile(0.5, "columns")
    filtered_data[2] = merged_data.loc[:, 1:].quantile(0.05, "columns")
    filtered_data[3] = merged_data.loc[:, 1:].quantile(0.95, "columns")

    artists = axes.plot(filtered_data[0], 100 * filtered_data[1], label=label)

    axes.fill_between(
        filtered_data[0],
        100 * filtered_data[2],
        100 * filtered_data[3],
        color=artists[0].get_color(),
        alpha=0.25,
    )


if __name__ == "__main__":
    fig, axes = plt.subplots()

    plot_prevalence(sys.stdin, axes, None)

    axes.set_xlabel("Time / d")

    axes.set_ylabel("Prevalence / %")
    axes.set_ylim(0, 100)

    fig.tight_layout()

    plt.savefig(sys.stdout.buffer, format="png")
