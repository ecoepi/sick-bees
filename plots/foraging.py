import sys

import pandas as pd
import matplotlib.pyplot as plt


def plot_foraging_activity(csv_file, axes):
    data = pd.read_csv(csv_file, header=None)

    axes.plot(data[0], 100 * data[1], label="searching")
    axes.plot(data[0], 100 * data[2], label="collecting")
    axes.plot(data[0], 100 * data[3], label="empty")
    axes.plot(data[0], 100 * data[4], label="pollen")
    axes.plot(data[0], 100 * data[5], label="nectar")
    axes.plot(data[0], 100 * data[6], label="eggs")


if __name__ == "__main__":
    fig, axes = plt.subplots()

    plot_foraging_activity(sys.stdin, axes)

    axes.set_xlabel("Time / h")

    axes.set_ylabel("Utilisation / %")
    axes.set_ylim(0, 100)

    axes.legend()

    fig.tight_layout()

    plt.savefig(sys.stdout.buffer, format="png")
