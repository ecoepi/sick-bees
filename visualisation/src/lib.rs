use gdnative::{
    api::{ArrayMesh, Camera, Mesh, MeshInstance, MultiMeshInstance, Node, Viewport},
    core_types::{Basis, Color, Transform, VariantArray, Vector3, Vector3Array},
    godot_gdnative_init, godot_gdnative_terminate, godot_nativescript_init, methods,
    nativescript::InitHandle,
    NativeClass,
};

use model::{params::Params, space::Vector, units::Tick, Model};
use process::SingleThread;

#[derive(NativeClass)]
#[inherit(Node)]
struct Visualisation {
    model: Option<Model>,
    #[property]
    flower_patch_color: Color,
    #[property]
    hoverfly_color: Color,
    #[property]
    solitary_bee_color: Color,
    #[property]
    bumblebee_color: Color,
    #[property]
    pathogen_color: Color,
}

#[methods]
impl Visualisation {
    fn new(_owner: &Node) -> Self {
        let black = Color::rgb(0., 0., 0.);

        Self {
            model: None,
            flower_patch_color: black,
            hoverfly_color: black,
            solitary_bee_color: black,
            bumblebee_color: black,
            pathogen_color: black,
        }
    }

    #[export]
    fn _ready(&mut self, owner: &Node) {
        let model = Model::new(Params::new(Tick::seconds(1) / 25), 0, 1);

        unsafe {
            update_instance_count(owner, "FlowerPatches", model.flower_patch_locations.len());
            update_instance_count(owner, "HoverflyClutches", model.clutch_locations.len());
            update_instance_count(owner, "SolitaryBeeNests", model.nest_locations.len());
            update_instance_count(owner, "BumblebeeColonies", model.world.colonies.len());
            update_instance_count(owner, "Hoverflies", model.world.hoverflies.len());
            update_instance_count(owner, "SolitaryBees", model.world.solitary_bees.len());
            update_instance_count(owner, "Bumblebees", model.world.bumblebees.len());

            update_point_mesh(
                owner,
                "FlowerPatches/Overview",
                fetch_positions(
                    model
                        .flower_patch_locations
                        .iter()
                        .map(|location| &location.position),
                ),
            );

            update_point_mesh(
                owner,
                "HoverflyClutches/Overview",
                fetch_positions(
                    model
                        .clutch_locations
                        .iter()
                        .map(|location| &location.position),
                ),
            );

            update_point_mesh(
                owner,
                "SolitaryBeeNests/Overview",
                fetch_positions(
                    model
                        .nest_locations
                        .iter()
                        .map(|location| &location.position),
                ),
            );

            update_point_mesh(
                owner,
                "BumblebeeColonies/Overview",
                fetch_positions(
                    model
                        .world
                        .colonies
                        .iter()
                        .map(|colony| &colony.location.position),
                ),
            );
        }

        self.model = Some(model);
    }

    #[export]
    fn _physics_process(&mut self, owner: &Node, _delta: f64) {
        let model = self.model.as_mut().unwrap();

        let flower_patch_color = self.flower_patch_color;

        let hoverfly_color = self.hoverfly_color;
        let solitary_bee_color = self.solitary_bee_color;
        let bumblebee_color = self.bumblebee_color;

        let pathogen_color = self.pathogen_color;

        model.run(&mut SingleThread);

        unsafe {
            let camera = owner
                .get_node("Observer/Camera")
                .unwrap()
                .assume_safe()
                .cast::<Camera>()
                .unwrap();

            let viewport = camera
                .upcast::<Node>()
                .get_viewport()
                .unwrap()
                .assume_safe();

            let frustum = Frustum::new(&camera, &viewport);

            update_instance_transforms(
                owner,
                "FlowerPatches",
                fetch_transforms(
                    &frustum,
                    model.flower_patch_locations.iter().map(|location| {
                        let contaminated = model.world.flower_patches[location.index.get()]
                            .contaminated_flowers
                            .count(&model.world.flowers)
                            != 0;

                        let color = if contaminated {
                            pathogen_color
                        } else {
                            flower_patch_color
                        };

                        (&location.position, None, Some(color))
                    }),
                ),
            );

            update_instance_transforms(
                owner,
                "HoverflyClutches",
                fetch_transforms(
                    &frustum,
                    model
                        .clutch_locations
                        .iter()
                        .filter(|location| model.world.clutches[location.index.get()].eggs != 0)
                        .map(|location| (&location.position, None, None)),
                ),
            );

            update_instance_transforms(
                owner,
                "SolitaryBeeNests",
                fetch_transforms(
                    &frustum,
                    model
                        .nest_locations
                        .iter()
                        .filter(|location| model.world.nests[location.index.get()].occupied)
                        .map(|location| (&location.position, None, None)),
                ),
            );

            update_instance_transforms(
                owner,
                "BumblebeeColonies",
                fetch_transforms(
                    &frustum,
                    model
                        .world
                        .colonies
                        .iter()
                        .map(|colony| (&colony.location.position, None, None)),
                ),
            );

            update_instance_transforms(
                owner,
                "Hoverflies",
                fetch_transforms(
                    &frustum,
                    model.world.hoverflies.iter().map(|hoverfly| {
                        let color = if hoverfly.insect.pathogen_status.is_infected() {
                            pathogen_color
                        } else {
                            hoverfly_color
                        };

                        (
                            &hoverfly.insect.position,
                            Some(&hoverfly.insect.movement),
                            Some(color),
                        )
                    }),
                ),
            );

            update_point_mesh(
                owner,
                "Hoverflies/Overview",
                fetch_positions(
                    model
                        .world
                        .hoverflies
                        .iter()
                        .map(|hoverfly| &hoverfly.insect.position),
                ),
            );

            update_instance_transforms(
                owner,
                "SolitaryBees",
                fetch_transforms(
                    &frustum,
                    model.world.solitary_bees.iter().map(|solitary_bee| {
                        let color = if solitary_bee.insect.pathogen_status.is_infected() {
                            pathogen_color
                        } else {
                            solitary_bee_color
                        };

                        (
                            &solitary_bee.insect.position,
                            Some(&solitary_bee.insect.movement),
                            Some(color),
                        )
                    }),
                ),
            );

            update_point_mesh(
                owner,
                "SolitaryBees/Overview",
                fetch_positions(
                    model
                        .world
                        .solitary_bees
                        .iter()
                        .map(|solitary_bee| &solitary_bee.insect.position),
                ),
            );

            update_instance_transforms(
                owner,
                "Bumblebees",
                fetch_transforms(
                    &frustum,
                    model.world.bumblebees.iter().map(|bumblebee| {
                        let color = if bumblebee.insect.pathogen_status.is_infected() {
                            pathogen_color
                        } else {
                            bumblebee_color
                        };

                        (
                            &bumblebee.insect.position,
                            Some(&bumblebee.insect.movement),
                            Some(color),
                        )
                    }),
                ),
            );

            update_point_mesh(
                owner,
                "Bumblebees/Overview",
                fetch_positions(
                    model
                        .world
                        .bumblebees
                        .iter()
                        .map(|bumblebee| &bumblebee.insect.position),
                ),
            );
        }
    }
}

fn init(handle: InitHandle) {
    handle.add_class::<Visualisation>();
}

godot_gdnative_init!();
godot_nativescript_init!(init);
godot_gdnative_terminate!();

fn fetch_transforms<'a, I>(
    frustum: &'a Frustum,
    iter: I,
) -> impl Iterator<Item = (Transform, Option<Color>)> + 'a
where
    I: Iterator<Item = (&'a Vector, Option<&'a Vector>, Option<Color>)> + 'a,
{
    iter.map(|(position, movement, color)| {
        let position = Vector3::new(
            position.x.as_meters() as f32,
            0.,
            position.y.as_meters() as f32,
        );

        (position, movement, color)
    })
    .filter(move |(position, _movement, _color)| frustum.is_visible(position))
    .map(|(position, movement, color)| {
        let mut transform = Transform::translate(position);

        if let Some(movement) = movement {
            let direction = movement.y.atan2(movement.x) as f32;

            transform.basis = Basis::from_axis_angle(&Vector3::new(0., -1., 0.), direction);
        }

        (transform, color)
    })
}

unsafe fn update_instance_count(owner: &Node, node: &str, len: usize) {
    let multimesh = owner
        .get_node(node)
        .unwrap()
        .assume_safe()
        .cast::<MultiMeshInstance>()
        .unwrap()
        .multimesh()
        .unwrap();

    multimesh.assume_safe().set_instance_count(len as i64);
}

unsafe fn update_instance_transforms<I>(owner: &Node, node: &str, transforms: I)
where
    I: Iterator<Item = (Transform, Option<Color>)>,
{
    let multimesh = owner
        .get_node(node)
        .unwrap()
        .assume_safe()
        .cast::<MultiMeshInstance>()
        .unwrap()
        .multimesh()
        .unwrap();

    let multimesh = multimesh.assume_safe();

    let mut cnt = 0;

    for (idx, (transform, color)) in transforms.enumerate() {
        multimesh.set_instance_transform(idx as i64, transform);

        if let Some(color) = color {
            multimesh.set_instance_color(idx as i64, color);
        }

        cnt += 1;
    }

    multimesh.set_visible_instance_count(cnt);
}

fn fetch_positions<'a, I>(iter: I) -> Vector3Array
where
    I: ExactSizeIterator<Item = &'a Vector>,
{
    let mut positions = Vector3Array::new();

    positions.resize(iter.len() as i32);

    {
        let mut positions = positions.write();
        let positions = positions.as_mut_slice();

        for (src, dst) in iter.zip(positions) {
            *dst = Vector3::new(src.x.as_meters() as f32, 0., src.y.as_meters() as f32);
        }
    }

    positions
}

unsafe fn update_point_mesh(owner: &Node, node: &str, vertices: Vector3Array) {
    let mesh = ArrayMesh::new();

    if !vertices.is_empty() {
        let arrays = VariantArray::new();
        arrays.resize(ArrayMesh::ARRAY_MAX as i32);
        arrays.set(ArrayMesh::ARRAY_VERTEX as i32, vertices);

        mesh.add_surface_from_arrays(
            Mesh::PRIMITIVE_POINTS,
            arrays.into_shared(),
            VariantArray::new().into_shared(),
            0,
        );
    }

    owner
        .get_node(node)
        .unwrap()
        .assume_safe()
        .cast::<MeshInstance>()
        .unwrap()
        .set_mesh(mesh);
}

struct Frustum<'a> {
    camera: &'a Camera,
    pos: Vector3,
    dir: Vector3,
    far: f32,
    near: f32,
    width: f32,
    height: f32,
}

impl<'a> Frustum<'a> {
    fn new(camera: &'a Camera, viewport: &Viewport) -> Self {
        let global_transform = camera.global_transform();
        let pos = global_transform.origin;
        let dir = global_transform.basis.z().normalize();

        let far = camera.zfar() as f32;
        let near = camera.znear() as f32;

        let viewport_size = viewport.size();
        let width = viewport_size.x;
        let height = viewport_size.y;

        Self {
            camera,
            pos,
            dir,
            far,
            near,
            width,
            height,
        }
    }

    fn is_visible(&self, pos: &Vector3) -> bool {
        let dist = self.dir.dot(self.pos - *pos);

        if dist < self.near || self.far < dist {
            return false;
        }

        let pos = self.camera.unproject_position(*pos);

        pos.x >= 0. && pos.x <= self.width && pos.y >= 0. && pos.y <= self.height
    }
}
