shader_type spatial;
render_mode unshaded;

uniform float point_size = 1.0;
uniform vec3 color = vec3(1.0);

void vertex() {
    POINT_SIZE = point_size;
}

void fragment() {
    ALBEDO = color;
}