extends MeshInstance

var camera

func _ready():
    camera = $Camera

    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):
    if Input.is_action_pressed("ui_cancel"):
        get_tree().quit()

    var movement = Vector3()

    if Input.is_action_pressed("move_forward"):
        movement.z -= 1
    if Input.is_action_pressed("move_backward"):
        movement.z += 1
    if Input.is_action_pressed("move_left"):
        movement.x -= 1
    if Input.is_action_pressed("move_right"):
        movement.x += 1
    if Input.is_action_pressed("move_up"):
        movement.y += 1
    if Input.is_action_pressed("move_down"):
        movement.y -= 1

    movement = movement.normalized()

    if Input.is_action_pressed("move_fast"):
        movement *= 100 * delta
    else:
        movement *= 10 * delta

    self.translate(movement)

    self.translation.y = clamp(self.translation.y, 0.1, 100)

func _input(event):
    if event is InputEventMouseMotion:
        self.rotate_y(deg2rad(event.relative.x * -0.1))

        camera.rotate_x(deg2rad(event.relative.y * -0.1))

        var rotation = camera.rotation_degrees
        rotation.x = clamp(rotation.x, -70, 70)
        camera.rotation_degrees = rotation
