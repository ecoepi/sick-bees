extends MeshInstance

export var diameter = 1000.0

func _ready():
    var tool_ = SurfaceTool.new()

    tool_.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)

    tool_.add_normal(Vector3(0, 1, 0))
    tool_.add_vertex(Vector3(0, 0, 0))

    tool_.add_normal(Vector3(0, 1, 0))
    tool_.add_vertex(Vector3(diameter, 0, 0))

    tool_.add_normal(Vector3(0, 1, 0))
    tool_.add_vertex(Vector3(0, 0, diameter))

    tool_.add_normal(Vector3(0, 1, 0))
    tool_.add_vertex(Vector3(diameter, 0, diameter))

    mesh = tool_.commit()
