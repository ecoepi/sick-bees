import multiprocessing

import numpy as np
import matplotlib.pyplot as plt

import rpy2.robjects as robjects
from rpy2.rinterface_lib import callbacks


scenarios = [
    "broad_diet",
    "broad_diet_more_sampling",
    "broad_diet_even_more_sampling",
    "broad_diet_more_handling",
    "broad_diet_even_more_handling",
    "narrow_diet",
    "narrow_diet_more_sampling",
    "narrow_diet_even_more_sampling",
    "narrow_diet_more_handling",
    "narrow_diet_even_more_handling",
]

runs = 50

r = robjects.r
callbacks.consolewrite_print = lambda s: None
callbacks.consolewrite_warnerror = lambda s: None

r("library(bipartite)")


def load_data(scenario):
    contacts = []
    observations = []

    for run in range(1, runs + 1):
        r["source"](f"{scenario}_{run}/bipartite.R")

        contacts.append(r('networklevel(contacts)[["weighted connectance"]]')[0])

        observations.append(
            r('networklevel(observations)[["weighted connectance"]]')[0]
        )

    contacts = np.array(contacts)
    observations = np.array(observations)

    return (scenario, contacts, observations)


errors = {}

with multiprocessing.Pool() as pool:
    data = pool.imap_unordered(load_data, scenarios)

    for (scenario, contacts, observations) in data:
        errors[scenario] = contacts - observations


def save_boxplot(plots, labels, ylabel, file_name):
    fig, axes = plt.subplots()

    axes.axhline(0)
    axes.tick_params(axis="x", which="major", labelsize=8)
    axes.boxplot(plots, labels=labels, showmeans=True, whis=(5, 95))

    axes.set_ylabel(ylabel)

    fig.tight_layout()
    fig.savefig(file_name)


plots = [
    errors["broad_diet"],
    errors["broad_diet_more_sampling"],
    errors["broad_diet_even_more_sampling"],
]
labels = ["2h", "20h", "200h"]

save_boxplot(
    plots,
    labels,
    "Weighted connectance / 1",
    "sampling_effort_broad_diet_weighted_connectance.pdf",
)

plots = [
    errors["narrow_diet"],
    errors["narrow_diet_more_sampling"],
    errors["narrow_diet_even_more_sampling"],
]
labels = ["2h", "20h", "200h"]

save_boxplot(
    plots,
    labels,
    "Weighted connectance / 1",
    "sampling_effort_narrow_diet_weighted_connectance.pdf",
)

plots = [
    errors["broad_diet"],
    errors["broad_diet_more_handling"],
    errors["broad_diet_even_more_handling"],
]
labels = ["1min", "10min", "100min"]

save_boxplot(
    plots,
    labels,
    "Weighted connectance / 1",
    "handling_time_broad_diet_weighted_connectance.pdf",
)

plots = [
    errors["narrow_diet"],
    errors["narrow_diet_more_handling"],
    errors["narrow_diet_even_more_handling"],
]
labels = ["1min", "10min", "100min"]

save_boxplot(
    plots,
    labels,
    "Weighted connectance / 1",
    "handling_time_narrow_diet_weighted_connectance.pdf",
)
