\documentclass{beamer}

\usepackage{hyperref}

\usepackage{sourcecodepro}
\usepackage{listings}

\usepackage{tikz}
\usetikzlibrary{overlay-beamer-styles,backgrounds,matrix}

\mode<presentation>
{
\usetheme{Pittsburgh}
}

\AtBeginSection[]
{
\begin{frame}{Outline}

\tableofcontents[currentsection]

\end{frame}
}

\setbeameroption{show notes}

\title{Simulating the VOODOO experimental protocol}
\author{EcoEpi}

\tikzstyle{insect} = [circle,text=white,minimum size=0.75cm]
\tikzstyle{plant} = [circle,fill=green,minimum size=0.75cm]

\tikzstyle{inset} = [fill=white,draw=black,scale=0.75]

\newcommand{\broaddiet}{\begin{tikzpicture}[->,node distance=1.5cm]

\node[insect,fill=red] at (0,0) (insect-1) {1};
\node[insect,fill=blue] (insect-2) [right of=insect-1] {2};

\node[plant] (plant-1) [below of=insect-1] {};
\node[plant] (plant-2) [right of=plant-1] {};
\node[plant] (plant-3) [right of=plant-2] {};

\path
    (insect-1) edge[red] node {} (plant-1)
    (insect-1) edge[red] node {} (plant-2)
    (insect-1) edge[red] node {} (plant-3)
    (insect-2) edge[blue] node {} (plant-1)
    (insect-2) edge[blue] node {} (plant-2)
    (insect-2) edge[blue] node {} (plant-3);

\end{tikzpicture}}

\newcommand{\narrowdiet}{\begin{tikzpicture}[->,node distance=1.5cm]

\node[insect,fill=red] at (0,0) (insect-1) {1};
\node[insect,fill=blue] (insect-2) [right of=insect-1] {2};

\node[plant] (plant-1) [below of=insect-1] {};
\node[plant] (plant-2) [right of=plant-1] {};
\node[plant] (plant-3) [right of=plant-2] {};

\path
    (insect-1) edge[red] node {} (plant-1)
    (insect-2) edge[blue] node {} (plant-1)
    (insect-2) edge[blue] node {} (plant-2)
    (insect-2) edge[blue] node {} (plant-3);

\end{tikzpicture}}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}{Outline}

\tableofcontents

\end{frame}

\section{Methods}

\begin{frame}{Simplified protocol}
\begin{center}
\includegraphics[scale=0.2]{transect-walk}
\end{center}
\end{frame}

\note[itemize]
{
\item Transect geometry and orientation is fixed

\item But the landscape is randomly generated for each simulation run

\item Flower patches are placed uniformly throughout the landscape

\item Plant species are assigned with equal weights as well
}

\begin{frame}{Simplified protocol}
\begin{itemize}

\pause
\item simulated observer walks with constant speed; reaches end of transect after given duration

\pause
\item looks for insects foraging at flower patches within given distance; closest one is caught

\pause
\item clock is stopped and movement suspended until \emph{handling time} has elapsed; but simulation continues

\pause
\item as clock is stopped while preparing specimen; transect duration determines \emph{sampling effort}

\end{itemize}
\end{frame}

\begin{frame}{Abstract results}
\begin{itemize}

\pause
\item results are \emph{number of specimen $n_s$} of species $s$ and \emph{number of infected specimen $i_s$} of species $s$

\pause
\item derived from this are species ratio and specific prevalance
\begin{displaymath}
r_s := \frac{n_s}{\sum_r n_r} \qquad p_s := \frac{i_s}{n_s}
\end{displaymath}

\pause
\item errors of relative abundance $a_s$ and share of contacts $c_s$ are given by
\begin{displaymath}
\Delta a_s := r_s - a_s \qquad \Delta c_s := r_s - c_s
\end{displaymath}

\pause
\item number of specimen $m_{sp}$ of species $s$ caught on plant species $p$ define web as understood by bipartite R package

\end{itemize}
\end{frame}

\note[itemize]
{
\item All box plots show 50 simulation runs

\item Boxes are 25th to 75th percentile

\item Whiskers are 5th to 95th percentile
}
\begin{frame}{Basic scenarios}
\begin{columns}

\begin{column}{0.5\textwidth}
\begin{itemize}

\item<2-> in \emph{broad diet} scenario, connectance is $1$ and both species have equal number of flower patches available

\item<3-> in \emph{narrow diet} scenario, connectance is $2/3$ and species $1$ has fewer available flower patches compared to species $2$

\end{itemize}
\end{column}

\begin{column}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[->,node distance=1.5cm,ampersand replacement=\&]

\matrix[matrix of nodes,row sep=0.5cm]
{

\broaddiet

\\

\narrowdiet

\\
};

\end{tikzpicture}
\end{center}
\end{column}

\end{columns}
\end{frame}

\section{Results}

\begin{frame}{Prevalence over sampling effort}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{sampling_effort_narrow_diet_prevalence}};
\end{scope}

\node[inset] at (4,3) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item Number of specimen increases with sampling effort

\item Therefore variance of prevalence decreases with sampling effort

\item Specific prevalences are estimated consistently but overall prevalence is not

\item Species 1 has more contacts than species 2, hence more specimen, hence less variance

\item Sample median and mean can differ significantly as Binomial distribution with small $n$ and $p \approx 0$ or $p \approx 1$ is skewed
}

\begin{frame}{Abundance over sampling effort}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{sampling_effort_narrow_diet_abundance}};
\end{scope}

\node[inset] at (4,0) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item Explains consistent estimate of specific prevalence but inconsistent estimate of overall prevalence

\item Meaning the source of the inconsistencies is the estimate of the relative abundances

\item Species 1 has more contacts than species 2, hence is catched disproportionally more often as sampling effort increases

\item With low sampling effort, species 1 is catched less often as it is found only on a third of the flower patches
}

% \begin{frame}{Abundance over sampling effort}
% \begin{center}
% \begin{tikzpicture}

% \begin{scope}[on background layer]
% \node {\includegraphics[width=0.9\textwidth]{sampling_effort_broad_diet_abundance}};
% \end{scope}

% \node[inset] at (4,3) {\broaddiet};

% \end{tikzpicture}
% \end{center}
% \end{frame}

% \begin{frame}{Share of contacts over sampling effort}
% \begin{center}
% \begin{tikzpicture}

% \begin{scope}[on background layer]
% \node {\includegraphics[width=0.9\textwidth]{sampling_effort_broad_diet_contacts}};
% \end{scope}

% \node[inset] at (4,3) {\broaddiet};

% \end{tikzpicture}
% \end{center}
% \end{frame}

\begin{frame}{Share of contacts over sampling effort}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{sampling_effort_narrow_diet_contacts}};
\end{scope}

\node[inset] at (4,3) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item Explains increasing inconsistency of abundance estimate

\item Meaning transect increases to approximate contact network as sampling effort increases

\item Share of contacts is aggregated over all plant species

\item Results are similar for each insect-species-plant-species pair, but much more noisy
}

\begin{frame}{Intuition}
\begin{itemize}

\item \emph{Short} sampling means capturing \emph{relative abundances}

\item \emph{Long} sampling means capturing \emph{contact network}

\pause
\item Better to have fewer but longer sessions, e.g. fewer sites or repetitions but stay in the field as long as possible

\end{itemize}
\end{frame}

\begin{frame}{Explanation}
\begin{itemize}

\pause
\item At $0$, an instantenous snapshot of the transect area is taken

\pause
\item The spatial distribution of insects is uniform

\pause
\item[\Rightarrow] The relative abundances in the transect area will reproduce the overall values

\end{itemize}
\end{frame}

\begin{frame}{Explanation}
\begin{itemize}

\pause
\item At $\infty$, the observer stands still and collects all insect visiting the flower patches within its range of visibility

\pause
\item As the model dynamics appear ergodic, i.e. each flower patch is eventually visited by every insect

\pause
\item[\Rightarrow] The time average of the visits to a single flower patch will reproduce the ensemble average of the visits to all flower patches of that plant species

\end{itemize}
\end{frame}

% \begin{frame}{Abundance over handling time}
% \begin{center}
% \begin{tikzpicture}

% \begin{scope}[on background layer]
% \node {\includegraphics[width=0.9\textwidth]{handling_time_broad_diet_abundance}};
% \end{scope}

% \node[inset] at (4,3) {\broaddiet};

% \end{tikzpicture}
% \end{center}
% \end{frame}

\begin{frame}{Abundance over handling time}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{handling_time_narrow_diet_abundance}};
\end{scope}

\node[inset] at (4,3) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item Effect of handling time is inverted compared to sampling effort

\item Does not converge against share of contacts, i.e. handling time is pure sampling error
}

\begin{frame}[fragile]{Network metrics}
\begin{lstlisting}[language=R,basicstyle=\scriptsize\ttfamily]
library(bipartite)

contacts <- rbind(
        c(487828465, 4071455),
        c(0, 3646230),
        c(0, 3587070))

contacts_results <- networklevel(web = contacts)

observations <- rbind(
        c(4, 2),
        c(0, 4),
        c(0, 4))

observations_results <- networklevel(web = observations)
\end{lstlisting}
\end{frame}

\note[itemize]
{
\item \texttt{contacts} are all insect-flower-patch contacts recorded during the simulation

\item \texttt{observations} are the insect-flower-patch combinations observed during the transect

\item Columns represent the insect species

\item Rows represent the plant species
}

% \begin{frame}{Connectance over sampling effort}
% \begin{center}
% \begin{tikzpicture}

% \begin{scope}[on background layer]
% \node {\includegraphics[width=0.9\textwidth]{sampling_effort_broad_diet_weighted_connectance}};
% \end{scope}

% \node[inset] at (4,3) {\broaddiet};

% \end{tikzpicture}
% \end{center}
% \end{frame}

\begin{frame}{Connectance over sampling effort}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{sampling_effort_narrow_diet_weighted_connectance}};
\end{scope}

\node[inset] at (4,0) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}{Connectance over handling time}
\begin{center}
\begin{tikzpicture}

\begin{scope}[on background layer]
\node {\includegraphics[width=0.9\textwidth]{handling_time_narrow_diet_weighted_connectance}};
\end{scope}

\node[inset] at (4,-1.75) {\narrowdiet};

\end{tikzpicture}
\end{center}
\end{frame}

\end{document}
