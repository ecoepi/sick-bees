import os
import sys

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

script_dir = os.path.realpath(os.path.dirname(__file__))
sys.path.append(f"{script_dir}/../../measurements")

from contacts import read_contacts


def add_boxplot(axes, plots, labels, ylabel):
    axes.axhline(0)
    axes.tick_params(axis="x", which="major", labelsize=8)
    axes.boxplot(plots, labels=labels, showmeans=True, whis=(5, 95))

    axes.set_ylabel(ylabel)


EFFORTS = [("", "2h"), ("_more_sampling", "20h"), ("_even_more_sampling", "200h")]
STRATEGIES = [("", "walk"), ("_sit_3", "sit, 3x"), ("_sit_7", "sit, 7x")]

fig, axes = plt.subplots(3, 1, figsize=[6.4, 3 * 4.8], sharex=True)

for (axis, (effort, effort_label)) in zip(axes, EFFORTS):
    labels = []
    plots = []

    for (strategy, strategy_label) in STRATEGIES:
        errors_species_1 = []
        errors_species_2 = []

        for run in range(1, 51):
            dir_name = f"narrow_diet{effort}{strategy}_{run}"

            data = pd.read_csv(f"{dir_name}/observations.csv", header=None)
            observations = data.iloc[-1]

            data = read_contacts(f"{dir_name}/contacts.log")
            contacts = np.sum(data, axis=0)

            species_1 = observations[2]
            species_2 = observations[5]
            species_12 = species_1 + species_2

            if species_12 == 0:
                continue

            ratio_species_1 = species_1 / species_12
            ratio_species_2 = species_2 / species_12

            errors_species_1.append(ratio_species_1 - contacts[0])
            errors_species_2.append(ratio_species_2 - contacts[1])

        labels.append("species 1\n" + strategy_label)
        labels.append("species 2\n" + strategy_label)

        plots.append(np.array(errors_species_1))
        plots.append(np.array(errors_species_2))

    add_boxplot(axis, plots, labels, "Share of contacts / 1")

    axis.set_title(effort_label)

fig.tight_layout()
fig.savefig("strategies.png")
