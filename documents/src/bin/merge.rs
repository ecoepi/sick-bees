use std::ffi::OsStr;
use std::fs::{read_to_string, write};

use documents::{
    for_each_file, is_tex_begin_code, is_tex_end_code, source_directories, Fallible, RS_BEGIN_CODE,
    RS_END_CODE,
};

fn main() -> Fallible {
    for_each_file(source_directories(), OsStr::new("tex"), |path| {
        let tex_input = read_to_string(&path)?;
        let rs_input = read_to_string(&path.with_extension("rs"))?;
        let mut tex_output = Vec::new();

        {
            let mut rs_lines = rs_input.lines();
            let mut within_code = false;

            for tex_line in tex_input.lines() {
                if !within_code && is_tex_begin_code(tex_line) {
                    tex_output.push(tex_line);

                    for rs_line in &mut rs_lines {
                        if rs_line.trim_start() == RS_BEGIN_CODE {
                            continue;
                        } else if rs_line.trim_start() == RS_END_CODE {
                            break;
                        }

                        tex_output.push(rs_line);
                    }

                    within_code = true;
                    continue;
                } else if within_code && is_tex_end_code(tex_line) {
                    tex_output.push(tex_line);

                    within_code = false;
                    continue;
                }

                if !within_code {
                    tex_output.push(tex_line);
                }
            }
        }

        let tex_output = tex_output.join("\n");

        if tex_input != tex_output {
            write(&path, &tex_output)?;
        }

        Ok(())
    })
}
