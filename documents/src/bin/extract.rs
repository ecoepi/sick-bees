use std::ffi::OsStr;
use std::fs::{read, read_to_string, write};
use std::io::Write;

use documents::{
    for_each_file, is_tex_begin_code, is_tex_end_code, source_directories, Fallible, RS_BEGIN_CODE,
    RS_END_CODE,
};

fn main() -> Fallible {
    for_each_file(source_directories(), OsStr::new("tex"), |path| {
        let input = read_to_string(&path)?;
        let mut output = Vec::new();

        let mut lines = input.lines().peekable();
        let mut any_code = false;
        let mut within_code = false;

        while let Some(line) = lines.next() {
            if !within_code && is_tex_begin_code(line) {
                let indent = lines.peek().map_or(0, |line| {
                    line.chars()
                        .take_while(|char_| char_.is_whitespace())
                        .count()
                });

                if any_code {
                    writeln!(output, "{2:1$}{0}", RS_END_CODE, indent, "")?;
                }

                writeln!(output, "{2:1$}{0}", RS_BEGIN_CODE, indent, "")?;

                any_code = true;
                within_code = true;
                continue;
            } else if within_code && is_tex_end_code(line) {
                within_code = false;
                continue;
            }

            if within_code {
                writeln!(output, "{}", line)?;
            }
        }

        if any_code {
            writeln!(output, "{}", RS_END_CODE)?;
        }

        if within_code {
            return Err(format!("Incomplete code block in file `{}`", path.display()).into());
        }

        let rs_path = path.with_extension("rs");

        if !rs_path.exists() || read(&rs_path)? != output {
            write(&rs_path, &output)?;
        }

        Ok(())
    })
}
