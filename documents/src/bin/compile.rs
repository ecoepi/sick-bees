use std::env::args;
use std::ffi::OsStr;
use std::fs::remove_file;
use std::path::Path;
use std::process::Command;

use documents::{for_each_file, Fallible};

fn main() -> Fallible {
    let filters = args().skip(1).collect::<Vec<_>>();

    for_each_file(vec!["documents".into()], OsStr::new("tex"), |path| {
        if !filters.is_empty() {
            let path = path.to_str().unwrap();

            if !filters.iter().any(|filter| path.contains(filter)) {
                return Ok(());
            }
        }

        run_python3(path)?;

        run_lualatex(path)?;

        run_biber(path)?;

        run_lualatex(path)?;
        run_lualatex(path)?;

        remove_temporary_files(path);

        Ok(())
    })
}

fn run_python3(path: &Path) -> Fallible {
    let path = path.with_extension("py");

    if !path.exists() {
        return Ok(());
    }

    let status = Command::new("python3")
        .current_dir(path.parent().unwrap())
        .arg(path.file_name().unwrap())
        .status()?;

    if status.success() {
        Ok(())
    } else {
        Err(format!("Failed to run python3: {:?}", status).into())
    }
}

fn run_lualatex(path: &Path) -> Fallible {
    let status = Command::new("lualatex")
        .arg("-interaction=batchmode")
        .current_dir(path.parent().unwrap())
        .arg(path.file_name().unwrap())
        .status()?;

    if status.success() {
        Ok(())
    } else {
        Err(format!("Failed to run lualatex: {:?}", status).into())
    }
}

fn run_biber(path: &Path) -> Fallible {
    if !path.with_extension("bcf").exists() {
        return Ok(());
    }

    let status = Command::new("biber")
        .arg("--quiet")
        .current_dir(path.parent().unwrap())
        .arg(path.file_stem().unwrap())
        .status()?;

    if !status.success() {
        return Err(format!("Failed to run biber: {:?}", status).into());
    }

    Ok(())
}

fn remove_temporary_files(path: &Path) {
    let mut path = path.to_owned();

    for extension in &[
        "log", "aux", "out", "toc", "bbl", "bcf", "blg", "nav", "snm", "run.xml",
    ] {
        path.set_extension(extension);

        let _ = remove_file(&path);
    }
}
