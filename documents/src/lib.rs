use std::error::Error;
use std::ffi::OsStr;
use std::fs::read_dir;
use std::path::{Path, PathBuf};

use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

pub fn is_tex_begin_code(line: &str) -> bool {
    line == "\\begin{code}" || line == "\\begin{hiddencode}"
}

pub fn is_tex_end_code(line: &str) -> bool {
    line == "\\end{code}" || line == "\\end{hiddencode}"
}

pub const RS_BEGIN_CODE: &str = "// begin code";
pub const RS_END_CODE: &str = "// end code";

pub fn source_directories() -> Vec<PathBuf> {
    vec![
        "src".into(),
        "experiments/src".into(),
        "measurements/src".into(),
    ]
}

pub type Fallible<T = ()> = Result<T, Box<dyn Error + Send + Sync>>;

pub fn for_each_file<F>(mut paths: Vec<PathBuf>, extension: &OsStr, f: F) -> Fallible
where
    F: Send + Sync + Fn(&Path) -> Fallible,
{
    let mut files = Vec::new();

    while let Some(path) = paths.pop() {
        for entry in read_dir(path)? {
            let entry = entry?;
            let path = entry.path();

            if entry.file_type()?.is_dir() {
                paths.push(path);
            } else if path.extension() == Some(extension) {
                files.push(path);
            }
        }
    }

    files.par_iter().try_for_each(|path| f(path))?;

    Ok(())
}
