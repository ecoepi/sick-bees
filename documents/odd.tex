\documentclass[a4paper,10pt]{scrartcl}

\usepackage{amsmath}
\usepackage{environ}
\usepackage{hyperref}
\usepackage{arydshln}

\usepackage{geometry}
\geometry{left=2cm,right=3cm,top=2cm,bottom=2cm}

\usepackage{biblatex}
\bibliography{references}

\usepackage{sourcecodepro}
\usepackage{listings}

\lstdefinelanguage{Rust}
{
    sensitive=true,
    morecomment=[l]{//},
    morecomment=[s]{/*}{*/},
    morestring=[b]{"},
    morestring=[b]{'},
    moredelim=[s][\itshape]{\#[}{]},
    morekeywords={break, continue, else, for, if, in, loop, match, return, while},
    morekeywords={as, const, let, move, mut, ref, static},
    morekeywords={dyn, enum, fn, impl, Self, self, struct, trait, type, union, use, where},
    morekeywords={crate, extern, mod, pub, super},
    morekeywords={unsafe},
    morekeywords={macro_rules},
}

\lstnewenvironment{code}{\lstset{language=Rust, basicstyle=\scriptsize\ttfamily, backgroundcolor=\color[gray]{0.95}}}{}
\newcommand{\inlinecode}[1]{\texttt{\scriptsize \lstinline|#1|}}
\NewEnviron{hiddencode}{}

\usepackage{tikz}
\usetikzlibrary{arrows,automata,positioning,shapes}

\author{EcoEpi}
\title{sick-bees}
\subtitle{Overview, design concepts and details}

\begin{document}

\tableofcontents

\section{Purpose}

The purpose of the \href{https://git.ufz.de/ecoepi/sick-bees}{sick-bees} model is to investigate how the shape of plant-pollinator interaction networks determines pathogen dynamics in mixed communities of hoverflies, solitary bees, bumblebees and flowering plants.

\section{Design concepts}

\begin{itemize}

\item \emph{Emergence:} Specific production rates of flowering plants and specific consumption rates of insects are imposed. The resulting balance of production and consumption of the whole community emerges via the individual foraging activity. Specific abundances of insects and flowering plants are imposed as reproduction and mortality are not considered. Specific preferences of the insects for the plants are imposed, but the resulting contact network and pathogen prevalences emerge via the individual foraging activity.

\item \emph{Adaptation:} No adaptive traits are considered.

\item \emph{Fitness:} Fitness seeking is modelled implicitly as insects exploit floral resources to reach the largest possible reproduction rate.

\item \emph{Prediction:} Insects memorise a finite number of depleted resources like empty flower patches or occupied nests and assume this state to persist until newer memories replace the older ones.

\item \emph{Sensing:} Insects sense resources like flower patches, clutches and nests in close proximity but have to interact directly with them to determine if they are available. Insects also track the amount of pollen they have collected to decide when to engage in their reproductive activities.

\item \emph{Interaction:} All insect interactions are mediated via the flower patches, i.e. insects interact only indirectly by visiting the same flower patch. Insects are assumed to collect nectar and pollen at flower patches. Pathogen transmission is mediated via contaminated flowers. Insects use the floral resources they gain for their reproductive activities by interacting with a clutch, nest respectively colony.

\item \emph{Stochasticity:} Stochasticity is used during initialisation to account for the variability in distribution of floral resources. It is also used during the search phase of foraging as it is the implementation based on smallest number of assumptions on the individual decision making process. Finally, it is used to implement pathogen model state transitions where a phenomenological formulation of the underlying microscopic processes is applied.

\item \emph{Collectives:} Each bumblebee is considered to belong to a colony to which it always returns and from which it always begins its foraging bouts.

\item \emph{Observation:} Macro observables like nutrient balances and pathogen prevalences are evaluated by periodically dumping the complete state to disk for offline analysis and visualisation. Micro observables like contact rates and transmission vectors are realised by instrumenting the model rules to aggregate the relevant events in the world data structure so they become available via the periodic dumps.

\end{itemize}

\input{../src/lib.tex}

\section{Submodels}

The submodels are organised along the different entities instead of the processes. Strictly speaking, this is a deviation from the ODD protocol \autocite{the_odd_protocol} with the intention of making it easier to convey the software design as a literate program \autocite{literate_programming}. Generally, there is one submodel per type of entity, but the insect submodel captures behaviour that is shared between the three pollinator families. This object-oriented design was chosen over an earlier entity-component-system-based approach to keep the state variables for all processes related to a single entity within one contiguous data structure which can be easily transferred between processes via message passing.

As the model state is distributed among the entities and compartmentalised in the submodels, each submodel description mirrors the model at large. This means that each section describes the state variables associated with an entity, how they are initialised and how they are updated each time step. If applicable, the sections also describe auxiliary entities like for example the colonies associated with the bumblebees.

The submodels describing the mobile entities are implemented as finite state machines \autocite{state_machines}, i.e. it is assumed that each entity has a current activity which mostly determines which processes are considered for the current iteration and which are not, e.g. a hoverfly will either move through the landscape searching for a clutch site or searching for a flower patch, but never to search for both at the same time as these are modelled as distinct activities.

For reasons of computational efficiency, some parts of the submodels of the stationary entities are technically implemented in the submodels of the mobile entities interacting with them, e.g. a flower patch is marked as depleted by the code which simulates the insect that reduces its resources to zero.

\input{../src/flower_patch.tex}
\input{../src/insect.tex}
\input{../src/hoverfly.tex}
\input{../src/solitary_bee.tex}
\input{../src/bumblebee.tex}
\input{../src/pathogen.tex}
\input{../src/observer.tex}

\input{../src/params.tex}

\input{../src/init.tex}

\appendix

\section{Technicalities}

\input{../src/space.tex}

\input{../src/grid.tex}

\input{../src/units.tex}

\printbibliography

\end{document}
