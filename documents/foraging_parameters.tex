\documentclass[a4paper,10pt]{scrartcl}

\usepackage{amsmath}

\usepackage{geometry}
\geometry{left=1.5cm,right=1.5cm,top=3cm,bottom=3cm}

\usepackage{pgfplots}
\usepgfplotslibrary{fillbetween}
\pgfplotsset{compat=1.16, width=10cm}

\usepackage{hyperref}

\usepackage{biblatex}
\bibliography{references}

\title{Expert knowledge elicitation on foraging parameters}
\author{EcoEpi}

\begin{document}

\section{Expert knowledge elicitation on foraging parameters}

\subsection{Problem}

In addition to the qualitative differences in foraging behaviour between our three main insect types (hover flies, solitary bees and bumble bees) presented in our call on modelling concepts, there are four important quantitative parameters that govern the foraging processes of the individual animals:
\begin{enumerate}
\item The rate at which nectar is collected in mass per unit time during foraging.
\item The rate at which pollen are collected in volume per unit time during foraging.
\item The volume of pollen necessary per reproductive unit, e.g. per egg or brood cell.
\item The distance at which flowering plants are discovered by visual or olfactory sensing.
\end{enumerate}

Note that the mass of nectar is assumed to refer to the sugar content only as experimental procedures often remove the watery content. Also note that the rates are usually defined relative to the time spent foraging, e.g. if an insect forages for six hours on a given day then its gains on that day are considered relative to these six hours instead of the full day.

\subsection{Context}

The values of the above parameters are critical determinants of the temporal dynamics of foraging and reproductive processes in the current model structure as they describe how fast pollinators find and exploit floral resources and thereby how soon they can begin to engage in their reproductive activities.

The nectar production rates of various plant species are for example described by data published by the AgriLand project \autocite{agriland_nectar} \autocite{agriland_flower_density} showing a large variability between $20~\mathrm{mg}~\mathrm{d}^{-1}~\mathrm{m}^{-2}$ and $4000~\mathrm{mg}~\mathrm{d}^{-1}~\mathrm{m}^{-2}$ between different species.

There is also a statistical model available, that predicts pollen production based on flower morphology \autocite{food_for_pollinators} even though no such simple relation seems to exist describing the availability of nectar which seems to be more actively managed by the flowering plants.

Together with land cover and species abundance data which is assumed to be available for the experimental sites, this allows us to approximate the productivity of the flowering plants. Together with the observation that a lot of studies report flowers to be completely emptied of nectar by the evening, this also makes it possible to roughly calibrate the scale of the model.

For example, we currently assume 5\% of the landscape area to be covered by flowering plants and in lieu of site-specific data work with the five most abundant plants species found in a study of British Higher Level Environmental Stewardship farms \autocite{bee_and_flower_abundance}. The above pattern on daily nectar consumption is then reproduced by assuming 30,000 simulated insects present in a randomly generated landscape unit with a diameter of $2 \mathrm{km}$.\footnote{Note that this number is probably too small by a factor of three as simulated insects are currently active for the full 24 hours of the day.}

In our preliminary literature survey, our findings on the detailed foraging behaviour of the various insects are distributed unevenly with most publications focusing on honey bees followed by bumble bees then solitary bees and finally scarce accounts concerning hoverflies. Another issue we face is that methods are often incompatible, e.g. some studies report pollen mass whereas other measure pollen volume which are difficult to relate to each other due to the high variability in pollen shape and hence density.

Some examples of parameter values that we did find are:
\begin{itemize}
\item A study \autocite{commercially_imported_bumble_bees} comparing the nectar foraging rates of native and commercially imported colonies of \emph{Bombus terrestris} finds a mean nectar foraging rate of $175~\mathrm{mg}~\mathrm{h}^{-1}$ for the native colonies.
\item A publication \autocite{imidacloprid_reduce_bumble_bee_pollen_foraging} on the effects of the pesticide Imidacloprid reports a pollen foraging rate of $48~\mathrm{mg}~\mathrm{h}^{-1}$ for the unaffected members of \emph{Bombus terrestris}.
\item An analysis \autocite{quantitative_pollen_requirements_of_solitary_bees} of the brood cells of oligolectic solitary bees yields a regression of pollen requirements
\begin{displaymath}
\frac{V}{\mathrm{mm}^3} = 8.868 \log\left( \frac{m}{\mathrm{mg}} \right) + 0.433
\end{displaymath}
where $V$ is the volume of pollen and $m$ is the dry body mass, e.g. approximately $16~\mathrm{mm}^3$ for a member of \emph{Andrena ruficrus}.
\end{itemize}

\subsection{Procedure}

We aim to apply the procedure of \emph{expert knowledge elicitation} as for example described in the context of ecological modelling in \autocite{probabilistic_uncertainty_specification}. More specifically this questionnaire uses direct elicitation following the quartile elicitation procedure which tries to exploit that human experts seem to be able to accurately judge probabilities to be equal.

We therefore ask for a median $M$, a lower quartile $L$ and an upper quartile $U$ for each parameter. Given a real random variable $X$ with a probability distribution $P$, the median $M$ captures the idea of a "typical value" insofar there are as many smaller values as there are larger ones and it is defined by
\begin{displaymath}
P(X < M) \stackrel{!}{=} P(M < X) \quad\textrm{.}
\end{displaymath}
Similarly, the lower/upper quartiles $L$/$U$ are "typical small/large" values insofar they too have equal weight between the median and below respectively above themselves which is realised by the equations
\begin{displaymath}
P(X < L) \stackrel{!}{=} P(L < X < M) \qquad P(M < X < U) \stackrel{!}{=} P(U < X) \quad\textrm{.}
\end{displaymath}

To help establish a frame of reference for estimating the quartiles, we begin by asking for the "smallest/largest possible" value, i.e. a value $l$/$u$ so that smaller/larger values are considered nearly impossible. These are identified with the 1st and 99th percentiles which are defined by
\begin{displaymath}
P(X < l) \stackrel{!}{=} 1 \% \qquad P(X < u) \stackrel{!}{=} 99 \% \Leftrightarrow P(X > u) \stackrel{!}{=} 1 \% \quad\textrm{.}
\end{displaymath}

\begin{figure}[h]
\begin{center}

\begin{tikzpicture}
\begin{axis}[xmin=0, xmax=3.5, ymin=0, ymax=1, xlabel = $x$, ylabel = {$P(X=x)$}]

\addplot[name path=P, domain=0:3.5, samples=100]{sqrt(2/pi) * exp(-2 * ln(x)^2) / x};

\path[name path=lx] (axis cs:0,0) -- (axis cs:0.31,0);
\path[name path=lLx] (axis cs:0.31,0) -- (axis cs:0.71,0);
\path[name path=LMx] (axis cs:0.71,0) -- (axis cs:1,0);
\path[name path=MUx] (axis cs:1,0) -- (axis cs:1.4,0);
\path[name path=Uux] (axis cs:1.4,0) -- (axis cs:3.2,0);
\path[name path=ux] (axis cs:3.2,0) -- (axis cs:3.5,0);

\addplot[fill=gray, opacity=0.1] fill between [of=P and lx, soft clip={domain=0:0.31}];
\addplot[fill=gray, opacity=0.25] fill between [of=P and lLx, soft clip={domain=0.31:0.71}];
\addplot[fill=gray, opacity=0.4] fill between [of=P and LMx, soft clip={domain=0.71:1}];
\addplot[fill=gray, opacity=0.6] fill between [of=P and MUx, soft clip={domain=1:1.4}];
\addplot[fill=gray, opacity=0.75] fill between [of=P and Uux, soft clip={domain=1.4:3.2}];
\addplot[fill=gray, opacity=0.9] fill between [of=P and ux, soft clip={domain=3.2:3.5}];

\addplot [mark=none] coordinates {(0.31, 0) (0.31, 0.9)};
\node at (axis cs: 0.31, 0.95) {$l$};

\addplot [mark=none] coordinates {(0.71, 0) (0.71, 0.9)};
\node at (axis cs: 0.71, 0.95) {$L$};

\addplot [mark=none] coordinates {(1, 0) (1, 0.9)};
\node at (axis cs: 1, 0.95) {$M$};

\addplot [mark=none] coordinates {(1.4, 0) (1.4, 0.9)};
\node at (axis cs: 1.4, 0.95) {$U$};

\addplot [mark=none] coordinates {(3.2, 0) (3.2, 0.9)};
\node at (axis cs: 3.2, 0.95) {$u$};

\end{axis}
\end{tikzpicture}

\caption{A plot of an log-normal distribution with parameters $\mu = 0$ and $\sigma = \frac{1}{2}$, i.e. $P(X = x) := \sqrt{\frac{2}{\pi}} \frac{e^{-2 \ln^2 x}}{x}$, illustrating the four quartiles: The quantile function is $Q(p) = e^{\sqrt{8} \operatorname{erf}^{-1}\left( 2p - 1 \right)}$. Thereby the median is $M = 1$, the lower quartile is $L \approx 0.71$ and the upper quartile is $U \approx 1.40$. The 1st and 99th percentiles are $l \approx 0.31$ and $u \approx 3.20$.}

\end{center}
\end{figure}

\pagebreak

\subsection{Questionnaire}

Let us first emphasise that we understand the investment of time which we are asking for by sending you this questionnaire. Due to the limitations of the available literature, we are depending on your expert knowledge to produce a reasonable approximation of foraging dynamics, so that only with your support will individual-based modelling to become a useful tool for the VOODOO project.

This first questionnaire considers only the bumblebees. This is done to become acquainted with the procedure and to act as a starting point for more varied questions on the different species and families of insects.

\subsubsection{Specific parameter values}

The first part of the questionnaire considers the parameter values for \emph{an individual of the species Bombus terrestris}. This species was selected as it is expected to be the most common in the target habitats and/or the most widely studied species of the bumble bees. It is assumed to act as a prototype of the related species during model development.

To answer these questions, please think about how much nectar an average individual insect would collect during an average hour of foraging activity at your transect locations. Try to provide your best guess for these questions even if you do not know the answer or have never thought about the question. You can also tick a disclaimer at the end of each block of questions to indicate that we should reduce the weight of your estimates during the analysis.

We will first ask for the smallest possible values, meaning that for example, you would be quite astonished if an individual bumble bee would collect fewer milligrams of nectar or cubic millimetres of pollen during an hour of activity. Then we are interested in what you consider the largest possible value, meaning that you would hardly believe that a single insect could collect such a large amount of nectar and pollen during only one hour.

The next question is about your judgement of typical values, e.g. it should be just as likely for individual to collect more as it is to collect fewer nectar or pollen during one hour of foraging. Please note that it is not necessary that this value is exactly in the middle of the smallest and largest possible values as the distribution could be skewed.

Finally we would ask you to consider the range between the smallest possible and the typical value and try to estimate which value in this interval would have as many individuals foraging at a slower pace as there would be individual foraging at a faster pace but all of them slow in the sense of staying below the typical value. Similarly a typical large values captures the same consideration for the interval between the typical largest possible value.

\begin{Form}
\begin{enumerate}

\item Consider the \emph{rate of nectar collection} of Bombus terrestris expressed in $\mathrm{mg}~\mathrm{h}^{-1}$.

\TextField[name=species_rate_nectar_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=species_rate_nectar_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=species_rate_nectar_median]{What is a \emph{typical value}?} \\
\TextField[name=species_rate_nectar_lower]{What is a \emph{typical small value}?} \\
\TextField[name=species_rate_nectar_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=species_rate_nectar_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the \emph{rate of pollen collection} of Bombus terrestris expressed in $\mathrm{mm^3}~\mathrm{h}^{-1}$.

\TextField[name=species_rate_pollen_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=species_rate_pollen_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=species_rate_pollen_median]{What is a \emph{typical value}?} \\
\TextField[name=species_rate_pollen_lower]{What is a \emph{typical small value}?} \\
\TextField[name=species_rate_pollen_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=species_rate_pollen_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the \emph{amount of pollen collected} per brood cell by Bombus terrestris expressed in $\mathrm{mm^3}$.

\TextField[name=species_amount_pollen_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=species_amount_pollen_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=species_amount_pollen_median]{What is a \emph{typical value}?} \\
\TextField[name=species_amount_pollen_lower]{What is a \emph{typical small value}?} \\
\TextField[name=species_amount_pollen_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=species_amount_pollen_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the \emph{sensing distance} for flowering plants of Bombus terrestris expressed in $\mathrm{m}$.

\TextField[name=species_sensing_distance_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=species_sensing_distance_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=species_sensing_distance_median]{What is a \emph{typical value}?} \\
\TextField[name=species_sensing_distance_lower]{What is a \emph{typical small value}?} \\
\TextField[name=species_sensing_distance_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=species_sensing_distance_confidence]{Is your confidence in these values exceptionally small?}

\end{enumerate}
\end{Form}

\subsubsection{Variability of parameter values}

In the second part of the questionnaire, \emph{the variability of parameter values within a pollinator family} is considered. The aim of separately eliciting this information is to try to distinguish aleatory uncertainty due to biological variability within a family and epistemic uncertainty due to incomplete data as these two kinds of uncertainties should be treated differently during e.g. sensitivity analysis of the model.

Hence instead of a typical individual of some species, we would like you to think of all species of bumble bee you expect to find at your transect locations and then e.g. consider the differences in foraging rate between the slowest and fastest species or the difference in brood cell provisioning between the species with the smallest and the largest pollen packets.

\begin{Form}
\begin{enumerate}

\item Consider the \emph{rate of nectar collection} of all bumble bees expressed in $\mathrm{mg}~\mathrm{h}^{-1}$.

\TextField[name=family_rate_nectar_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=family_rate_nectar_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=family_rate_nectar_median]{What is a \emph{typical value}?} \\
\TextField[name=family_rate_nectar_lower]{What is a \emph{typical small value}?} \\
\TextField[name=family_rate_nectar_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=family_rate_nectar_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the \emph{rate of pollen collection} of all bumble bees expressed in $\mathrm{mm^3}~\mathrm{h}^{-1}$.

\TextField[name=family_rate_pollen_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=family_rate_pollen_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=family_rate_pollen_median]{What is a \emph{typical value}?} \\
\TextField[name=family_rate_pollen_lower]{What is a \emph{typical small value}?} \\
\TextField[name=family_rate_pollen_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=family_rate_pollen_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the \emph{amount of pollen collected} per brood cell by all bumble bees expressed in $\mathrm{mm^3}$.

\TextField[name=family_amount_pollen_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=family_amount_pollen_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=family_amount_pollen_median]{What is a \emph{typical value}?} \\
\TextField[name=family_amount_pollen_lower]{What is a \emph{typical small value}?} \\
\TextField[name=family_amount_pollen_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=family_amount_pollen_confidence]{Is your confidence in these values exceptionally small?}

\item Consider the sensing distance for flowering plants of \emph{bumble bee} expressed in $\mathrm{m}$.

\TextField[name=family_sensing_distance_minimum]{What is the \emph{smallest possible value}?} \\
\TextField[name=family_sensing_distance_maximum]{What is the \emph{largest possible value}?} \\
\TextField[name=family_sensing_distance_median]{What is a \emph{typical value}?} \\
\TextField[name=family_sensing_distance_lower]{What is a \emph{typical small value}?} \\
\TextField[name=family_sensing_distance_upper]{What is a \emph{typical large value}?} \\
\CheckBox[name=family_sensing_distance_confidence]{Is your confidence in these values exceptionally small?}

\end{enumerate}
\end{Form}

\subsubsection{Feedbback}

\begin{Form}

\CheckBox[name=discuss_results]{I would like to participate in an online meeting to discuss the results of the elicitation procedure:}

\TextField[name=comments, multiline=true, width=\linewidth, height=5cm]{I have the following comments on the elicitation procedure: \\}

\end{Form}

\printbibliography

\end{document}
