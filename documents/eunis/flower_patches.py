import math as m
import random as r

count = 30
scale = 2.67
# scale = 4
min_distance_2 = 0.0125 * scale ** 2
size = m.floor(2.5 * scale)

colors = ["purple", "red", "green", "blue"]
weights = [8, 2, 4, 1]

r.seed(1)

positions = []

for _ in range(count):
    collision = True
    while collision:
        position = (r.random() * scale, r.random() * scale)

        for position1 in positions:
            distance_2 = (position[0] - position1[0]) ** 2 + (
                position[1] - position1[1]
            ) ** 2

            if distance_2 < min_distance_2:
                break
        else:
            collision = False

    positions.append(position)

    [color] = r.choices(colors, weights=weights)

    print(
        f"\\node[circle,fill=flower_patch_{color},minimum size={size:.0f},inner sep=0] at ({position[0]:.2f},{position[1]:.2f}) {{}};"
    )
