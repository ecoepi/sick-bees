import collections
import os
import sys

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

script_dir = os.path.realpath(os.path.dirname(__file__))
sys.path.append(f"{script_dir}/../../measurements")
sys.path.append(f"{script_dir}/../../plots")

from contacts import read_contacts
from vectors import read_infections, read_vectors
from prevalence import plot_filtered_prevalence


# Read results for all scenarios ...
prevalence_last_month = collections.defaultdict(list)
foraging_last_two_months = collections.defaultdict(list)
contacts = collections.defaultdict(list)
infections = collections.defaultdict(list)
vectors = collections.defaultdict(list)

for name in os.listdir():
    if not os.path.exists(f"{name}/prevalence.csv"):
        continue

    scenario = name[:-3]

    data = pd.read_csv(f"{name}/prevalence.csv", header=None)
    data = data[-30:]

    prevalence_last_month[scenario].append(data)

    data = pd.read_csv(f"{name}/foraging.csv", header=None)
    data = data[-60:]

    foraging_last_two_months[scenario].append(data)

    data = read_contacts(f"{name}/contacts.log")
    contacts[scenario].append(data)

    data = read_infections(f"{name}/vectors.log")
    infections[scenario].append(data)

    data = read_vectors(f"{name}/vectors.log")
    vectors[scenario].append(data)

# ... and compute their summary statistics
for scenario in prevalence_last_month:
    data = prevalence_last_month[scenario]
    data = pd.concat(data)

    median = 100 * data.quantile(0.5)
    lower = 100 * data.quantile(0.05)
    upper = 100 * data.quantile(0.95)
    std = 100 * data.std()

    prevalence_last_month[scenario] = (median, lower, upper, std)

for scenario in foraging_last_two_months:
    data = foraging_last_two_months[scenario]
    data = pd.concat(data)

    median = 100 * data.quantile(0.5)
    lower = 100 * data.quantile(0.05)
    upper = 100 * data.quantile(0.95)
    std = 100 * data.std()

    foraging_last_two_months[scenario] = (median, lower, upper, std)

print("contacts")
print("--------")
data = np.array(contacts["broad_diet"])
print(f"broad_diet:\n{np.around(100 * np.mean(data, axis=0))}")
data = np.array(contacts["narrow_diet"])
print(f"narrow_diet:\n{np.around(100 * np.mean(data, axis=0))}")
data = np.array(contacts["search_time_limited_narrow_diet"])
print(f"search_time_limited_narrow_diet:\n{np.around(100 * np.mean(data, axis=0))}")

print("infections")
print("----------")
data = infections["broad_diet"]
print(f"broad_diet: {round(np.mean(data))}")
data = infections["narrow_diet"]
print(f"narrow_diet: {round(np.mean(data))}")
data = infections["search_time_limited_broad_diet"]
print(f"search_time_limited_broad_diet: {round(np.mean(data))}")
data = infections["search_time_limited_narrow_diet"]
print(f"search_time_limited_narrow_diet: {round(np.mean(data))}")

print("vectors")
print("-------")
data = np.array(vectors["broad_diet"])
print(f"broad_diet:\n{np.around(100 * np.mean(data, axis=0))}")
data = np.array(vectors["narrow_diet"])
print(f"narrow_diet:\n{np.around(100 * np.mean(data, axis=0))}")
data = np.array(vectors["search_time_limited_broad_diet"])
print(f"search_time_limited_broad_diet:\n{np.around(100 * np.mean(data, axis=0))}")
data = np.array(vectors["search_time_limited_narrow_diet"])
print(f"search_time_limited_narrow_diet:\n{np.around(100 * np.mean(data, axis=0))}")


# Bar plot of foraging activity comparing
# handling time limited and search time limited regimes
fig, axes = plt.subplots()

x = np.arange(2)
width = 0.15

lhs = foraging_last_two_months["broad_diet"]
rhs = foraging_last_two_months["search_time_limited_broad_diet"]

searching = [lhs[0][1], rhs[0][1]]
searching_error = [lhs[3][1], rhs[3][1]]
collecting = [lhs[0][2], rhs[0][2]]
collecting_error = [lhs[3][2], rhs[3][2]]
empty = [lhs[0][3], rhs[0][3]]
empty_error = [lhs[3][3], rhs[3][3]]
nectar = [lhs[0][5], rhs[0][5]]
nectar_error = [lhs[3][5], rhs[3][5]]
clutches = [lhs[0][6], rhs[0][6]]
clutches_error = [lhs[3][6], rhs[3][6]]

axes.bar(x - 2 * width, searching, width, label="Searching", yerr=searching_error)
axes.bar(x - width, collecting, width, label="Collecting", yerr=collecting_error)
axes.bar(x, empty, width, label="Empty", yerr=empty_error)
axes.bar(x + width, nectar, width, label="Nectar", yerr=nectar_error)
axes.bar(x + 2 * width, clutches, width, label="Clutches", yerr=clutches_error)

axes.set_ylabel("Activity / %")
axes.set_ylim(0, 100)

axes.set_xticks(x)
axes.set_xticklabels(["handling time limited", "search time limited"])

fig.legend(loc=(0.35, 0.70))
fig.tight_layout()
fig.savefig("foraging_activity.pdf")


# Bar plots of steady state prevalence comparing scenarios
SCENARIOS = [
    ("broad_diet", "broad diet"),
    ("narrow_diet", "narrow diet"),
    ("narrow_diet_dominant_species", "narrow diet\ndominant species"),
    (
        "narrow_diet_dominant_species_more_flowers",
        "narrow diet\ndominant species\nmore flowers",
    ),
]

fig, axes = plt.subplots()

x = np.arange(len(SCENARIOS))
width = 0.25

data = [prevalence_last_month[scenario] for (scenario, _) in SCENARIOS]
overall = [data[0][1] for data in data]
overall_error = [3 * data[3][1] for data in data]
species_1 = [data[0][2] for data in data]
species_1_error = [3 * data[3][2] for data in data]
species_2 = [data[0][3] for data in data]
species_2_error = [3 * data[3][3] for data in data]

axes.bar(x - width, overall, width, label="Overall", yerr=overall_error)
axes.bar(x, species_1, width, label="Species 1", yerr=species_1_error)
axes.bar(x + width, species_2, width, label="Species 2", yerr=species_2_error)

axes.set_xticks(x)
axes.set_xticklabels([label for (_, label) in SCENARIOS])

axes.set_ylabel("Prevalence / %")
axes.set_ylim(0, 100)

fig.legend(loc=(0.75, 0.75))
fig.tight_layout()
fig.savefig("steady_state_handling_time_limited.pdf")

fig, axes = plt.subplots()

print("prevalence")
print("----------")
print(f"broad diet: {overall[0]:.1f}, {species_1[0]:.1f}, {species_2[0]:.1f}")
print(f"narrow diet: {overall[1]:.1f}, {species_1[1]:.1f}, {species_2[1]:.1f}")


x = np.arange(len(SCENARIOS))
width = 0.25

data = [
    prevalence_last_month[f"search_time_limited_{scenario}"]
    for (scenario, _) in SCENARIOS
]
overall = [data[0][1] for data in data]
overall_error = [data[3][1] for data in data]
species_1 = [data[0][2] for data in data]
species_1_error = [data[3][2] for data in data]
species_2 = [data[0][3] for data in data]
species_2_error = [data[3][3] for data in data]

axes.bar(x - width, overall, width, label="Overall", yerr=overall_error)
axes.bar(x, species_1, width, label="Species 1", yerr=species_1_error)
axes.bar(x + width, species_2, width, label="Species 2", yerr=species_2_error)

axes.set_xticks(x)
axes.set_xticklabels([label for (_, label) in SCENARIOS])

axes.set_ylabel("Prevalence / %")
axes.set_ylim(45, 85)

fig.legend(loc=(0.2, 0.75))
fig.tight_layout()
fig.savefig("steady_state_search_time_limited.pdf")

print(
    f"search_time_limited_broad_diet: {overall[0]:.1f}, {species_1[0]:.1f}, {species_2[0]:.1f}"
)
print(
    f"search_time_limited_narrow_diet: {overall[1]:.1f}, {species_1[1]:.1f}, {species_2[1]:.1f}"
)


# Line plots of time evolution of prevalence
fig, axes = plt.subplots()

for (scenario, label) in SCENARIOS:
    plot_filtered_prevalence(
        [f"{scenario}_{run}/prevalence.csv" for run in range(1, 51)],
        axes,
        label.replace("\n", ", "),
    )

axes.set_xlabel("Time / d")
axes.set_xlim(1, 120)

axes.set_ylabel("Prevalence / %")
axes.set_ylim(0, 80)

fig.legend(loc=(0.35, 0.15))
fig.tight_layout()
fig.savefig("time_evolution_handling_time_limited.pdf")


fig, axes = plt.subplots()

for (scenario, label) in SCENARIOS:
    plot_filtered_prevalence(
        [
            f"search_time_limited_{scenario}_{run}/prevalence.csv"
            for run in range(1, 51)
        ],
        axes,
        label.replace("\n", ", "),
    )

axes.set_xlabel("Time / d")
axes.set_xlim(1, 120)

axes.set_ylabel("Prevalence / %")
axes.set_ylim(55, 75)
axes.set_yticks(np.arange(55, 75 + 1, 5))

fig.legend(loc=(0.35, 0.3))
fig.tight_layout()
fig.savefig("time_evolution_search_time_limited.pdf")

# Read prevalence and observations to
# create box plots of abundance and prevalence estimates
# comparing increasing sampling efforts
def add_boxplot(axes, plots, labels, ylabel):
    axes.axhline(0)
    axes.tick_params(axis="x", which="major", labelsize=8)
    axes.boxplot(plots, labels=labels, showmeans=True, whis=(5, 95))

    axes.set_ylabel(ylabel)


def save_boxplot(plots, labels, ylabel, file_name):
    fig, axes = plt.subplots()

    add_boxplot(axes, plots, labels, ylabel)

    fig.tight_layout()
    fig.savefig(file_name)


SCENARIOS = [("broad_diet", "broad diet"), ("narrow_diet", "narrow diet")]
EFFORTS = [("", "2h"), ("_more_sampling", "20h"), ("_even_more_sampling", "200h")]

print("observations")
print("------------")

abundance_fig, abundance_axes = plt.subplots(2, 1, figsize=[6.4, 2 * 4.8], sharex=True)
contacts_fig, contacts_axes = plt.subplots(2, 1, figsize=[6.4, 2 * 4.8], sharex=True)

for (abundance_axes, contacts_axes, (scenario, title)) in zip(
    abundance_axes, contacts_axes, SCENARIOS
):
    labels = []
    abundance_plots = []
    contacts_plots = []
    prevalence_labels = ([], [], [])
    prevalence_plots = ([], [], [])

    for (effort, label) in EFFORTS:
        abundance_species_1 = []
        abundance_species_2 = []
        contacts_species_1 = []
        contacts_species_2 = []
        prevalence_overall = []
        prevalence_species_1 = []
        prevalence_species_2 = []

        for run in range(1, 51):
            dir_name = f"sampling_effects/{scenario}{effort}_{run}"

            data = pd.read_csv(f"{dir_name}/prevalence.csv", header=None)
            prevalence = data[-30:].mean()

            data = pd.read_csv(f"{dir_name}/observations.csv", header=None)
            observations = data.iloc[-1]

            data = read_contacts(f"{dir_name}/contacts.log")
            contacts = np.sum(data, axis=0)

            species_1 = observations[2]
            species_1_infected = observations[3]
            species_2 = observations[5]
            species_2_infected = observations[6]

            abundance_species_1.append(species_1 / (species_1 + species_2) - 0.5)
            abundance_species_2.append(species_2 / (species_1 + species_2) - 0.5)

            contacts_species_1.append(species_1 / (species_1 + species_2) - contacts[0])
            contacts_species_2.append(species_2 / (species_1 + species_2) - contacts[1])

            prevalence_overall.append(
                (species_1_infected + species_2_infected) / (species_1 + species_2)
                - prevalence[1]
            )

            if species_1 != 0:
                prevalence_species_1.append(
                    species_1_infected / species_1 - prevalence[2]
                )

            if species_2 != 0:
                prevalence_species_2.append(
                    species_2_infected / species_2 - prevalence[3]
                )

        labels.append("species 1\n" + label)
        labels.append("species 2\n" + label)

        abundance_plots.append(np.array(abundance_species_1))
        abundance_plots.append(np.array(abundance_species_2))

        contacts_plots.append(np.array(contacts_species_1))
        contacts_plots.append(np.array(contacts_species_2))

        print(
            f"{scenario}{effort}: {round(100 * np.mean(abundance_species_1))} {round(100 * np.mean(abundance_species_2))} {round(100 * np.mean(contacts_species_1))} {round(100 * np.mean(contacts_species_1))}"
        )

        prevalence_labels[0].append("overall\n" + label)
        prevalence_plots[0].append(100 * np.array(prevalence_overall))

        prevalence_labels[1].append("species 1\n" + label)
        prevalence_plots[1].append(100 * np.array(prevalence_species_1))

        prevalence_labels[2].append("species 2\n" + label)
        prevalence_plots[2].append(100 * np.array(prevalence_species_2))

    add_boxplot(abundance_axes, abundance_plots, labels, "Relative abundance / 1")

    abundance_axes.set_ylim(-0.75, 0.75)
    abundance_axes.set_title(title)

    add_boxplot(contacts_axes, contacts_plots, labels, "Share of contacts / 1")

    contacts_axes.set_ylim(-0.9, 0.9)
    contacts_axes.set_title(title)

    save_boxplot(
        sum(prevalence_plots, []),
        sum(prevalence_labels, []),
        "Prevalence / %",
        f"sampling_effort_{scenario}_prevalence.pdf",
    )

abundance_fig.tight_layout()
abundance_fig.savefig("sampling_effort_abundance.pdf")

contacts_fig.tight_layout()
contacts_fig.savefig("sampling_effort_contacts.pdf")


# Read observations to create box plots of abundance estimates
# comparing increasing handling times
SCENARIOS = [("broad_diet", "broad diet"), ("narrow_diet", "narrow diet")]
TIMES = [
    ("_no_handling_at_all", "0min"),
    ("", "1min"),
    ("_more_handling", "10min"),
    ("_even_more_handling", "100min"),
]

fig, axes = plt.subplots(2, 1, figsize=[6.4, 2 * 4.8], sharex=True)

for (axes, (scenario, title)) in zip(axes, SCENARIOS):
    abundance_labels = []
    abundance_plots = []

    for (time, label) in TIMES:
        abundance_species_1 = []
        abundance_species_2 = []

        for run in range(1, 51):
            dir_name = f"sampling_effects/{scenario}{time}_{run}"

            data = pd.read_csv(f"{dir_name}/observations.csv", header=None)
            observations = data.iloc[-1]

            species_1 = observations[2]
            species_2 = observations[5]

            abundance_species_1.append(species_1 / (species_1 + species_2) - 0.5)
            abundance_species_2.append(species_2 / (species_1 + species_2) - 0.5)

        abundance_labels.append("species 1\n" + label)
        abundance_plots.append(np.array(abundance_species_1))
        abundance_labels.append("species 2\n" + label)
        abundance_plots.append(np.array(abundance_species_2))

        print(
            f"{scenario}{time}: {round(100 * np.mean(abundance_species_1))} {round(100 * np.mean(abundance_species_2))}"
        )

    add_boxplot(axes, abundance_plots, abundance_labels, "Relative abundance / 1")

    axes.set_ylim(-0.75, 0.75)
    axes.set_title(title)

fig.tight_layout()
fig.savefig("handling_time_abundance.pdf")


# Read observations to create a box plot
# comparing decreasing refill intervals
SCENARIO = "narrow_diet_even_more_handling"
INTERVALS = [
    ("", "24h"),
    ("_refill_12h", "12h"),
    ("_refill_2h", "2h"),
    ("_refill_15min", "15min"),
]

abundance_labels = []
abundance_plots = []

for (interval, label) in INTERVALS:
    abundance_species_1 = []
    abundance_species_2 = []

    for run in range(1, 51):
        dir_name = f"sampling_effects/{SCENARIO}{interval}_{run}"

        data = pd.read_csv(f"{dir_name}/observations.csv", header=None)
        observations = data.iloc[-1]

        species_1 = observations[2]
        species_2 = observations[5]

        abundance_species_1.append(species_1 / (species_1 + species_2) - 0.5)
        abundance_species_2.append(species_2 / (species_1 + species_2) - 0.5)

    abundance_labels.append("species 1\n" + label)
    abundance_plots.append(np.array(abundance_species_1))
    abundance_labels.append("species 2\n" + label)
    abundance_plots.append(np.array(abundance_species_2))

    print(
        f"{SCENARIO}{interval}: {round(100 * np.mean(abundance_species_1))} {round(100 * np.mean(abundance_species_2))}"
    )

save_boxplot(
    abundance_plots,
    abundance_labels,
    "Relative abundance / 1",
    f"handling_time_{SCENARIO}_refill_interval.pdf",
)


# Read direct and indirect activity measurements
# to create a line plot showing convergence
SCENARIOS = [
    ("broad_diet", "broad diet"),
    ("narrow_diet_dominant_species", "narrow diet, dominant species"),
    ("search_time_limited_broad_diet", "search-time-limited, broad diet"),
    (
        "search_time_limited_narrow_diet_dominant_species",
        "search-time-limited, narrow diet, dominant species",
    ),
]

fig, axes = plt.subplots()

for (scenario, label) in SCENARIOS:
    data = pd.read_csv(f"activities_{scenario}.csv", header=None)

    axes.plot(data[0], 100 * abs(data[1] - data[2]), label=f"searching, {label}")
    axes.plot(data[0], 100 * abs(data[3] - data[4]), label=f"collecting, {label}")

axes.set_xlabel("Time / h")
axes.set_ylabel("Error / %")

axes.legend()

fig.tight_layout()
fig.savefig(f"activities.pdf")
