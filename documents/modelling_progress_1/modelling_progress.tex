\documentclass[10pt]{beamer}

\usepackage{adjustbox}
\usepackage{scalefnt}

\usepackage{tikz}
\usetikzlibrary{overlay-beamer-styles}

\usepackage{pgfplots}
\pgfplotsset{
    compat=1.16,
    /pgfplots/every plot/.append style={no marks},
    /pgfplots/y filter/.expression={100 * y},
    /pgfplots/xmin=0, /pgfplots/xmax=4320,
    /pgfplots/xlabel={$\textrm{Time} / \mathrm{h}$},
    /pgfplots/ylabel={$\textrm{Prevalence} / \%$},
    /pgfplots/legend pos={north west},
    /pgfplots/legend cell align=left,
    /pgfplots/table/col sep=comma
}
\usepgfplotslibrary{fillbetween}

\mode<presentation>
{
\usetheme{Pittsburgh}
}

\AtBeginSection[]
{
\begin{frame}{Outline}

\tableofcontents[currentsection]

\end{frame}
}

% \setbeameroption{show notes}

\title{VOODOO modelling progress}
\author{EcoEpi}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}{Outline}

\tableofcontents

\end{frame}

\section{Diet breadth and pathogen prevalence}

\begin{frame}{Approach}

\begin{itemize}

\item Validate model design by reimplementing SIS model described in Figueroa et al. 2020, Ecology Letters 23 (8) as a sub model.

\item Aim to reproduce the effect of diet breadth on pathogen prevalence both when contact rate is independent of as well as proportional to network degree.

\item Qualitatively different dynamics emerge due to quantitatively different availability of floral resources, i.e. switch from handling to search time limited scenarios.

\end{itemize}

\end{frame}

\note[itemize]
{
    \item 9,000 insects individually foraging in 112,500 flower patches within landscape of one square kilometer with periodic boundary conditions instead of system of non-linear ordinary differential equations.

    \item Parameter values were reused from published R code even though some could be removed as they become emergent properties in the model.

    \item Independent contact rate corresponds to handling time limited scenario whereas proportional contact rate corresponds to search time limited scenario.
}

\begin{frame}{Handling time limited scenarios}

\centering

{\scalefont{0.4}

\begin{tikzpicture}
\begin{axis}[ymin=0, ymax=80, width=0.9\textwidth]

\addplot[blue] table{broad_diet/prevalence_1.csv};
\addplot[blue] table{broad_diet/prevalence_2.csv};
\addplot[blue] table{broad_diet/prevalence_3.csv};

\addplot[red] table{narrow_diet/prevalence_1.csv};
\addplot[red] table{narrow_diet/prevalence_2.csv};
\addplot[red] table{narrow_diet/prevalence_3.csv};

\addplot[yellow] table{narrow_diet_dominant_species/prevalence_1.csv};
\addplot[yellow] table{narrow_diet_dominant_species/prevalence_2.csv};
\addplot[yellow] table{narrow_diet_dominant_species/prevalence_3.csv};

\addplot[orange] table{narrow_diet_dominant_species_more_flowers/prevalence_1.csv};
\addplot[orange] table{narrow_diet_dominant_species_more_flowers/prevalence_2.csv};
\addplot[orange] table{narrow_diet_dominant_species_more_flowers/prevalence_3.csv};

\addplot[brown] table{narrow_diet_dominant_species_more_species/prevalence_1.csv};
\addplot[brown] table{narrow_diet_dominant_species_more_species/prevalence_2.csv};
\addplot[brown] table{narrow_diet_dominant_species_more_species/prevalence_3.csv};

\legend{
    broad diet,,,
    narrow diet,,,
    {narrow diet, dominant species},,,
    {narrow diet, dominant species, more flowers},,,
    {narrow diet, dominant species, more species},,,
}

\end{axis}
\end{tikzpicture}

}

\end{frame}

\begin{frame}{Handling time limited scenarios}

\centering

{\scalefont{0.4}

\begin{tikzpicture}
\begin{axis}[ymin=0, ymax=80, width=0.9\textwidth]

\addplot[blue] table{broad_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=broad_diet_min] table[y index=2]{broad_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=broad_diet_max] table[y index=3]{broad_diet/prevalence_filtered.csv};
\addplot[blue, opacity=0.1] fill between[of=broad_diet_min and broad_diet_max];

\addplot[red] table{narrow_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_min] table[y index=2]{narrow_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_max] table[y index=3]{narrow_diet/prevalence_filtered.csv};
\addplot[red, opacity=0.1] fill between[of=narrow_diet_min and narrow_diet_max];

\addplot[yellow] table{narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_min] table[y index=2]{narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_max] table[y index=3]{narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[yellow, opacity=0.1] fill between[of=narrow_diet_dominant_species_min and narrow_diet_dominant_species_max];

\addplot[orange] table{narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_flowers_min] table[y index=2]{narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_flowers_max] table[y index=3]{narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[orange, opacity=0.1] fill between[of=narrow_diet_dominant_species_more_flowers_min and narrow_diet_dominant_species_more_flowers_max];

\addplot[brown] table{narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_species_min] table[y index=2]{narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_species_max] table[y index=3]{narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[brown, opacity=0.1] fill between[of=narrow_diet_dominant_species_more_species_min and narrow_diet_dominant_species_more_species_max];

\legend{
    broad diet,,,,
    narrow diet,,,,
    {narrow diet, dominant species},,,,
    {narrow diet, dominant species, more flowers},,,,
    {narrow diet, dominant species, more species},,,,
}

\end{axis}
\end{tikzpicture}

}

\end{frame}

\begin{frame}{Search time limited scenarios}

\centering

{\scalefont{0.4}

\begin{tikzpicture}
\begin{axis}[ymin=45, ymax=65, width=0.9\textwidth]

\addplot[blue] table{search_time_limited_broad_diet/prevalence_1.csv};
\addplot[blue] table{search_time_limited_broad_diet/prevalence_2.csv};
\addplot[blue] table{search_time_limited_broad_diet/prevalence_3.csv};

\addplot[red] table{search_time_limited_narrow_diet/prevalence_1.csv};
\addplot[red] table{search_time_limited_narrow_diet/prevalence_2.csv};
\addplot[red] table{search_time_limited_narrow_diet/prevalence_3.csv};

\addplot[yellow] table{search_time_limited_narrow_diet_dominant_species/prevalence_1.csv};
\addplot[yellow] table{search_time_limited_narrow_diet_dominant_species/prevalence_2.csv};
\addplot[yellow] table{search_time_limited_narrow_diet_dominant_species/prevalence_3.csv};

\addplot[orange] table{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_1.csv};
\addplot[orange] table{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_2.csv};
\addplot[orange] table{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_3.csv};

\addplot[brown] table{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_1.csv};
\addplot[brown] table{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_2.csv};
\addplot[brown] table{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_3.csv};

\legend{
    broad diet,,,
    narrow diet,,,
    {narrow diet, dominant species},,,
    {narrow diet, dominant species, more flowers},,,
    {narrow diet, dominant species, more species},,,
}

\end{axis}
\end{tikzpicture}

}

\end{frame}

\begin{frame}{Search time limited scenarios}

\centering

{\scalefont{0.4}

\begin{tikzpicture}
\begin{axis}[ymin=45, ymax=65, width=0.9\textwidth]

\addplot[blue] table{search_time_limited_broad_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=broad_diet_min] table[y index=2]{search_time_limited_broad_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=broad_diet_max] table[y index=3]{search_time_limited_broad_diet/prevalence_filtered.csv};
\addplot[blue, opacity=0.1] fill between[of=broad_diet_min and broad_diet_max];

\addplot[red] table{search_time_limited_narrow_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_min] table[y index=2]{search_time_limited_narrow_diet/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_max] table[y index=3]{search_time_limited_narrow_diet/prevalence_filtered.csv};
\addplot[red, opacity=0.1] fill between[of=narrow_diet_min and narrow_diet_max];

\addplot[yellow] table{search_time_limited_narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_min] table[y index=2]{search_time_limited_narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_max] table[y index=3]{search_time_limited_narrow_diet_dominant_species/prevalence_filtered.csv};
\addplot[yellow, opacity=0.1] fill between[of=narrow_diet_dominant_species_min and narrow_diet_dominant_species_max];

\addplot[orange] table{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_flowers_min] table[y index=2]{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_flowers_max] table[y index=3]{search_time_limited_narrow_diet_dominant_species_more_flowers/prevalence_filtered.csv};
\addplot[orange, opacity=0.1] fill between[of=narrow_diet_dominant_species_more_flowers_min and narrow_diet_dominant_species_more_flowers_max];

\addplot[brown] table{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_species_min] table[y index=2]{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[opacity=0, name path=narrow_diet_dominant_species_more_species_max] table[y index=3]{search_time_limited_narrow_diet_dominant_species_more_species/prevalence_filtered.csv};
\addplot[brown, opacity=0.1] fill between[of=narrow_diet_dominant_species_more_species_min and narrow_diet_dominant_species_more_species_max];

\legend{
    broad diet,,,,
    narrow diet,,,,
    {narrow diet, dominant species},,,,
    {narrow diet, dominant species, more flowers},,,,
    {narrow diet, dominant species, more species},,,,
}

\end{axis}
\end{tikzpicture}

}

\end{frame}

\begin{frame}{Spillover from dominant species}

\begin{itemize}

\item Both insect species are generalist foragers:

\adjustbox{vspace=0.5cm}{

\begin{tabular}{cc}

\small{infections at flower 1} & \small{infections at flower 2} \\

\begin{tabular}{ccc}
& \tiny{contaminated by 1} & \tiny{contaminated by 2} \\
\tiny{1 is infected} & 28.5\% & 8.8\% \\
\tiny{2 is infected} & 8.9\% & 3.8\%
\end{tabular}

&

\begin{tabular}{ccc}
& \tiny{contaminated by 1} & \tiny{contaminated by 2} \\
\tiny{1 is infected} & 28.8\% & 8.9\% \\
\tiny{2 is infected} & 8.6\% & 3.8\%
\end{tabular}

\end{tabular}

}

\item Dominant species specialises on first flower species:

\adjustbox{vspace=0.5cm}{

\begin{tabular}{cc}

\small{infections at flower 1} & \small{infections at flower 2} \\

\begin{tabular}{ccc}
& \tiny{contaminated by 1} & \tiny{contaminated by 2} \\
\tiny{1 is infected} & 84.8\% & 2.4\% \\
\tiny{2 is infected} & 9.3\% & 0.5\%
\end{tabular}

&

\begin{tabular}{ccc}
& \tiny{contaminated by 1} & \tiny{contaminated by 2} \\
\tiny{1 is infected} & 0\% & 0\% \\
\tiny{2 is infected} & 0\% & 3.0\%
\end{tabular}

\end{tabular}

}

\end{itemize}

\end{frame}

\note[itemize]
{
    \item Individual-based model enables detailed observations by instrumenting rule-based dynamics.

    \item First insect species is three times as abundant as the second one, but causes almost 20 times more infections of the second one at the shared flower species.
}

\section{Expert knowledge elicitation}

\begin{frame}{Initial motivation}

\begin{itemize}

\item Model dynamics are determined by a relatively small set of parameter values on the individual foraging activities of the considered insect species.

\item It is difficult to source these parameters for a community of insect species due to gaps and inconsistencies in the available literature. One approach to productively handle such uncertainties is systematic estimation of the values by human experts experienced in the subject matter.

\item This is preferable to modellers with limited field experience trying to guess parameters so they yield sensible model patterns as the consistency of the model design can be checked instead of being assumed even if a large spread of the numerical values is still present.

\end{itemize}

\end{frame}

\begin{frame}{Next steps}

\begin{itemize}

\item One approach to reduce the burden of responding to multiple questionnaires would be an interactive session which includes discussing estimates among the participants.

\item This can reduce the uncertainty of the final estimates, but it must to be performed in a systematic manner to avoid introducing bias via group dynamics.

\end{itemize}

\end{frame}

\end{document}
