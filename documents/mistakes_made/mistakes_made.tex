\documentclass{beamer}

\usepackage{hyperref}

\usepackage{tikz}
\usetikzlibrary{overlay-beamer-styles}

\mode<presentation>
{
\usetheme{Pittsburgh}
}

\AtBeginSection[]
{
\begin{frame}{Outline}

\tableofcontents[currentsection]

\end{frame}
}

\setbeameroption{show notes}

\title{Mistakes made modelling vision and choice}
\author{EcoEpi}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}{Outline}

\tableofcontents

\end{frame}

\section{Introduction}

\begin{frame}{Mistakes}
\begin{itemize}

\item<2-> mistakes are a powerful way to learn

\item<3-> we focus on certain kinds of errors

\begin{itemize}
\item<4-> above \emph{programming errors}, i.e. problems with the model, not its implementation as a computer program

\item<5-> below \emph{validation failures}, i.e. internal inconsistencies, not inability to reproduce established patterns
\end{itemize}

\end{itemize}
\end{frame}

\note[itemize]
{
\item each kind of error has an associated test strategy
\begin{itemize}
\item programming errors and unit tests

\item validation failures and pattern-oriented modelling
\end{itemize}

\item an implementation which correctly captures the explicit model structure can induce unintended implicit model structure

\item internal inconsistencies can be found by changing something that should not have an effect but observing one nevertheless
}

\begin{frame}{Purpose}
\begin{itemize}

\item<2-> indirect pathogen transmission between pollinators

\item<3-> mediated by contaminated flowers of shared plants

\item<4-> \href{https://voodoo-project.eu}{VOODOO project} defines experimental protocol

\item<5-> investigate contacts between plants, pollinators and pathogens

\item<6-> look for patterns in results of simulated transect walks

\end{itemize}
\end{frame}

\begin{frame}{Design}
\begin{itemize}

\item<2-> spatially explicit individual-based model with high spatial and temporal resolutions
\begin{itemize}
\item<3-> insects randomly explore landscape
\item<4-> sense nearby flower patches
\item<5-> decide which to exploit based on specific preferences
\end{itemize}

\item<6-> pathogen transmission driven by colocation of multiple insect families and plant species

\item<7-> implementation available at \url{https://git.ufz.de/ecoepi/sick-bees}

\end{itemize}
\end{frame}

\note[itemize]
{
\item spatial resolution is flexible, i.e. no grid, and currently equivalent to 3m resolution

\item done to support detailed landscape structure including linear elements like field margins

\item temporal resolution is flexible, i.e. processes scale with time step, and currently at 1s

\item done to enable a real-time visualisation as a means of increasing engagement

\item this resolution has a significant computational cost, i.e. tens of CPU hours per simulation run

\item therefore parallel execution based on landscape subdivision is important for exploration
}

\begin{frame}{Transfer}
\begin{itemize}

\item<2-> issues presented here were encountered modelling insects visiting flowering plants

\item<3-> they should transfer to other individual-based models that include \emph{vision and choice}

\item<4-> mobile entities that sense stationary resources of different kinds and decide which to exploit
\begin{itemize}
\item<5-> fish hunting in shallow waters
\item<5-> birds of prey hunting small mammals
\item<5-> scientists collecting insects along transect walks
\end{itemize}

\end{itemize}
\end{frame}

\section{Time-step-invariant sensing}

\tikzstyle{insect} = [circle,fill=yellow]
\tikzstyle{plant} = [circle,fill=green]

\begin{frame}{Sensing}
\begin{center}
\begin{tikzpicture}[scale=0.7]

\node[insect,label=$t$] (curr) at (0,0) {};
\node[insect,label=$t+\Delta t$] (next) at (5,3) {};
\path (curr) edge[->] node {} (next);

\node[plant] at (1.4,-3.5) {};
\node[plant] at (-1.2,2.5) {};
\node[plant] at (5.5,-0.5) {};
\node[plant] at (6.7,5.5) {};
\node[plant] at (6.5,2.7) {};
\node[plant] at (-1.2,2.7) {};
\node[plant] at (7.1,6.1) {};
\node[plant] at (1.8,-2.3) {};
\node[plant] at (0.5,6.9) {};
\node[plant] at (-0.7,-3.0) {};
\node[plant] at (-1.9,2.8) {};
\node[plant] at (3.8,-0.9) {};
\node[plant] at (-3.3,0.7) {};
\node[plant] at (2.4,1.8) {};

\draw[visible on=<2->] (0,0) circle (4);
\draw[dashed, visible on=<3->] (5,3) circle (4);

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item first implementation queried all flower patches within a given distance of the current position of a pollinator

\item chosen to minimise number of assumptions due to lack of information on perception of various pollinator families

\item shows a brittle interaction with the relation between sensing distance and time step, i.e. visibility gaps

\item also showed scheduling artefacts when using flower patch clusters, i.e. look-then-move versus move-then-look
}

\begin{frame}{Gaps}
\begin{center}
\begin{tikzpicture}[scale=0.9]

\node[insect,label=$t$] (curr) at (0,0) {};
\node[insect,label=$t+\Delta t$] (next) at (5,3) {};
\path (curr) edge[->] node {} (next);

\node[plant] at (5.9,2.5) {};
\node[plant] at (1.1,0.4) {};
\node[plant] at (5.2,1.5) {};
\node[plant] at (4.1,1.9) {};
\node[plant] at (-0.4,0.5) {};
\node[plant] at (6.5,4.3) {};
\node[plant] at (-0.3,0.2) {};
\node[plant] at (1.5,0.8) {};
\node[plant] at (3.3,2.3) {};
\node[plant] at (1.1,1.2) {};
\node[plant] at (-1.2,3.8) {};

\node[plant,visible on=<1-3>] at (3.9,0.1) {};
\node[plant,visible on=<1-3>] at (1.9,2.2) {};
\node[plant,visible on=<1-3>] at (3.6,0.2) {};

\node[plant,gray,visible on=<4->] at (3.9,0.1) {};
\node[plant,gray,visible on=<4->] at (1.9,2.2) {};
\node[plant,gray,visible on=<4->] at (3.6,0.2) {};

\draw[visible on=<2->] (0,0) circle (2);
\draw[dashed,visible on=<3->] (5,3) circle (2);

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item if movement distance per time step is significantly larger than the sensing distance, some flower patches become invisible

\item highly relevant for us as flying insects can move fast, e.g. 3 meters per second above ground

\item also goes against the incentive to make the time step as large as possible due to computational costs
}

\begin{frame}{Scheduling}
\begin{center}
\begin{tikzpicture}[scale=0.9]

\node[insect,label=$t$] (curr) at (0,0) {};
\node[insect,label=$t+\Delta t$] (next) at (5,3) {};
\path (curr) edge[->] node {} (next);

\node[plant] at (-1.2,1.1) {};
\node[plant] at (0.5,0.2) {};
\node[plant] at (1.7,0.3) {};
\node[plant] at (-0.7,1.4) {};
\node[plant] at (-0.7,-0.7) {};
\node[plant] at (0.8,-1.1) {};
\node[plant] at (0.2,-1.6) {};

\node[plant] at (7.9,0.7) {};
\node[plant] at (7.7,1.6) {};
\node[plant] at (6.8,-0.1) {};
\node[plant] at (7.3,1.0) {};
\node[plant] at (6.5,1.1) {};
\node[plant] at (7.2,2.0) {};
\node[plant] at (7.3,0.3) {};

\draw[visible on=<2>] (0,0) circle (2);
\draw[visible on=<3>] (5,3) circle (2);

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item our initial implementation also used clusters of flower patches to ensure choice

\item if there is a flower patch of some species, there are more flower patches of other species nearby

\item with look-then-move, an insect that found a cluster at all, would always detect all flower patches within it

\item with move-then-look, a cluster would always be left after visiting one flower patch within it

\item this yielded significant differences in contact rates and hence prevalence values

\item we dropped clustering later on to simplify the model as it eventually became unnecessary to realise specific preferences
}

\begin{frame}{Tubes}
\begin{center}
\begin{tikzpicture}[scale=0.7]

\node[insect,label=$t$] (curr) at (0,0) {};
\node[insect,label=$t+\Delta t$] (next) at (5,3) {};
\path (curr) edge[->] node {} (next);

\node[plant] at (1.4,-3.5) {};
\node[plant] at (-1.2,2.5) {};
\node[plant] at (5.5,-0.5) {};
\node[plant] at (6.7,5.5) {};
\node[plant] at (6.5,2.7) {};
\node[plant] at (-1.2,2.7) {};
\node[plant] at (7.1,6.1) {};
\node[plant] at (1.8,-2.3) {};
\node[plant] at (0.5,6.9) {};
\node[plant] at (-0.7,-3.0) {};
\node[plant] at (-1.9,2.8) {};
\node[plant] at (3.8,-0.9) {};
\node[plant] at (-3.3,0.7) {};
\node[plant] at (2.4,1.8) {};

\draw[visible on=<2->] (-2.06,3.43) arc (120.96:300.96:4) -- (7.06,-0.43) arc (-59.04:120.96:4) -- (-2.06,3.43);
\draw[dashed,visible on=<3->] (-4,-4) rectangle (9,7);

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item our solution to both issues was to modify the spatial queries to consider a tube from the current to the next position

\item contrary to our expectations, this implied only a minimal loss of performance as considering the AABB limits the number of distance-between-point-and-line-segment computations

\item we implemented spatial queries using so-called \href{https://en.wikipedia.org/wiki/K-d_tree}{K-D trees}, which can store points only but effectively prune candidates using the AABB of the query
}

\section{Ensuring choice in weighted choice}

\begin{frame}{Weights}
\begin{center}
\begin{tikzpicture}

\node[insect] at (0,0) {};
\draw (0,0) circle (3);

\node[plant,visible on=<-3>] at (1.7,1.5) {};
\node[plant,visible on=<-3>] at (-1.3,2.3) {};
\node[plant,visible on=<-3>] at (0.7,2.0) {};
\node[plant,visible on=<-3>] at (2.2,1.0) {};
\node[plant,label={\only<4-5>{$1$}},visible on=<4-5>] at (1.7,1.5) {};
\node[plant,label={\only<4-5>{$1$}},visible on=<4-5>] at (-1.3,2.3) {};
\node[plant,label={\only<4-5>{$0$}},visible on=<4-5>] at (0.7,2.0) {};
\node[plant,label={\only<4-5>{$0$}},visible on=<4-5>] at (2.2,1.0) {};
\node[plant,label={\only<6->{$3$}},visible on=<6->] at (1.7,1.5) {};
\node[plant,label={\only<6->{$1$}},visible on=<6->] at (-1.3,2.3) {};
\node[plant,label={\only<6->{$3$}},visible on=<6->] at (0.7,2.0) {};
\node[plant,label={\only<6->{$1$}},visible on=<6->] at (2.2,1.0) {};

\draw[thick,visible on=<5->] (1.7,1.5) circle (0.25);

\node[plant,visible on=<-2>] at (-1.2,-2.4) {};
\node[plant,darkgray,visible on=<3->] at (-1.2,-2.4) {};

\node[plant] at (-3.7,3.8) {};
\node[plant] at (3.1,-0.0) {};
\node[plant] at (4.3,0.8) {};
\node[plant] at (-4.5,-1.8) {};
\node[plant] at (3.9,-3.3) {};
\node[plant] at (1.4,2.7) {};
\node[plant] at (-3.2,-0.1) {};
\node[plant] at (-0.4,-3.3) {};
\node[plant] at (0.4,-3.2) {};
\node[plant,gray,visible on=<2->] at (-3.7,3.8) {};
\node[plant,gray,visible on=<2->] at (3.1,-0.0) {};
\node[plant,gray,visible on=<2->] at (4.3,0.8) {};
\node[plant,gray,visible on=<2->] at (-4.5,-1.8) {};
\node[plant,gray,visible on=<2->] at (3.9,-3.3) {};
\node[plant,gray,visible on=<2->] at (1.4,2.7) {};
\node[plant,gray,visible on=<2->] at (-3.2,-0.1) {};
\node[plant,gray,visible on=<2->] at (-0.4,-3.3) {};
\node[plant,gray,visible on=<2->] at (0.4,-3.2) {};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item after determining which flower patches are visible, we set out to model choosing among those resources based on specific preferences

\item our first implementation of preference used weighted choice, i.e.
\begin{itemize}
\item enumerate all flower patches which are currently visible

\item ignore those remembered as already visited

\item assign weights to each patch based on insect and plant species

\item make a random choice based on those weights
\end{itemize}

\item this works if the weights are essentially binary, but there is a problem if the weights are more nuanced
}

\begin{frame}{alternativlos}
\begin{center}
\begin{tikzpicture}

\node[insect] at (0,0) {};
\draw (0,0) circle (3);

\node[plant,visible on=<-3>] at (1.7,1.5) {};
\node[plant,label={\only<4>{$0$}},visible on=<4>] at (1.7,1.5) {};
\node[plant,label={\only<5>{$3$}},visible on=<5>] at (1.7,1.5) {};
\node[plant,label={\only<6->{$1$}},visible on=<6->] at (1.7,1.5) {};

\draw[thick,visible on=<5->] (1.7,1.5) circle (0.25);

\node[plant,visible on=<-2>] at (-1.2,-2.4) {};
\node[plant,darkgray,visible on=<3->] at (-1.2,-2.4) {};

\node[plant] at (-3.7,3.8) {};
\node[plant] at (4.3,0.8) {};
\node[plant] at (-4.5,-1.8) {};
\node[plant] at (3.9,-3.3) {};
\node[plant,gray,visible on=<2->] at (-3.7,3.8) {};
\node[plant,gray,visible on=<2->] at (4.3,0.8) {};
\node[plant,gray,visible on=<2->] at (-4.5,-1.8) {};
\node[plant,gray,visible on=<2->] at (3.9,-3.3) {};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item depending on the time step, the sensing distance and the flower patch density, there can be few visible flower patches

\item patches with a zero weight are still ignored

\item but patches with a non-zero weight are chosen independently of the magnitude of the weight

\item therefore weights are effectively reduced to binary values

\item this is especially troubling as it implies a non-continuous dependency on the parameters, e.g.
\begin{itemize}
\item the results of using 100:0 or 100:1 are very different

\item but the results of 100:1 and 100:100 are essentially the same
\end{itemize}

\item the problem is that weighted choice relies on having a sufficiently large sample which faithfully represents the composition of plant species at each time step
}

\begin{frame}{Acceptance}
\begin{center}
\begin{tikzpicture}

\node[insect] at (0,0) {};
\draw (0,0) circle (3);

\node[plant,visible on=<-1>] at (1.7,1.5) {};
\node[plant,label={\only<2>{$0.25$}},visible on=<2>] at (1.7,1.5) {};
\node[plant,darkgray,visible on=<3->] at (1.7,1.5) {};

\node[plant,visible on=<-3>] at (-1.3,2.3) {};
\node[plant,label={\only<4>{$0$}},visible on=<4>] at (-1.3,2.3) {};
\node[plant,darkgray,visible on=<5->] at (-1.3,2.3) {};

\node[plant,visible on=<-5>] at (0.7,2.0) {};
\node[plant,label={\only<6>{$0.75$}},visible on=<6>] at (0.7,2.0) {};
\node[plant,visible on=<7->] at (0.7,2.0) {};

\node[plant] at (2.2,1.0) {};

\draw[dashed,visible on=<2>] (1.7,1.5) circle (0.25);
\draw[dashed,visible on=<4>] (-1.3,2.3) circle (0.25);
\draw[dashed,visible on=<6>] (0.7,2.0) circle (0.25);
\draw[thick,visible on=<7->] (0.7,2.0) circle (0.25);

\node[plant,darkgray] at (-1.2,-2.4) {};

\node[plant,gray] at (-3.7,3.8) {};
\node[plant,gray] at (3.1,-0.0) {};
\node[plant,gray] at (4.3,0.8) {};
\node[plant,gray] at (-4.5,-1.8) {};
\node[plant,gray] at (3.9,-3.3) {};
\node[plant,gray] at (1.4,2.7) {};
\node[plant,gray] at (-3.2,-0.1) {};
\node[plant,gray] at (-0.4,-3.3) {};
\node[plant,gray] at (0.4,-3.2) {};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item our solution to this issue was to replace weighted choice by acceptance probabilities, i.e.
\begin{itemize}
\item enumerate all flower patches which are currently visible and not remembered as already visited

\item assign an acceptance probability based on insect and plant species

\item sample acceptance probabilities in arbitrary order

\item if accepted, visit a flower patch and if not, ignore it and memorise that
\end{itemize}

\item yields unchanged results with binary weights, but shows a continuous dependency on the probabilities
}

\begin{frame}{Ignorance}
\begin{center}
\begin{tikzpicture}

\node[insect] at (0,0) {};
\draw (0,0) circle (3);

\node[plant,visible on=<-1>] at (1.7,1.5) {};
\node[plant,label={\only<2>{$0$}},visible on=<2>] at (1.7,1.5) {};
\node[plant,label={\only<3>{$0.75$}},visible on=<3>] at (1.7,1.5) {};
\node[plant,label={\only<4->{$0.25$}},visible on=<4->] at (1.7,1.5) {};

\draw[dashed,visible on=<2->] (1.7,1.5) circle (0.25);

\node[plant,darkgray] at (-1.2,-2.4) {};

\node[plant,gray] at (-3.7,3.8) {};
\node[plant,gray] at (4.3,0.8) {};
\node[plant,gray] at (-4.5,-1.8) {};
\node[plant,gray] at (3.9,-3.3) {};

\end{tikzpicture}
\end{center}
\end{frame}

\note[itemize]
{
\item as a possible downside, this implies that even if there is only a single visible flower patch, it is sometimes ignored based on its acceptance probability

\item both approaches are modulated by the specific abundances of the plants, i.e. a high preference for a rare plant species will still yield a small number of visitations

\item an alternative would have been to accumulate visible flower patches until a certain count is reached before making the weighted choice, but we decided against this to avoid introducing a new time scale into the model

\item the chosen approach also aligns with trait matching models considered in the literature as those models usually quantify trait matching in a way that can also be interpreted as a probability
}

\section{Randomising order of acceptance}

\tikzstyle{insect} = [circle,fill=yellow,minimum size=0.75cm]
\tikzstyle{plant} = [circle,fill=green,minimum size=0.5cm]

\begin{frame}{Trees}
\begin{center}
\begin{tiny}
\begin{tikzpicture}

\clip (-5.5,-3) rectangle (6,3.5);

\draw[visible on=<2->] (-0.7,3) -- (-0.7,-3);
\draw[visible on=<3->] (-5.5,0) -- (-0.7,0);
\draw[visible on=<4->] (-2.3,0) -- (-2.3,-3);

\draw[visible on=<5->] (-0.7,0.7) -- (6,0.7);
\draw[visible on=<5->] (0.3,0.7) -- (0.3,3);
\draw[visible on=<5->] (1.4,0.7) -- (1.4,-3);

\node[plant] at (-3.6,0.8) {\only<6->{$0$}};
\node[plant] at (-4.9,0) {\only<7->{$1$}};
\node[plant] at (-2.3,-2.3) {\only<8->{$2$}};
\node[plant] at (-1.4,-2.7) {\only<9->{$3$}};
\node[plant] at (-0.7,1.1) {\only<9->{$4$}};
\node[plant] at (0.3,2.8) {\only<9->{$5$}};
\node[plant] at (4.8,2.3) {\only<9->{$6$}};
\node[plant] at (3.7,0.7) {\only<9->{$7$}};
\node[plant] at (1.4,-1.9) {\only<9->{$8$}};
\node[plant] at (5.7,-0.2) {\only<9->{$9$}};

\end{tikzpicture}
\end{tiny}
\end{center}
\end{frame}

\note[itemize]
{
\item spatial queries are backed by K-D trees which map the spatial relations to parent-child relations in a tree

\item their construction cycles through the axes and selects the median element based on comparing the coordinate along the current axis

\item the algorithm recurses using the elements before and after the median and terminates when the number of elements is zero or one

\item each step represents a binary partition of space along a hyperplane defined by the corresponding axis and the median element

\item this enables resolving spatial queries using a logarithmic number of AABB checks, but it also determines the order in which the matching elements are yielded
}

\begin{frame}{Correlations}
\begin{center}
\begin{tiny}
\begin{tikzpicture}

\clip (-5.5,-3) rectangle (6,3.5);

\node[plant] at (-3.6,0.8) {$0$};
\node[plant] at (-4.9,0) {$1$};
\node[plant] at (-2.3,-2.3) {$2$};
\node[plant] at (-1.4,-2.7) {$3$};
\node[plant] at (-0.7,1.1) {$4$};
\node[plant] at (0.3,2.8) {$5$};
\node[plant] at (4.8,2.3) {$6$};
\node[plant] at (3.7,0.7) {$7$};
\node[plant] at (1.4,-1.9) {$8$};
\node[plant] at (5.7,-0.2) {$9$};

\node[insect,visible on=<2->] at (-1.7,1.9) {};
\draw[visible on=<2->] (-1.7,1.9) circle (2.75);
\draw[thick,visible on=<3>] (-3.6,0.8) circle (0.3);

\node[insect,visible on=<4->] at (-2.9,-1.2) {};
\draw[visible on=<4->] (-2.9,-1.2) circle (2.75);
\draw[thick,visible on=<5->] (-3.6,0.8) circle (0.3);
\fill[white] (-5.5,-3) rectangle (-4.5,-2);

\end{tikzpicture}
\end{tiny}
\end{center}
\end{frame}

\note[itemize]
{
\item this implied that insects would sense and hence to a certain degree also visit flower patches in a fixed global order

\item thereby, artificial correlations were introduced into the order in which flower patches were visited by the insects

\item this is especially problematic when acceptance probabilities are high and sensing distances are large as this increases the strength and range of these correlations

\item hence, pollinators visited flower patches almost in lockstep which raised prevalences significantly
}

\begin{frame}{Permutations}
\begin{center}
\begin{tiny}
\begin{tikzpicture}

\clip (-5.5,-3) rectangle (6,3.5);

\node[plant] at (-3.6,0.8) {\only<-2>{$0$}\only<3-4>{$'1$}\only<5>{$0$}\only<6->{$'2$}};
\node[plant] at (-4.9,0) {\only<-5>{$1$}\only<6->{$'3$}};
\node[plant] at (-2.3,-2.3) {\only<-5>{$2$}\only<6->{$'0$}};
\node[plant] at (-1.4,-2.7) {\only<-5>{$3$}\only<6->{$'2$}};
\node[plant] at (-0.7,1.1) {\only<-2>{$4$}\only<3-4>{$'0$}\only<5->{$4$}};
\node[plant] at (0.3,2.8) {\only<-2>{$5$}\only<3-4>{$'2$}\only<5->{$5$}};
\node[plant] at (4.8,2.3) {$6$};
\node[plant] at (3.7,0.7) {$7$};
\node[plant] at (1.4,-1.9) {$8$};
\node[plant] at (5.7,-0.2) {$9$};

\node[insect,visible on=<2->] at (-1.7,1.9) {};
\draw[visible on=<2->] (-1.7,1.9) circle (2.75);
\draw[thick,visible on=<4>] (-0.7,1.1) circle (0.3);

\node[insect,visible on=<5->] at (-2.9,-1.2) {};
\draw[visible on=<5->] (-2.9,-1.2) circle (2.75);
\draw[thick,visible on=<7->] (-2.3,-2.3) circle (0.3);
\fill[white] (-5.5,-3) rectangle (-4.5,-2);

\end{tikzpicture}
\end{tiny}
\end{center}
\end{frame}

\note[itemize]
{
\item visible flower patches are collected into a temporary list

\item acceptance probability is evaluated on a random permutation

\item this breaks the correlations encoded in the K-D tree

\item but it implied a significant overhead of an approximately $10 \%$ longer runtime for managing and shuffling the list

\item it also introduced additional implementation complexity to keep the list around to avoid further overhead due to dynamic allocations
}

\section{Conclusion}

\begin{frame}{Awareness}
\begin{itemize}

\item<2-> between programming errors and validation failures exists a distinct class of \emph{conceptual mistakes}

\item<3-> they introduce artefacts and affect the internal consistency

\item<4-> often become visible only when exercising the model in multiple scenarios or parameterisations

\end{itemize}
\end{frame}

\note[itemize]
{
\item an implementation which correctly captures the explicit model structure can induce unintended implicit model structure

\item internal inconsistencies can be found by changing something that should not have an effect but observing one nevertheless
}

\end{document}
