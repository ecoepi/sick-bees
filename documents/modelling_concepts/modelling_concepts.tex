\documentclass[10pt]{beamer}

\usepackage{graphicx}

\theoremstyle{plain}
\newtheorem*{objective}{Objective}

\usepackage{tikz}
\usetikzlibrary{overlay-beamer-styles}

\mode<presentation>
{
\usetheme{Pittsburgh}
}

\AtBeginSection[]
{
\begin{frame}{Outline}

\tableofcontents[currentsection]

\end{frame}
}

%\setbeameroption{show notes}

\title{VOODOO modelling concepts}
\author{EcoEpi}

\begin{document}

\begin{frame}

\titlepage

\end{frame}

\begin{frame}{Outline}

\tableofcontents

\end{frame}

\section{Overview}

\subsection{Setup}

\begin{frame}{Setup}

\begin{tikzpicture}[every node/.style={anchor=south west,inner sep=0}]

\node (land-use) at (0,0) {\includegraphics[width=\textwidth]{setup_land_use}};
\node[visible on=<1-2>] (landscape-units) at (0,0) {\includegraphics[width=\textwidth]{setup_landscape_units}};
\node[visible on=<2>] (insets) at (0,0) {\includegraphics[width=\textwidth]{setup_insets}};
\node[visible on=<3>] (simulations) at (0,0) {\includegraphics[width=\textwidth]{setup_simulations}};
\node[visible on=<2->] (transects) at (0,0) {\includegraphics[width=\textwidth]{setup_transects}};

\end{tikzpicture}

\end{frame}

\note[itemize]
{
    \item Reproduce statistical observations through emergent properties by simulating individual behavior.

    \item Detailed landscape description necessary as agents interact mechanistically with their environment.
}

\subsection{Purpose}

\begin{frame}{Purpose}

\begin{itemize}

\item Model virus transmission within a community of flying pollinator species that use nectar and pollen from a variety of flowering plants.

\item Experimental protocol suggests spatial dimensions of $2km$ and time scale of one season at a resolution of $1s$.

\end{itemize}

\begin{objective}<2->

Investigate how assumptions on foraging behavior determine interaction networks and thereby virus transmission dynamics.

\end{objective}

\begin{objective}<3->

Simulate transect-based collection and compare virtual and real measurements w.r.t. sampling of interaction networks.

\end{objective}

\end{frame}

\section{Design}

\subsection{Entities}

\begin{frame}{Questions}

\begin{itemize}

\item Which species of plants and insects do we expect and can we distinguish?

\item<2-> What is the structure of the landscape units considering:

\begin{itemize}

\item<2-> the distribution of plant species

\item<2-> the abundance of insect species

\item<2-> possible sites for insect nests, colonies, etc.

\end{itemize}

\end{itemize}

\end{frame}

\note[itemize]
{
    \item We need access to expert knowledge and suggestions for literature.

    \item We need GIS data on the choosen landscape units.
}

\begin{frame}{Entities}

\begin{tikzpicture}
\tikzstyle{flower_patch} = [circle,fill=green,minimum size=1cm]
\tikzstyle{hoverfly} = [circle,fill=yellow,minimum size=0.25cm]
\tikzstyle{solitary_bee} = [circle,fill=red,minimum size=0.25cm]
\tikzstyle{bumble_bee} = [circle,fill=black,minimum size=0.25cm]

\node[flower_patch] at (0, 0) (flower-patch-1) {};
\node[flower_patch] at (0.8, 0.2) (flower-patch-2) {};
\node[flower_patch] at (0.6, 0.8) (flower-patch-3) {};
\node[flower_patch] at (0.115, 1.2) (flower-patch-4) {};
\node[flower_patch] at (-0.6, 0.65) (flower-patch-5) {};
\node at (0, 2) (flower-patches) {Flower patches};

\node[visible on=<2->,hoverfly] at (6, 4) (hoverfly-1) {};
\node[visible on=<2->,hoverfly] at (6.2, 4.2) (hoverfly-2) {};
\node[visible on=<2->,hoverfly] at (6.3, 4.1) (hoverfly-3) {};
\node[visible on=<2->] at (6, 5) (hoverflies) {Hoverflies};

\node[visible on=<3->,solitary_bee] at (3, 5) (solitary-bee-1) {};
\node[visible on=<3->] at (3, 4) (solitary-bees) {Solitary bees};

\node[visible on=<4->,bumble_bee,minimum size=0.75cm] at (9, 0) (bumble-bee-colony) {};
\node[visible on=<4->,bumble_bee] at (8.4, 0.4) (bumble-bee-1) {};
\node[visible on=<4->,bumble_bee] at (8.75, 0.6) (bumble-bee-2) {};
\node[visible on=<4->,bumble_bee] at (8.45, 0.7) (bumble-bee-3) {};
\node[visible on=<4->,bumble_bee] at (8.65, 0.8) (bumble-bee-3) {};
\node[visible on=<4->] at (8, 2) (bumble-bees) {Bumble bees};
\end{tikzpicture}

\end{frame}

\note[itemize]
{
    \item Flower patches produce nectar and pollen

    \item Flower patches are occur in clusters

    \item Hoverflies are not bound to any location

    \item Solitary bees build nests and chambers

    \item Bumble bees spawn at and return to colonies
}

\subsection{Processes}

\begin{frame}{Questions}

\begin{itemize}

\item Which properties determine how these insects forage, rest and reproduce?

\item<2-> How do these insects perceive and explore their surroundings?

\item<3-> What rules can be formulated on how individuals move through the landscape and use its resources?

\item<4-> What affinities exist between the different insect and plant species?

\end{itemize}

\end{frame}

\note[itemize]
{
    \item Do affinities result from coevolution of plant and insect traits? Which traits are known respectively relevant?

    \item Modeling preferences based on insect visitation data needs abundance data on plant abundance to infer independent parameter values.
}

\begin{frame}{Processes}

\begin{tikzpicture}[->]
\tikzstyle{flower_patch} = [circle,fill=green,minimum size=1cm]
\tikzstyle{hoverfly} = [circle,fill=yellow,minimum size=0.25cm]
\tikzstyle{clutch} = [circle,fill=magenta,minimum size=0.25cm]

\node[flower_patch] at (0, 0) (flower-patch-1) {};
\node[flower_patch] at (0.8, 0.2) (flower-patch-2) {};
\node[flower_patch] at (0.6, 0.8) (flower-patch-3) {};
\node[flower_patch] at (0.115, 1.2) (flower-patch-4) {};
\node[flower_patch] at (-0.6, 0.65) (flower-patch-5) {};
\node at (0, 2) (flower-patches) {Flower patches};

\node[hoverfly] at (6, 4) (hoverfly-1) {};
\node[hoverfly] at (6.2, 4.2) (hoverfly-2) {};
\node[hoverfly] at (6.3, 4.1) (hoverfly-3) {};
\node at (6, 5) (hoverflies) {Hoverflies};

\node[clutch] at (7, 2) (clutch-1) {};
\node[clutch] at (7.1, 1.8) (clutch-2) {};
\node at (7, 2.5) (clutches) {Clutches};

\path
    (hoverfly-1) edge[visible on=<2->] [bend right] node[above,sloped] {explore landscape} (flower-patch-3)
    (flower-patch-3) edge[visible on=<2->] [loop right] node[align=left] {collect nectar\\and pollen} (flower-patch-3)
    (flower-patch-2) edge[thick,visible on=<3->] [bend right] node[below,sloped] {find clutch} (clutch-1)
    (clutch-2) edge[thick,visible on=<4->] [loop right] node {lay eggs} (clutch-2);
\end{tikzpicture}

\end{frame}

\note[itemize]
{
    \item Hoverflies forage for nectar and pollen

    \item When sufficient pollen are collected, they look for a place to establish a clutch

    \item When such a place is found, they lay eggs and return to foraging
}

\begin{frame}{Processes}

\begin{tikzpicture}[->]
\tikzstyle{flower_patch} = [circle,fill=green,minimum size=1cm]
\tikzstyle{solitary_bee} = [circle,fill=red,minimum size=0.25cm]
\tikzstyle{nest} = [circle,fill=blue,minimum size=0.5cm]

\node[flower_patch] at (0, 0) (flower-patch-1) {};
\node[flower_patch] at (0.8, 0.2) (flower-patch-2) {};
\node[flower_patch] at (0.6, 0.8) (flower-patch-3) {};
\node[flower_patch] at (0.115, 1.2) (flower-patch-4) {};
\node[flower_patch] at (-0.6, 0.65) (flower-patch-5) {};
\node at (0, 2) (flower-patches) {Flower patches};

\node[solitary_bee] at (3, 5) (solitary-bee-1) {};
\node at (3, 4) (solitary-bees) {Solitary bees};

\node[nest] at (6, 2.5) (nest-1) {};
\node[nest] at (6.5, 2.7) (nest-2) {};
\node at (6, 3.5) (nests) {Nests};

\path
    (solitary-bee-1) edge [bend right] node[above,sloped] {explore landscape} (flower-patch-3)
    (flower-patch-3) edge [loop right] node[align=left] {collect nectar\\and pollen} (flower-patch-3)
    (flower-patch-2) edge[thick] [bend right] node[below,sloped] {find nest} (nest-1)
    (flower-patch-3) edge[thick,visible on=<2->] [bend left] node[above,sloped] {return to nest} (nest-1)
    (nest-2) edge[thick] [loop right] node {build chamber} (nest-2);
\end{tikzpicture}

\end{frame}

\note[itemize]
{
    \item Solitary bees forage for nectar and pollen

    \item When sufficient pollen are collected, they return to their nest and build a new chamber

    \item If they have no nest, they search for a place to establish one

    \item Similar dispersal to hoverflies, but solitary bees return to the same nest multiple times.
}

\begin{frame}{Processes}

\begin{tikzpicture}[->]
\tikzstyle{flower_patch} = [circle,fill=green,minimum size=1cm]
\tikzstyle{hoverfly} = [circle,fill=yellow,minimum size=0.25cm]
\tikzstyle{solitary_bee} = [circle,fill=red,minimum size=0.25cm]
\tikzstyle{bumble_bee} = [circle,fill=black,minimum size=0.25cm]

\node[flower_patch] at (0, 0) (flower-patch-1) {};
\node[flower_patch] at (0.8, 0.2) (flower-patch-2) {};
\node[flower_patch] at (0.6, 0.8) (flower-patch-3) {};
\node[flower_patch] at (0.115, 1.2) (flower-patch-4) {};
\node[flower_patch] at (-0.6, 0.65) (flower-patch-5) {};
\node at (0, 2) (flower-patches) {Flower patches};

\node[bumble_bee,minimum size=0.75cm] at (6, 0) (bumble-bee-colony) {};
\node[bumble_bee] at (5.4, 0.4) (bumble-bee-1) {};
\node[bumble_bee] at (5.75, 0.6) (bumble-bee-2) {};
\node[bumble_bee] at (5.45, 0.7) (bumble-bee-3) {};
\node[bumble_bee] at (5.65, 0.8) (bumble-bee-3) {};
\node at (6, 2) (bumble-bees) {Bumble bees};

\path
    (bumble-bee-1) edge[visible on=<3->] [bend right] node[above,sloped] {explore landscape} (flower-patch-3)
    (flower-patch-3) edge[visible on=<3->] [loop right] node[align=left] {collect nectar\\and pollen} (flower-patch-3)
    (bumble-bee-colony) edge[thick,visible on=<2->] [bend right=2cm] node[right,align=left] {spawn\\worker} (bumble-bee-3)
    (flower-patch-2) edge[thick,visible on=<4->] [bend right] node[below,sloped] {return to colony} (bumble-bee-colony);
\end{tikzpicture}

\end{frame}

\note[itemize]
{
    \item Bumble bees spawn at the colony to forage for nectar and pollen

    \item When sufficient pollen are collected, they return to the colonoy

    \item Bumble bees are eusocial and hence considered agents of their colonies.

    \item Similar disperal to hoverflies and solitary bees, but bound to a single colony.
}

\section{Discussion}

\begin{frame}{Discussion}

\begin{itemize}

\item Is our approach suitable to reflect the sampling described by the experimental protocol?

\item<2-> Which entities and processes are essential to capture virus transmisson dynamics?

\item<3-> Which more detailed descriptions of insect and plant behavior should be investigated?

\item<4-> Which patterns can be observer and analysed in a qualitative manner?

\end{itemize}

\end{frame}

\end{document}
