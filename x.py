#!/usr/bin/env python3

import subprocess

if __name__ == "__main__":
    subprocess.run(["cargo", "documents", "extract"], check=True)

    subprocess.run(["cargo", "fmt", "--all"], check=True)

    subprocess.run(["cargo", "documents", "merge"], check=True)

    subprocess.run(["cargo", "clippy", "--workspace", "--all-targets"], check=True)
