FROM debian:stable

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install --yes --no-install-recommends \
        curl pkg-config \
        libopenmpi-dev llvm clang mingw-w64 \
        texlive texlive-luatex texlive-latex-extra texlive-fonts-extra texlive-bibtex-extra biber texlive-lang-german fonts-noto-color-emoji \
        python3-matplotlib python3-pandas && \
    luaotfload-tool --update

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

RUN curl --output rustup-init --location https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init && \
    chmod +x rustup-init && \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain nightly-2022-03-07 --component clippy rustfmt --target x86_64-pc-windows-gnu && \
    rm rustup-init && \
    cargo install cargo-sweep
