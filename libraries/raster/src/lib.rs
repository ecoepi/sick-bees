use std::convert::TryInto;
use std::fs::File;
use std::path::Path;

use memmap2::Mmap;

pub struct Raster {
    width: usize,
    height: usize,
    buf: Mmap,
}

impl Raster {
    #[allow(clippy::missing_safety_doc)]
    pub unsafe fn open<P: AsRef<Path>>(path: P) -> Self {
        let buf = Mmap::map(&File::open(path).unwrap()).unwrap();

        let width = usize::from_ne_bytes(buf[..8].try_into().unwrap());
        let height = usize::from_ne_bytes(buf[8..16].try_into().unwrap());

        Self { width, height, buf }
    }

    pub fn get(&self, x: f64, y: f64) -> f32 {
        let col = (x * self.width as f64) as usize;
        let row = (y * self.height as f64) as usize;

        self.cell(col, row)
    }

    pub fn columns(&self) -> Box<[f64]> {
        (0..self.width)
            .scan(0., |sum, col| {
                *sum += (0..self.height)
                    .map(|row| self.cell(col, row) as f64)
                    .sum::<f64>();

                Some(*sum)
            })
            .collect()
    }

    pub fn rows<C>(&self, columns: C) -> Box<[f64]>
    where
        C: Iterator<Item = usize> + Clone,
    {
        (0..self.height)
            .scan(0., |sum, row| {
                *sum += columns
                    .clone()
                    .map(|col| self.cell(col, row) as f64)
                    .sum::<f64>();

                Some(*sum)
            })
            .collect()
    }

    fn cell(&self, col: usize, row: usize) -> f32 {
        let idx = col + self.width * row;

        let start = 16 + 4 * idx;
        let end = start + 4;

        f32::from_ne_bytes(self.buf[start..end].try_into().unwrap())
    }
}
