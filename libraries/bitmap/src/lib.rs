#[derive(Clone, Copy, Default)]
pub struct Bitmap {
    start: usize,
    end: usize,
}

impl Bitmap {
    pub fn new(words: &mut Vec<u64>, bits: usize) -> Self {
        let start = words.len();
        words.resize(words.len() + words_len(bits), 0);
        let end = words.len();

        Self { start, end }
    }

    pub fn get(&self, words: &[u64], bit: usize) -> bool {
        let word = &words[self.start..][words_idx(bit)];
        let mask = mask(bit);

        *word & mask != 0
    }

    pub fn set(&mut self, words: &mut [u64], bit: usize, val: bool) {
        let word = &mut words[self.start..][words_idx(bit)];
        let mask = mask(bit);

        if val {
            *word |= mask;
        } else {
            *word &= !mask;
        }
    }

    pub fn test_and_set(&mut self, words: &mut [u64], bit: usize, val: bool) -> bool {
        let word = &mut words[self.start..][words_idx(bit)];
        let mask = mask(bit);

        match (*word & mask != 0, val) {
            (false, true) => {
                *word |= mask;
                true
            }
            (true, false) => {
                *word &= !mask;
                true
            }
            _ => false,
        }
    }

    pub fn count(&self, words: &[u64]) -> u32 {
        words[self.start..self.end]
            .iter()
            .map(|word| word.count_ones())
            .sum()
    }

    pub fn reset(&mut self, words: &mut [u64]) {
        words[self.start..self.end].fill(0);
    }
}

fn words_len(bits: usize) -> usize {
    (bits + 63) / 64
}

fn words_idx(bit: usize) -> usize {
    bit / 64
}

fn mask(bit: usize) -> u64 {
    1 << (bit % 64)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn set_and_get_work() {
        let mut words = Vec::new();

        let mut bitmap = Bitmap::new(&mut words, 4);
        assert_eq!(words.len(), 1);

        bitmap.set(&mut words, 0, true);
        bitmap.set(&mut words, 2, true);

        assert!(bitmap.get(&words, 0));
        assert!(!bitmap.get(&words, 1));
        assert!(bitmap.get(&words, 2));
        assert!(!bitmap.get(&words, 3));

        bitmap.set(&mut words, 0, false);
        bitmap.set(&mut words, 1, true);
        bitmap.set(&mut words, 2, false);
        bitmap.set(&mut words, 3, true);

        assert!(!bitmap.get(&words, 0));
        assert!(bitmap.get(&words, 1));
        assert!(!bitmap.get(&words, 2));
        assert!(bitmap.get(&words, 3));
    }

    #[test]
    fn set_and_count_work() {
        let mut words = Vec::new();

        let mut bitmap = Bitmap::new(&mut words, 135);
        assert_eq!(words.len(), 3);

        for bit in 0..135 {
            if bit % 2 == 0 {
                bitmap.set(&mut words, bit, true);
            }
        }

        assert_eq!(bitmap.count(&words), 68);
    }

    #[test]
    fn test_and_set_works() {
        let mut words = Vec::new();

        let mut bitmap = Bitmap::new(&mut words, 4);
        assert_eq!(words.len(), 1);

        assert!(bitmap.test_and_set(&mut words, 0, true));
        assert!(bitmap.get(&words, 0));
        assert!(!bitmap.test_and_set(&mut words, 0, true));
        assert!(bitmap.test_and_set(&mut words, 0, false));
        assert!(!bitmap.get(&words, 0));
    }

    #[test]
    fn reset_works() {
        let mut words = Vec::new();

        let mut bitmap = Bitmap::new(&mut words, 135);
        assert_eq!(words.len(), 3);

        for bit in 0..135 {
            if bit % 2 == 0 {
                bitmap.set(&mut words, bit, true);
            }
        }

        assert_eq!(bitmap.count(&words), 68);

        bitmap.reset(&mut words);
        assert_eq!(bitmap.count(&words), 0);
    }

    #[test]
    fn supports_multiple_instances() {
        let mut words = Vec::new();

        let mut bitmap1 = Bitmap::new(&mut words, 4);
        let bitmap2 = Bitmap::new(&mut words, 4);
        let bitmap3 = Bitmap::new(&mut words, 4);
        assert_eq!(words.len(), 3);

        bitmap1.set(&mut words, 0, true);
        assert!(!bitmap2.get(&words, 0));
        assert_eq!(bitmap3.count(&words), 0);
    }

    #[test]
    fn words_len_rounds_upwards() {
        assert_eq!(words_len(0), 0);
        assert_eq!(words_len(1), 1);
        assert_eq!(words_len(2), 1);
        assert_eq!(words_len(63), 1);
        assert_eq!(words_len(64), 1);
        assert_eq!(words_len(65), 2);
    }

    #[test]
    fn words_idx_rounds_downwards() {
        assert_eq!(words_idx(0), 0);
        assert_eq!(words_idx(1), 0);
        assert_eq!(words_idx(2), 0);
        assert_eq!(words_idx(63), 0);
        assert_eq!(words_idx(64), 1);
        assert_eq!(words_idx(65), 1);
    }

    #[test]
    fn mask_sets_single_bit() {
        assert_eq!(mask(0), 0b1);
        assert_eq!(mask(1), 0b10);
        assert_eq!(mask(2), 0b100);
        assert_eq!(mask(63), 0x8000000000000000);
        assert_eq!(mask(64), 0b01);
        assert_eq!(mask(65), 0b10);
    }
}
