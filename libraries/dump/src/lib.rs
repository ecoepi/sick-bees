use std::io::IoSlice;
use std::mem::{align_of, size_of};
use std::ptr::{addr_of_mut, write};
use std::slice::{from_raw_parts, from_raw_parts_mut};

use model::{
    bumblebee::{Bumblebee, Colony},
    flower_patch::{FlowerPatch, Location as FlowerPatchLocation, Species as FlowerPatchSpecies},
    hoverfly::{Clutch, ClutchLocation, Hoverfly},
    insect::Species as InsectSpecies,
    observer::Observation,
    observer::Observer,
    solitary_bee::{Nest, NestLocation, SolitaryBee},
    units::Tick,
    TemporalNetwork, World,
};

pub fn save<'a, T>(val: &'a T, buf: &mut Vec<IoSlice<'a>>)
where
    T: Dump,
{
    assert!(buf.is_empty());

    let pos = &mut 0;

    save_one(val, buf, pos);
    val.save(buf, pos);
}

#[allow(clippy::missing_safety_doc)]
pub unsafe fn restore<T>(buf: &mut [u8]) -> &T
where
    T: Dump,
{
    let (ptr, rest) = restore_one::<T>(buf);
    let rest = Dump::restore(rest, ptr);

    assert!(rest.is_empty());

    &*ptr
}

unsafe impl Dump for World {
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        self.flower_patches.save(buf, pos);
        self.flowers.save(buf, pos);
        self.hoverflies.save(buf, pos);
        self.clutches.save(buf, pos);
        self.solitary_bees.save(buf, pos);
        self.nests.save(buf, pos);
        self.bumblebees.save(buf, pos);
        self.colonies.save(buf, pos);
        self.observers.save(buf, pos);

        self.observations.save(buf, pos);
        self.contacts.save(buf, pos);
        self.vectors.save(buf, pos);
        self.temporal_network.save(buf, pos);
    }

    unsafe fn restore(mut buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        buf = Dump::restore(buf, addr_of_mut!((*this).flower_patches));
        buf = Dump::restore(buf, addr_of_mut!((*this).flowers));
        buf = Dump::restore(buf, addr_of_mut!((*this).hoverflies));
        buf = Dump::restore(buf, addr_of_mut!((*this).clutches));
        buf = Dump::restore(buf, addr_of_mut!((*this).solitary_bees));
        buf = Dump::restore(buf, addr_of_mut!((*this).nests));
        buf = Dump::restore(buf, addr_of_mut!((*this).bumblebees));
        buf = Dump::restore(buf, addr_of_mut!((*this).colonies));
        buf = Dump::restore(buf, addr_of_mut!((*this).observers));

        buf = Dump::restore(buf, addr_of_mut!((*this).observations));
        buf = Dump::restore(buf, addr_of_mut!((*this).contacts));
        buf = Dump::restore(buf, addr_of_mut!((*this).vectors));
        Dump::restore(buf, addr_of_mut!((*this).temporal_network))
    }
}

unsafe impl Dump for TemporalNetwork {
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        self.edges.save(buf, pos);
        self.node_additions.save(buf, pos);
        self.node_removals.save(buf, pos);
    }

    unsafe fn restore(mut buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        buf = Dump::restore(buf, addr_of_mut!((*this).edges));
        buf = Dump::restore(buf, addr_of_mut!((*this).node_additions));
        Dump::restore(buf, addr_of_mut!((*this).node_removals))
    }
}

unsafe impl Dump for FlowerPatch {}

unsafe impl Dump for FlowerPatchLocation {}

unsafe impl Dump for Hoverfly {}

unsafe impl Dump for Clutch {}

unsafe impl Dump for ClutchLocation {}

unsafe impl Dump for SolitaryBee {}

unsafe impl Dump for Nest {}

unsafe impl Dump for NestLocation {}

unsafe impl Dump for Bumblebee {}

unsafe impl Dump for Colony {}

unsafe impl Dump for Observer {}

unsafe impl Dump for Observation {}

unsafe impl Dump for FlowerPatchSpecies {}

unsafe impl Dump for InsectSpecies {}

unsafe impl Dump for usize {}

unsafe impl Dump for u32 {}

unsafe impl Dump for u64 {}

unsafe impl Dump for Tick {}

#[allow(clippy::missing_safety_doc)]
pub unsafe trait Dump: Sized {
    fn save<'a>(&'a self, _buf: &mut Vec<IoSlice<'a>>, _pos: &mut usize) {}

    #[allow(clippy::missing_safety_doc)]
    unsafe fn restore(buf: &mut [u8], _ptr: *mut Self) -> &mut [u8] {
        buf
    }
}

unsafe impl<'a, T> Dump for &'a [T]
where
    T: Dump,
{
    fn save<'b>(&'b self, buf: &mut Vec<IoSlice<'b>>, pos: &mut usize) {
        save_many(self, buf, pos);
    }

    unsafe fn restore(buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        let len = (*this).len();
        let (ptr, rest) = restore_many::<T>(len, buf);

        let vals = from_raw_parts(ptr, len);

        write(this, vals);
        rest
    }
}

unsafe impl<S, T> Dump for (S, T)
where
    S: Dump,
    T: Dump,
{
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        self.0.save(buf, pos);
        self.1.save(buf, pos);
    }

    unsafe fn restore(mut buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        buf = Dump::restore(buf, addr_of_mut!((*this).0));
        Dump::restore(buf, addr_of_mut!((*this).1))
    }
}

unsafe impl<R, S, T> Dump for (R, S, T)
where
    R: Dump,
    S: Dump,
    T: Dump,
{
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        self.0.save(buf, pos);
        self.1.save(buf, pos);
        self.2.save(buf, pos);
    }

    unsafe fn restore(mut buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        buf = Dump::restore(buf, addr_of_mut!((*this).0));
        buf = Dump::restore(buf, addr_of_mut!((*this).1));
        Dump::restore(buf, addr_of_mut!((*this).2))
    }
}

unsafe impl<T> Dump for Box<[T]>
where
    T: Dump,
{
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        save_many(self, buf, pos);
    }

    unsafe fn restore(buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        let len = (*this).len();
        let (ptr, rest) = restore_many::<T>(len, buf);

        let vals = Box::from_raw(from_raw_parts_mut(ptr, len));

        write(this, vals);
        rest
    }
}

unsafe impl<T> Dump for Vec<T>
where
    T: Dump,
{
    fn save<'a>(&'a self, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
        save_many(self, buf, pos);
    }

    unsafe fn restore(buf: &mut [u8], this: *mut Self) -> &mut [u8] {
        let len = (*this).len();
        let (ptr, rest) = restore_many::<T>(len, buf);

        let vals = Vec::from_raw_parts(ptr, len, len);

        write(this, vals);
        rest
    }
}

fn save_many<'a, T>(vals: &'a [T], buf: &mut Vec<IoSlice<'a>>, pos: &mut usize)
where
    T: Dump,
{
    if vals.is_empty() {
        return;
    }

    pad::<T>(buf, pos);

    let ptr = vals.as_ptr() as *const u8;
    let len = vals.len() * size_of::<T>();

    unsafe {
        buf.push(IoSlice::new(from_raw_parts(ptr, len)));
        *pos += len;
    }

    for val in vals {
        val.save(buf, pos);
    }
}

unsafe fn restore_many<T>(len: usize, mut buf: &mut [u8]) -> (*mut T, &mut [u8])
where
    T: Dump,
{
    if len == 0 {
        return (align_of::<T>() as *mut T, buf);
    }

    buf = skip::<T>(buf);

    let ptr = buf.as_mut_ptr() as *mut T;
    buf = &mut buf[size_of::<T>() * len..];

    for idx in 0..len {
        buf = Dump::restore(buf, ptr.add(idx));
    }

    (ptr, buf)
}

fn save_one<'a, T>(val: &'a T, buf: &mut Vec<IoSlice<'a>>, pos: &mut usize) {
    pad::<T>(buf, pos);

    let ptr = val as *const T as *const u8;
    let len = size_of::<T>();

    unsafe {
        buf.push(IoSlice::new(from_raw_parts(ptr, len)));
        *pos += len;
    }
}

fn restore_one<T>(mut buf: &mut [u8]) -> (*mut T, &mut [u8]) {
    buf = skip::<T>(buf);

    let ptr = buf.as_mut_ptr() as *mut T;
    buf = &mut buf[size_of::<T>()..];

    (ptr, buf)
}

fn pad<T>(buf: &mut Vec<IoSlice<'_>>, pos: &mut usize) {
    let align = align_of::<T>();
    let rest = *pos % align;

    if rest != 0 {
        const PADDING: [&[u8]; 7] = [
            &[0],
            &[0, 0],
            &[0, 0, 0],
            &[0, 0, 0, 0],
            &[0, 0, 0, 0, 0],
            &[0, 0, 0, 0, 0, 0],
            &[0, 0, 0, 0, 0, 0, 0],
        ];

        let padding = PADDING[align - rest - 1];

        buf.push(IoSlice::new(padding));
        *pos += padding.len();
    }
}

fn skip<T>(buf: &mut [u8]) -> &mut [u8] {
    let align = align_of::<T>();
    let rest = buf.as_mut_ptr() as usize % align;

    if rest != 0 {
        &mut buf[align - rest..]
    } else {
        buf
    }
}
