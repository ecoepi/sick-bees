use std::ops::{ControlFlow, Deref};

pub trait Point: Sized {
    const DIM: usize;
    fn coord(&self, axis: usize) -> f64;
}

pub trait Query<P: Point> {
    fn aabb(&self) -> &(P, P);
    fn test(&self, position: &P) -> bool;
}

pub trait Object: Send {
    type Point: Point;
    fn position(&self) -> &Self::Point;
}

#[allow(clippy::upper_case_acronyms)]
pub struct KDTree<O> {
    objects: Box<[O]>,
}

impl<O: Object> KDTree<O> {
    pub fn new(mut objects: Box<[O]>) -> Self {
        sort(&mut objects, 0);

        Self { objects }
    }

    pub fn look_up<'a, Q: Query<O::Point>, V: FnMut(&'a O) -> ControlFlow<()>>(
        &'a self,
        query: Q,
        visitor: V,
    ) {
        if self.objects.is_empty() {
            return;
        }

        look_up(&mut LookUpArgs { query, visitor }, &self.objects, 0);
    }
}

impl<O> Deref for KDTree<O> {
    type Target = [O];

    fn deref(&self) -> &Self::Target {
        &self.objects
    }
}

fn sort<O: Object>(objects: &mut [O], axis: usize) {
    if objects.len() <= 1 {
        return;
    }

    let mid = objects.len() / 2;

    let (left, _, right) = objects.select_nth_unstable_by(mid, |lhs, rhs| {
        let lhs = lhs.position().coord(axis);
        let rhs = rhs.position().coord(axis);

        lhs.partial_cmp(&rhs).unwrap()
    });

    let next_axis = (axis + 1) % O::Point::DIM;

    sort(left, next_axis);
    sort(right, next_axis);
}

struct LookUpArgs<Q, V> {
    query: Q,
    visitor: V,
}

fn look_up<'a, O: Object, Q: Query<O::Point>, V: FnMut(&'a O) -> ControlFlow<()>>(
    args: &mut LookUpArgs<Q, V>,
    mut objects: &'a [O],
    mut axis: usize,
) -> ControlFlow<()> {
    loop {
        let (left, object, right) = split(objects);

        let position = object.position();

        if contains(args.query.aabb(), position) && args.query.test(position) {
            (args.visitor)(object)?;
        }

        let next_axis = (axis + 1) % O::Point::DIM;

        if !left.is_empty() && args.query.aabb().0.coord(axis) <= position.coord(axis) {
            look_up(args, left, next_axis)?;
        }

        if right.is_empty() || args.query.aabb().1.coord(axis) < position.coord(axis) {
            return ControlFlow::Continue(());
        }

        objects = right;
        axis = next_axis;
    }
}

fn split<O>(objects: &[O]) -> (&[O], &O, &[O]) {
    assert!(!objects.is_empty());

    let mid = objects.len() / 2;

    unsafe {
        (
            objects.get_unchecked(..mid),
            objects.get_unchecked(mid),
            objects.get_unchecked(mid + 1..),
        )
    }
}

fn contains<P: Point>(aabb: &(P, P), position: &P) -> bool {
    for axis in 0..P::DIM {
        if aabb.0.coord(axis) > position.coord(axis) || aabb.1.coord(axis) < position.coord(axis) {
            return false;
        }
    }

    true
}
