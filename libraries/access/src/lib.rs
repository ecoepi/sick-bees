use std::convert::TryInto;
use std::fmt::Debug;
use std::ops::Div;

pub fn dictionary<K, V>(dict: &mut Vec<(K, V)>, key: K) -> &mut V
where
    K: Copy + Ord,
    V: Default,
{
    let idx = dict
        .binary_search_by_key(&key, |(key, _)| *key)
        .unwrap_or_else(|idx| {
            dict.insert(idx, (key, Default::default()));

            idx
        });

    &mut dict[idx].1
}

pub fn histogram<T, I>(hist: &mut Vec<usize>, res: T, val: T)
where
    T: Copy + Div<Output = I>,
    I: TryInto<usize>,
    I::Error: Debug,
{
    let idx = (val / res).try_into().unwrap();

    if idx >= hist.len() {
        hist.resize(idx + 1, 0);
    }

    hist[idx] += 1;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn histogram_collects_counts() {
        let mut hist = Vec::new();

        histogram(&mut hist, 2, 1);
        histogram(&mut hist, 2, 2);
        histogram(&mut hist, 2, 3);
        histogram(&mut hist, 2, 5);
        histogram(&mut hist, 2, 5);
        histogram(&mut hist, 2, 21);

        assert_eq!(hist.len(), 11);
        assert_eq!(hist[0], 1);
        assert_eq!(hist[1], 2);
        assert_eq!(hist[2], 2);
        assert_eq!(hist[3], 0);
        assert_eq!(hist[4], 0);
        assert_eq!(hist[5], 0);
        assert_eq!(hist[6], 0);
        assert_eq!(hist[7], 0);
        assert_eq!(hist[8], 0);
        assert_eq!(hist[9], 0);
        assert_eq!(hist[10], 1);
    }
}
