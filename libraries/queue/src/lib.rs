#![feature(maybe_uninit_uninit_array)]

use std::mem::MaybeUninit;
use std::ptr::{read, write};
use std::slice::SliceIndex;

pub struct Queue<T: Copy, const N: usize> {
    vals: [MaybeUninit<T>; N],
    len: u16,
    pos: u16,
}

#[allow(clippy::len_without_is_empty)]
impl<T: Copy, const N: usize> Queue<T, N> {
    const CAP: u16 = N as u16;

    unsafe fn head(&mut self) -> *mut T {
        self.vals.get_unchecked_mut(self.pos as usize).as_mut_ptr()
    }

    unsafe fn tail(&self) -> *const T {
        let tail = if self.len <= self.pos {
            self.pos - self.len
        } else {
            Self::CAP - (self.len - self.pos)
        };

        self.vals.get_unchecked(tail as usize).as_ptr()
    }

    unsafe fn get<I>(&self, index: I) -> &[T]
    where
        I: SliceIndex<[MaybeUninit<T>], Output = [MaybeUninit<T>]>,
    {
        &*(self.vals.get_unchecked(index) as *const [MaybeUninit<T>] as *const [T])
    }

    pub fn len(&self) -> u16 {
        self.len
    }

    pub fn push(&mut self, val: T) {
        unsafe {
            let head = self.head();

            if self.len != Self::CAP {
                self.len += 1;
            }

            write(head, val);
        }

        self.pos = (self.pos + 1) % Self::CAP;
    }

    pub fn peek(&self) -> Option<&T> {
        unsafe {
            if self.len != 0 {
                let tail = self.tail();

                Some(&*tail)
            } else {
                None
            }
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        unsafe {
            if self.len != 0 {
                let tail = self.tail();

                self.len -= 1;

                Some(read(tail))
            } else {
                None
            }
        }
    }

    pub fn as_slices(&self) -> (&[T], &[T]) {
        unsafe {
            if self.len == 0 {
                (&[], &[])
            } else if self.len == Self::CAP {
                let left = self.get(..);

                (left, &[])
            } else if self.len <= self.pos {
                let left = self.get((self.pos - self.len) as usize..self.pos as usize);

                (left, &[])
            } else {
                let left = self.get(0..self.pos as usize);
                let right = self.get((Self::CAP - self.len + self.pos) as usize..);

                (left, right)
            }
        }
    }

    pub fn contains(&self, val: &T) -> bool
    where
        T: PartialEq,
    {
        let (left, right) = self.as_slices();

        left.contains(val) || right.contains(val)
    }
}

impl<T: Copy, const N: usize> Default for Queue<T, N> {
    fn default() -> Self {
        Self {
            vals: MaybeUninit::uninit_array(),
            len: 0,
            pos: 0,
        }
    }
}

impl<T: Copy, const N: usize> Clone for Queue<T, N> {
    fn clone(&self) -> Self {
        *self
    }
}

impl<T: Copy, const N: usize> Copy for Queue<T, N> {}
