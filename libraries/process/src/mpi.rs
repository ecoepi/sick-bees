use std::convert::TryInto;
use std::marker::PhantomData;
use std::mem::size_of;
use std::os::raw::{c_int, c_void};

use super::{MailBoxes, Message, Process};

pub fn run<F, M>(start: F)
where
    F: FnOnce(u8, u8, &mut dyn Process<M>) + Clone + Send + 'static,
    M: Message + Send + 'static,
{
    let world = World::new();

    let rank = world.rank();
    let size = world.size();

    start(rank, size, &mut MessagePassing { world });
}

struct MessagePassing<M> {
    world: World<M>,
}

impl<M> Process<M> for MessagePassing<M>
where
    M: Message,
{
    fn exchange(&mut self, mboxes: &mut MailBoxes<M>) {
        for mbox in mboxes.0.iter() {
            unsafe {
                self.world.immediate_send(mbox.dest, &mbox.outbox);
            }
        }

        for mbox in mboxes.0.iter_mut() {
            self.world.receive(mbox.dest, &mut mbox.inbox);
        }

        self.world.wait();

        for mbox in mboxes.0.iter_mut() {
            mbox.outbox.clear();
        }
    }
}

extern "C" {
    fn world_new(message_size: c_int) -> *mut c_void;
    fn world_drop(world: *mut c_void);

    fn world_rank(world: *mut c_void) -> c_int;
    fn world_size(world: *mut c_void) -> c_int;

    fn world_immediate_send(
        world: *mut c_void,
        destination: c_int,
        buffer: *const c_void,
        count: c_int,
    );
    fn world_wait(world: *mut c_void);

    fn world_probe(world: *mut c_void, source: c_int) -> c_int;
    fn world_receive(world: *mut c_void, source: c_int, buffer: *mut c_void, count: c_int);
}

struct World<M>(*mut c_void, PhantomData<M>);

impl<M> World<M> {
    fn new() -> Self {
        unsafe { Self(world_new(size_of::<M>() as c_int), PhantomData) }
    }

    fn rank(&self) -> u8 {
        unsafe { world_rank(self.0).try_into().unwrap() }
    }

    fn size(&self) -> u8 {
        unsafe { world_size(self.0).try_into().unwrap() }
    }

    unsafe fn immediate_send(&mut self, destination: u8, messages: &[M]) {
        world_immediate_send(
            self.0,
            destination as c_int,
            messages.as_ptr() as *const c_void,
            messages.len() as c_int,
        );
    }

    fn receive(&mut self, source: u8, messages: &mut Vec<M>)
    where
        M: Message,
    {
        unsafe {
            let count = world_probe(self.0, source as c_int);

            messages.reserve(count as usize);

            world_receive(
                self.0,
                source as c_int,
                messages.as_mut_ptr().add(messages.len()) as *mut c_void,
                count,
            );

            messages.set_len(messages.len() + count as usize);
        }
    }

    fn wait(&mut self) {
        unsafe {
            world_wait(self.0);
        }
    }
}

impl<M> Drop for World<M> {
    fn drop(&mut self) {
        unsafe {
            world_drop(self.0);
        }
    }
}
