#include <stdlib.h>

#include <mpi.h>

struct World {
  MPI_Datatype message_type;
  int requests_len;
  int requests_cap;
  MPI_Request *requests;
};

struct World *world_new(int message_size) {
  MPI_Init(NULL, NULL);

  struct World *world = malloc(sizeof(struct World));

  MPI_Type_contiguous(message_size, MPI_UNSIGNED_CHAR, &world->message_type);
  MPI_Type_commit(&world->message_type);

  world->requests_len = 0;
  world->requests_cap = 0;
  world->requests = NULL;

  return world;
}

void world_drop(struct World *world) {
  MPI_Type_free(&world->message_type);

  free(world->requests);
  free(world);

  MPI_Finalize();
}

int world_rank(struct World *world) {
  (void)world;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  return rank;
}

int world_size(struct World *world) {
  (void)world;

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  return size;
}

void world_immediate_send(struct World *world, int destination,
                          const void *buffer, int count) {
  if (world->requests_len == world->requests_cap) {
    world->requests =
        realloc(world->requests, ++world->requests_cap * sizeof(MPI_Request));
  }

  MPI_Isend(buffer, count, world->message_type, destination, 0, MPI_COMM_WORLD,
            &world->requests[world->requests_len++]);
}

void world_wait(struct World *world) {
  MPI_Waitall(world->requests_len, world->requests, NULL);

  world->requests_len = 0;
}

int world_probe(struct World *world, int source) {
  MPI_Status status;
  MPI_Probe(source, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

  int count;
  MPI_Get_count(&status, world->message_type, &count);

  return count;
}

void world_receive(struct World *world, int source, void *buffer, int count) {
  MPI_Recv(buffer, count, world->message_type, source, MPI_ANY_TAG,
           MPI_COMM_WORLD, NULL);
}