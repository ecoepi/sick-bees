use std::cell::UnsafeCell;
use std::convert::TryInto;
use std::hint::spin_loop;
use std::mem::swap;
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    mpsc::channel,
    Arc,
};
use std::thread::{park, spawn, Thread};

use super::{MailBoxes, Message, Process};

pub fn run<F, M>(size: usize, start: F)
where
    F: FnOnce(u8, u8, &mut dyn Process<M>) + Clone + Send + 'static,
    M: Message + Send + 'static,
{
    let pidgeonholes = (0..size * size)
        .map(|_| Pidgeonhole {
            flag: AtomicUsize::new(0),
            msgs: UnsafeCell::new(Vec::new()),
        })
        .collect::<Arc<[_]>>();

    let handles = (0..size)
        .map(|rank| {
            let start = start.clone();
            let pidgeonholes = pidgeonholes.clone();

            let (sender, receiver) = channel();

            let handle = spawn(move || {
                let threads = receiver.recv().unwrap();

                start(
                    rank.try_into().unwrap(),
                    size.try_into().unwrap(),
                    &mut MultipleThreads {
                        rank,
                        size,
                        threads,
                        pidgeonholes,
                    },
                );
            });

            (handle, sender)
        })
        .collect::<Vec<_>>();

    let threads = handles
        .iter()
        .map(|(handle, _)| handle.thread().clone())
        .collect::<Arc<[_]>>();

    for (_, sender) in &handles {
        sender.send(threads.clone()).unwrap();
    }

    for (handle, _) in handles {
        handle.join().unwrap();
    }
}

struct MultipleThreads<M> {
    rank: usize,
    size: usize,
    threads: Arc<[Thread]>,
    pidgeonholes: Arc<[Pidgeonhole<M>]>,
}

impl<M> Process<M> for MultipleThreads<M>
where
    M: Message,
{
    fn exchange(&mut self, mboxes: &mut MailBoxes<M>) {
        for mbox in mboxes.0.iter_mut() {
            unsafe {
                let receiver = &self.threads[mbox.dest as usize];
                let pidgeonhole = &self.pidgeonholes[self.rank + mbox.dest as usize * self.size];

                pidgeonhole.put(&mut mbox.outbox, receiver);
            }
        }

        for mbox in mboxes.0.iter_mut() {
            unsafe {
                let sender = &self.threads[mbox.dest as usize];
                let pidgeonhole = &self.pidgeonholes[mbox.dest as usize + self.rank * self.size];

                pidgeonhole.take(&mut mbox.inbox, sender);
            }
        }
    }
}

#[repr(align(128))]
struct Pidgeonhole<M> {
    flag: AtomicUsize,
    msgs: UnsafeCell<Vec<M>>,
}

unsafe impl<M> Sync for Pidgeonhole<M> where M: Send {}

const OWNER: usize = 0b01;
const WAITER: usize = 0b10;

impl<M> Pidgeonhole<M> {
    unsafe fn put(&self, msgs: &mut Vec<M>, receiver: &Thread) {
        self.wait(|flag| flag != 0);

        swap(&mut *self.msgs.get(), msgs);

        if self.flag.swap(1, Ordering::AcqRel) & WAITER != 0 {
            receiver.unpark();
        }
    }

    unsafe fn take(&self, msgs: &mut Vec<M>, sender: &Thread) {
        self.wait(|flag| flag == 0);

        swap(&mut *self.msgs.get(), msgs);

        if self.flag.swap(0, Ordering::AcqRel) & WAITER != 0 {
            sender.unpark();
        }
    }

    fn wait(&self, cond: impl Fn(usize) -> bool) {
        let mut rounds = 0;

        while cond(self.flag.load(Ordering::Acquire) & OWNER) {
            if rounds < 6 {
                for _ in 0..(1 << rounds) {
                    spin_loop();
                }

                rounds += 1;
            } else if cond(self.flag.fetch_or(WAITER, Ordering::AcqRel) & OWNER) {
                park();
            }
        }
    }
}
