#[cfg(feature = "mpi")]
mod mpi;
mod threads;

use std::env::var;

#[allow(clippy::missing_safety_doc)]
pub unsafe trait Message: Copy {}

pub trait Process<M>
where
    M: Message,
{
    fn exchange(&mut self, mboxes: &mut MailBoxes<M>);
}

pub fn run<F, M>(start: F)
where
    F: FnOnce(u8, u8, &mut dyn Process<M>) + Clone + Send + 'static,
    M: Message + Send + 'static,
{
    #[cfg(feature = "mpi")]
    if var("OMPI_UNIVERSE_SIZE").is_ok() {
        mpi::run(start);
        return;
    }

    if let Ok(num_threads) = var("NUM_THREADS") {
        let size = num_threads.parse::<usize>().unwrap();

        threads::run(size, start);
        return;
    }

    start(0, 1, &mut SingleThread);
}

pub struct SingleThread;

impl<M> Process<M> for SingleThread
where
    M: Message,
{
    fn exchange(&mut self, _mboxes: &mut MailBoxes<M>) {}
}

struct MailBox<M> {
    dest: u8,
    inbox: Vec<M>,
    outbox: Vec<M>,
}

pub struct MailBoxes<M>(Box<[MailBox<M>]>);

impl<M> MailBoxes<M> {
    pub fn new(dests: impl Iterator<Item = u8>) -> Self {
        let mut mboxes = dests
            .map(|dest| MailBox {
                dest,
                inbox: Vec::new(),
                outbox: Vec::new(),
            })
            .collect::<Box<[_]>>();

        mboxes.sort_unstable_by_key(|mbox| mbox.dest);

        Self(mboxes)
    }

    pub fn push(&mut self, dest: u8, msg: M) {
        let idx = self
            .0
            .binary_search_by_key(&dest, |mbox| mbox.dest)
            .unwrap();

        self.0[idx].outbox.push(msg);
    }

    pub fn drain(&mut self) -> impl Iterator<Item = M> + '_ {
        self.0.iter_mut().flat_map(|mbox| mbox.inbox.drain(..))
    }
}
