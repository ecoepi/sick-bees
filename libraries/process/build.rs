#[cfg(feature = "mpi")]
use cc::Build;
#[cfg(feature = "mpi")]
use pkg_config::Config;

#[cfg(feature = "mpi")]
fn main() {
    let ompi = Config::new().probe("ompi").unwrap();

    Build::new()
        .includes(ompi.include_paths)
        .file("src/mpi_shim.c")
        .compile("mpi_shim");

    println!("cargo:rerun-if-changed=src/mpi_shim.c");
}

#[cfg(not(feature = "mpi"))]
fn main() {}
