import math
import sys
import struct

from osgeo import gdal, ogr

WIDTH, HEIGHT, LENGTH = 10000, 10000, 1000

# based on the number of inflorescences per habitat type
# from the VOODOO flower survey, 2021 in France
WEIGHTS = {
    "I1.5": 0.021535,
    "G1.A": 0.064231,
    "G1.C": 0.032061,
    "G5.7": 0.008234,
    "FA.2": 0.010197,
    "I1.1": 0.189130,
    "E2.2": 0.038091,
    "E3.4": 0.003717,
    "E2.6": 0.222857,
    "J4.6": 0.004463,
    "C2.3": 0.004967,
    "X07": 0.071128,
    "I2.2": 0.321364,
    "FA.3": 0.003607,
    "F3.1": 0.002357,
    "C2.5": 0.000515,
    "FB.4": 0.001546,
}

data = gdal.OpenEx(sys.argv[1], gdal.OF_VECTOR)
layer = data.GetLayer()

x_min, x_max, y_min, y_max = layer.GetExtent()
x_low = (x_max + x_min - LENGTH) / 2
y_high = (y_max + y_min + LENGTH) / 2

driver = ogr.GetDriverByName("MEMORY")
temp_data = driver.CreateDataSource("TEMP")
temp_data.CopyLayer(layer, "temp")

temp_layer = temp_data.GetLayer()
temp_layer.CreateField(ogr.FieldDefn("Weight", ogr.OFTReal))

for feature in temp_layer:
    eunis = feature.GetField("Code_EUNIS")

    weight = 0.0

    # https://eunis.eea.europa.eu/habitats.jsp
    if eunis and eunis in WEIGHTS:
        weight = WEIGHTS[eunis]

    feature.SetField("Weight", weight)
    temp_layer.SetFeature(feature)


driver = gdal.GetDriverByName("GTiff")
out_data = driver.Create("/vsimem/map", WIDTH, HEIGHT, 1, gdal.GDT_Float32)
out_data.SetGeoTransform((x_low, LENGTH / WIDTH, 0, y_high, 0, -LENGTH / HEIGHT))

gdal.RasterizeLayer(out_data, [1], temp_layer, options=["ATTRIBUTE=Weight"])

sys.stdout.buffer.write(struct.pack("=QQ", WIDTH, HEIGHT))
sys.stdout.buffer.write(out_data.GetRasterBand(1).ReadRaster())
