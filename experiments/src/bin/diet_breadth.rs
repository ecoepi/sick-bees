use std::env::args;

use rand_distr::{Bernoulli, Normal, WeightedIndex};

use experiments::{preferences, sample};
use model::{
    params::Params,
    units::{MassRate, Tick},
};

fn main() {
    let mut params: Params = Default::default();

    let mut narrow_diet = false;
    let mut dominant_species = false;
    let mut more_flowers = false;
    let mut search_time_limited = false;

    for arg in args().skip(1) {
        match arg.as_str() {
            "narrow_diet" => narrow_diet = true,
            "dominant_species" => dominant_species = true,
            "more_flowers" => more_flowers = true,
            "search_time_limited" => search_time_limited = true,
            seed => params.init.seed = seed.parse().unwrap(),
        }
    }

    params.init.hoverflies = 9_000;
    params.init.clutches = 9_000;

    params.init.solitary_bees = 0;
    params.init.nests = 0;
    params.init.colonies = 0;

    let mut flower_patch = params.flower_patches[0].clone();
    flower_patch.flower_density = Normal::new(500., 50.).unwrap();
    flower_patch.nectar_production = Normal::new(1_000., 100.).unwrap();

    params.flower_patches = vec![flower_patch; 2].into();

    let mut hoverfly = params.hoverflies[0].clone();
    hoverfly.insect.nectar_consumption = MassRate::milligrams_per_hour(75.) * params.tick;
    hoverfly.insect.preferences = preferences(&[1.; 2]);

    params.hoverflies = vec![hoverfly; 2].into();

    params.solitary_bees = Default::default();
    params.bumblebees = Default::default();

    if narrow_diet {
        params.hoverflies[0].insect.preferences = preferences(&[1., 0.]);
    }

    params.init.hoverfly_species =
        WeightedIndex::new(&if dominant_species { [3, 1] } else { [1, 1] }).unwrap();

    params.init.flower_patch_species =
        WeightedIndex::new(&if more_flowers { [3, 1] } else { [1, 1] }).unwrap();

    if search_time_limited {
        params.init.flower_patch_cover /= 10.;
        params.init.flower_patches /= 10;

        params.init.empty_flower_patches = Bernoulli::new(0.99).unwrap();
    } else {
        params.init.empty_flower_patches = Bernoulli::new(0.6).unwrap();
    }

    params.transect.wait_until = Tick::MAX;

    sample(params, Tick::months(4), Tick::days(1));
}
