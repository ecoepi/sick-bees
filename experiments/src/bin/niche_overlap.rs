use std::env::args;
use std::sync::Arc;

use rand_distr::{Bernoulli, Exp, Normal, WeightedIndex};

use experiments::{preferences, sample};
use model::{
    insect::Species as InsectSpecies,
    params::Params,
    pathogen::{FixedSpecificPrevalence, MinimumSpecificPrevalence},
    units::{MassRate, Rate, Tick},
};

fn main() {
    let mut params: Params = Default::default();

    let mut args = args().skip(1);

    let weight: isize = args.next().unwrap().parse().unwrap();
    let prevalence: f64 = args.next().unwrap().parse().unwrap();

    let strength: f64 = args.next().unwrap().parse().unwrap();
    let infection: f64 = args.next().unwrap().parse().unwrap();
    let contamination: f64 = args.next().unwrap().parse().unwrap();
    let decontamination: f64 = args.next().unwrap().parse().unwrap();
    let recovery: f64 = args.next().unwrap().parse().unwrap();

    if let Some(arg) = args.next() {
        params.init.seed = arg.parse().unwrap();
    }

    params.init.hoverflies = 9_000;
    params.init.clutches = 9_000;

    params.init.solitary_bees = 0;
    params.init.nests = 0;
    params.init.colonies = 0;

    let mut flower_patch = params.flower_patches[0].clone();
    flower_patch.flower_density = Normal::new(500., 50.).unwrap();
    flower_patch.nectar_production = Normal::new(1_000., 100.).unwrap();

    flower_patch.decontamination =
        Exp::new(Rate::per_hour(1. / 3.) * decontamination * params.tick).unwrap();

    params.flower_patches = vec![flower_patch; 3].into();

    let mut hoverfly1 = params.hoverflies[0].clone();
    hoverfly1.insect.nectar_consumption = MassRate::milligrams_per_hour(75.) * params.tick;
    hoverfly1.insect.preferences = preferences(&[1., 0., 1.]);

    hoverfly1.insect.infection = Bernoulli::new(0.03 * strength).unwrap();
    hoverfly1.insect.contamination = Bernoulli::new(0.03 * strength).unwrap();
    hoverfly1.insect.recovery = Exp::new(Rate::per_day(1. / 3.) * recovery * params.tick).unwrap();

    let mut hoverfly2 = params.hoverflies[0].clone();
    hoverfly2.insect.nectar_consumption = MassRate::milligrams_per_hour(75.) * params.tick;
    hoverfly2.insect.preferences = preferences(&[0., 1., 1.]);

    hoverfly2.insect.infection = Bernoulli::new(0.03 * strength * infection).unwrap();
    hoverfly2.insect.contamination = Bernoulli::new(0.03 * strength * contamination).unwrap();
    hoverfly2.insect.recovery = Exp::new(Rate::per_day(1. / 3.) * recovery * params.tick).unwrap();

    params.hoverflies = vec![hoverfly1, hoverfly2].into();

    params.solitary_bees = Default::default();
    params.bumblebees = Default::default();

    params.init.hoverfly_species = WeightedIndex::new(&[1, 2]).unwrap();

    params.init.flower_patch_species = WeightedIndex::new(&if weight >= 0 {
        [weight as usize, 2 * weight as usize, 100]
    } else {
        [100, 200, -weight as usize]
    })
    .unwrap();

    params.init.empty_flower_patches = Bernoulli::new(0.6).unwrap();

    if prevalence > 0. {
        params.init.infection = Arc::new(FixedSpecificPrevalence::new(
            InsectSpecies::Hoverfly(0),
            prevalence,
        ));
    } else if prevalence < 0. {
        params.init.infection = Arc::new(MinimumSpecificPrevalence::new(
            InsectSpecies::Hoverfly(0),
            -prevalence,
        ));
    }

    params.transect.wait_until = Tick::MAX;

    sample(params, Tick::months(4), Tick::days(1));
}
