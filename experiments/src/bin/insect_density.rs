use std::env::args;

use experiments::sample;
use model::{params::Params, units::Tick};

fn main() {
    let mut params: Params = Default::default();

    let mut args = args().skip(1);

    let mut factor: f64 = args.next().unwrap().parse().unwrap();

    factor /= 5.;

    if let Some(arg) = args.next() {
        params.init.seed = arg.parse().unwrap();
    }

    let scale = |value: &mut u32| *value = (*value as f64 * factor) as u32;
    scale(&mut params.init.hoverflies);
    scale(&mut params.init.clutches);
    scale(&mut params.init.solitary_bees);
    scale(&mut params.init.nests);
    scale(&mut params.init.colonies);

    params.init.flower_patch_cover *= factor;

    let duration = params.transect.wait_until + Tick::days(1);

    sample(params, duration, Tick::hours(3));
}
