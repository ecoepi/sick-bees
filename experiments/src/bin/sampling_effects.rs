use std::env::args;

use rand_distr::{Bernoulli, Normal, WeightedIndex};

use experiments::{preferences, sample};
use model::{
    observer::Strategy as ObserverStrategy,
    params::Params,
    units::{MassRate, Tick},
};

fn main() {
    let mut params: Params = Default::default();

    let mut narrow_diet = false;
    let mut more_sampling = false;
    let mut even_more_sampling = false;
    let mut more_handling = false;
    let mut even_more_handling = false;
    let mut no_handling_at_all = false;
    let mut sit_instead_of_walk = false;
    let mut number_of_spots = 0;
    let mut use_landscape = false;

    let mut args = args().skip(1);

    while let Some(arg) = args.next() {
        match arg.as_str() {
            "narrow_diet" => narrow_diet = true,
            "more_sampling" => more_sampling = true,
            "even_more_sampling" => even_more_sampling = true,
            "more_handling" => more_handling = true,
            "even_more_handling" => even_more_handling = true,
            "no_handling_at_all" => no_handling_at_all = true,
            "sit_instead_of_walk" => {
                sit_instead_of_walk = true;

                number_of_spots = args.next().unwrap().parse().unwrap();
            }
            "use_landscape" => use_landscape = true,
            seed => params.init.seed = seed.parse().unwrap(),
        }
    }

    params.init.hoverflies = 9_000;
    params.init.clutches = 9_000;

    params.init.solitary_bees = 0;
    params.init.nests = 0;
    params.init.colonies = 0;

    let mut flower_patch = params.flower_patches[0].clone();
    flower_patch.flower_density = Normal::new(500., 50.).unwrap();
    flower_patch.nectar_production = Normal::new(1_000., 100.).unwrap();

    params.flower_patches = vec![flower_patch; 3].into();

    let mut hoverfly = params.hoverflies[0].clone();
    hoverfly.insect.nectar_consumption = MassRate::milligrams_per_hour(75.) * params.tick;
    hoverfly.insect.preferences = preferences(&[1.; 3]);

    params.hoverflies = vec![hoverfly; 2].into();

    params.solitary_bees = Default::default();
    params.bumblebees = Default::default();

    if narrow_diet {
        params.hoverflies[0].insect.preferences = preferences(&[1., 0., 0.]);
    }

    params.init.hoverfly_species = WeightedIndex::new(&[1, 1]).unwrap();

    params.init.flower_patch_species = WeightedIndex::new(&[1, 1, 1]).unwrap();

    params.init.empty_flower_patches = Bernoulli::new(0.6).unwrap();

    params.transect.wait_until = Tick::days(45);

    if more_sampling {
        params.transect.duration *= 10;

        if even_more_sampling {
            params.transect.duration *= 10;
        }
    }

    if more_handling {
        params.transect.handling_duration *= 10;

        if even_more_handling {
            params.transect.handling_duration *= 10;
        }
    }

    if no_handling_at_all {
        params.transect.handling_duration = Default::default();
    }

    if sit_instead_of_walk {
        params.transect.strategy = ObserverStrategy::Sit(number_of_spots);
    }

    if use_landscape {
        params.init.landscape = Some("../landscape.map".into());
    }

    sample(params, Tick::months(2), Tick::days(1));
}
