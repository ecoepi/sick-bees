use std::env::args;
use std::sync::Arc;

use rand_distr::{Bernoulli, Exp, Normal, WeightedIndex};

use experiments::{preferences, sample};
use model::{
    params::Params,
    pathogen::InitialOverallPrevalence,
    units::{MassRate, Rate, Tick},
};

fn main() {
    let mut params: Params = Default::default();

    let mut args = args();
    let months = args.nth(1).unwrap().parse::<i64>().unwrap();
    let life_expectancy = args.next().unwrap().parse::<f64>().unwrap();
    let decontamination = args.next().unwrap().parse::<f64>().unwrap();
    let narrow_diet = args.next().unwrap().parse::<bool>().unwrap();
    let seed = args.next().map_or(0, |arg| arg.parse::<u64>().unwrap());

    params.trace_temporal_network = true;

    params.init.seed = seed;

    params.length /= 2.;

    params.init.flower_patches /= 4;
    params.init.hoverflies = 9_000 / 4;
    params.init.clutches = 9_000 / 4;

    params.init.solitary_bees = 0;
    params.init.nests = 0;
    params.init.colonies = 0;

    let mut flower_patch = params.flower_patches[0].clone();
    flower_patch.flower_density = Normal::new(500., 50.).unwrap();
    flower_patch.nectar_production = Normal::new(1_000., 100.).unwrap();

    flower_patch.decontamination =
        Exp::new(Rate::per_hour(1. / decontamination) * params.tick).unwrap();

    params.flower_patches = vec![flower_patch; 2].into();

    let mut hoverfly = params.hoverflies[0].clone();
    hoverfly.insect.nectar_consumption = MassRate::milligrams_per_hour(75.) * params.tick;
    hoverfly.insect.preferences = preferences(&[1.; 2]);

    hoverfly.insect.life_expectancy =
        Exp::new(Rate::per_day(1. / life_expectancy) * params.tick).unwrap();

    params.hoverflies = vec![hoverfly; 2].into();

    if narrow_diet {
        params.hoverflies[0].insect.preferences = preferences(&[1., 0.]);
    }

    params.solitary_bees = Default::default();
    params.bumblebees = Default::default();

    params.init.hoverfly_species = WeightedIndex::new(&[1, 1]).unwrap();
    params.init.flower_patch_species = WeightedIndex::new(&[1, 1]).unwrap();

    params.init.empty_flower_patches = Bernoulli::new(0.6).unwrap();

    params.init.infection = Arc::new(InitialOverallPrevalence::new(0.));

    params.transect.wait_until = Tick::MAX;

    sample(params, Tick::months(months), Tick::days(1));
}
