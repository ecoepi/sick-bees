#![feature(write_all_vectored)]

use std::fs::File;
use std::io::{IoSlice, Write};
use std::mem::transmute;
use std::ops::Deref;
use std::path::Path;

use rand_distr::Bernoulli;

use dump::save;
use model::{params::Params, units::Tick, Model};
use process::run;

pub fn preferences<'a, P: IntoIterator<Item = &'a f64>>(probs: P) -> Box<[Bernoulli]> {
    probs
        .into_iter()
        .map(|prob| Bernoulli::new(*prob).unwrap())
        .collect()
}

pub fn sample(params: Params, duration: Tick, interval: Tick) {
    run(move |rank, size, process| {
        let mut model = Model::new(params, rank, size);

        let mut buffer = Vec::new();

        let locations = model.flower_patch_locations.deref();
        write_vectored(
            format!("flower_patch_locations_{:02}.bin", rank),
            &mut buffer,
            |buffer| save(&locations, buffer),
        );

        let locations = model.clutch_locations.deref();
        write_vectored(
            format!("clutch_locations_{:02}.bin", rank),
            &mut buffer,
            |buffer| save(&locations, buffer),
        );

        let locations = model.nest_locations.deref();
        write_vectored(
            format!("nest_locations_{:02}.bin", rank),
            &mut buffer,
            |buffer| save(&locations, buffer),
        );

        let duration = duration / interval;
        let interval = interval / model.params.tick;

        for snapshot in 0..duration {
            for _ in 0..interval {
                model.run(process);
            }

            write_vectored(
                format!("world_{:03}_{:02}.bin", snapshot, model.grid.rank),
                &mut buffer,
                |buffer| {
                    save(&model.world, buffer);
                },
            );

            model.world.temporal_network.clear();
        }
    });
}

fn write_vectored<'a, P, F>(path: P, buffer: &'a mut Vec<IoSlice<'static>>, f: F)
where
    P: AsRef<Path>,
    F: FnOnce(&mut Vec<IoSlice<'a>>),
{
    struct ClearOnDrop<'a, 'b>(&'a mut Vec<IoSlice<'b>>);

    impl<'a, 'b> Drop for ClearOnDrop<'a, 'b> {
        fn drop(&mut self) {
            self.0.clear();
        }
    }

    let buffer = ClearOnDrop(unsafe { transmute(buffer) });

    f(buffer.0);

    let mut file = File::create(path).unwrap();

    file.write_all_vectored(buffer.0).unwrap();
}
