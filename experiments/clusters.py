import math
import random
import sys
import struct

from PIL import Image, ImageDraw

WIDTH, HEIGHT = 10000, 10000

CLUSTERS = int(sys.argv[1])
AREA = float(sys.argv[2])

random.seed(int(sys.argv[3]))

radius = math.sqrt(AREA / CLUSTERS / math.pi)
clusters = []

while len(clusters) < CLUSTERS:
    x = random.uniform(radius, 1 - radius)
    y = random.uniform(radius, 1 - radius)

    for cluster in clusters:
        if math.hypot(x - cluster[0], y - cluster[1]) < 3 * radius:
            break
    else:
        clusters.append((x, y))

sys.stderr.write(f"Generated {CLUSTERS} clusters\n")

img = Image.new("F", (WIDTH, HEIGHT))
ctx = ImageDraw.Draw(img)

for (x, y) in clusters:
    ctx.ellipse(
        [
            WIDTH * (x - radius),
            HEIGHT * (y - radius),
            WIDTH * (x + radius),
            HEIGHT * (y + radius),
        ],
        fill=1,
    )

del ctx

sys.stdout.buffer.write(struct.pack("=QQ", WIDTH, HEIGHT))
sys.stdout.buffer.write(img.tobytes())
